import os
import csv
import time
import matplotlib.pyplot as plt
from matplotlib import gridspec
import numpy as np
import scipy.interpolate
import pandas as pd

from operator import add

# ##Temp./oC,Time/min,DSC/(mW/mg),Mass loss/mg,DTG/(mg/min),Sensit./(uV/mW),,time (s),Mass/%,g/s
def get_exp_file_data(filename):
    print("opening %s\n" % filename)
    xname = "time (s)"
    yname = "Mass/%"
    x = []
    y = []
    with open(filename, 'rb') as infile:
        reader = csv.DictReader(infile)
        for row in reader:
            x.append(row[xname])
            y.append(row[yname])
    a_zip = [x,y]
    return(a_zip)

# ##Temp./oC,Time/min,DSC/(mW/mg),Mass loss/mg,DTG/(mg/min),Sensit./(uV/mW),,time (s),Mass/%,g/s
def get_sim_file_data(filename):
    print("opening %s\n" % filename)
    xname = "t"
    yname = "002_M/M0( 0.0000_ 0.0000_ 0.0000)"
    x = []
    y = []
    with open(filename, 'rb') as infile:
        reader = csv.DictReader(infile)
        for row in reader:
            x.append(row[xname])
            y.append(float(row[yname])*100)
    a_zip = [x,y]
    return(a_zip)

def stacked_line_plot_with_TGA(points,exp,sim):
    DVs = []
    CHs = []
    ASs = []
    fig = plt.figure(figsize = (24,12))
    gs = gridspec.GridSpec(1,2, width_ratios=[1,3])
    plt.rc('text', usetex=True)
    plt.suptitle(r"Particle Composition in Simulated $\frac{20 ^\circ C}{min}$ Schedule")
    i = 0
    for time_point in points:
        t  = float(time_point["t"])
        DV = float(time_point["001_YI( 0.0000_ 0.0000_ 0.0000)"])
        CH = float(time_point["002_YI( 0.0000_ 0.0000_ 0.0000)"])
        AS = float(time_point["003_YI( 0.0000_ 0.0000_ 0.0000)"])
        ax = plt.subplot(gs[0])
        ax.set_ylim([0.0, 1.0])
        ax.clear()
        width = 0.5
        p1 = ax.bar(1, DV, width, bottom=0, color="brown", alpha=0.6)
        p2 = ax.bar(1, CH, width, bottom=DV, color="black", alpha=0.6 )
        p3 = ax.bar(1, AS, width, bottom=CH+DV, color="gray", alpha=0.6 )
        #ax.set_xlim([1,1])
        plt.xticks([0.75,1.25,1.75],"")
        ax.set_ylim([0.0, 1.0])
        ax.set_title("Gpyro experiment density of materials at time=%s" % str(round(t,1)))
        ax.set_xlabel("Spatial domain")
        ax.set_ylabel("Mass fraction of solid")
        ax.legend(loc=4)

        ax2 = plt.subplot(gs[1])
        ax2.plot(exp[0], exp[1], color="blue", marker='.', markersize=1.5, linestyle="None", label="Measured")
        ax2.plot(sim[0], sim[1], color="red", marker='.', markersize=1.5, linestyle="None", label="Simulated")
        ax2.plot(sim[0][i], sim[1][i], color="black",marker="^", markersize=5.0)
        ax2.axvline(x=sim[0][i], color="black")
        ax2.set_title("Experimental vs Simulated")
        ax2.set_ylabel(r'Mass remaining ($\frac{M}{M_{0}}$)')
        ax2.set_xlabel("Time (s)")
        ax2.set_ylim([0, 100])
        ax2.grid(linestyle='--', linewidth=0.5)
        ax2.legend()

        plt.subplots_adjust(left=0.1, wspace=0.1, top=0.8)
        #plt.show(block=True)

        fig.canvas.draw()
        plt.savefig('figs/gpyro_pyrolysis_%s.png' % str('%05d' % i))
        ax.clear()
        ax2.clear()
        i += 1
    return(0)

#wet_wood_density_DV0001
def get_points(pointfile, tags):
    with open(pointfile, 'r') as f:
        reader = csv.DictReader(f)
        values = []
        for row in reader:
            values.append(row)
    return(values)

exp_fname_20 = "PSME_wd_20_new.csv"
sim_fname_20 = "gpyro_summary_01_0003.csv"
exp_dirname = "/PSME_wood_gpyro/new_experimental_data/"
sim_dirname = "/PSME_wood_gpyro/"

d = os.getcwd()
expdata20 = get_exp_file_data(d + exp_dirname + exp_fname_20)
simdata20 = get_sim_file_data(d + sim_dirname + sim_fname_20)


#pointfile = os.getcwd() + "/albini_mass_conversion_test/albini_multi_layer/albini_model_devc.csv"
pointfile = os.getcwd() + "/PSME_wood_gpyro_plot_mass_fraction/gpyro_summary_01_0003.csv"
tags = ["DV", "CH", "AS"]

points = get_points(pointfile, tags)

stacked_line_plot_with_TGA(points,expdata20,simdata20)

