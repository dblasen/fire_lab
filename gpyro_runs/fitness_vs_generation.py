import os
import csv
import time
import matplotlib.pyplot as plt

def get_file_data(filename):
    print("opening %s\n" % filename)
    xname = "Gen"
    yname1 = "Best fit"
    yname2 = "Avg fit"
    x = []
    y1 = []
    y2 = []
    with open(filename, 'rb') as infile:
        reader = csv.DictReader(infile)
        for row in reader:
            x.append(row[xname])
            y1.append(row[yname1])
            y2.append(row[yname2])
    a_zip = [x,y1,y2]
    return(a_zip)

sim_name = "gpyro_summary.csv"
sim_dirname = "/PSME_foliage_gpyro/"

d = os.getcwd()
data = get_file_data(d + sim_dirname + sim_name)


plt.figure(figsize = (12,12))

ax1 = plt.subplot(111)
ax1.plot(data[0], data[1], color="blue", marker='.', markersize=2.5, linestyle="None", label="Best Fit")
#ax1.plot(data[0], data[2], color="red", marker='.', markersize=1.5, linestyle="None", label="Avg Fit")
ax1.set_title("12 Parameter GA Search; Population: 1k; Max Generations: 500")
ax1.set_xlabel("Generation")
ax1.set_ylabel("Fitness")
ax1.grid(linestyle='--', linewidth=0.5)
ax1.legend(loc='lower right')
plt.subplots_adjust(left=0.1, wspace=0.1, top=0.8)
plt.show(block=True)

#Gen,Best fit,Avg fit,nrepop,nnotconverged,var 01,var 02,var 03,var 04,var 05,var 06,var 07,var 08,var 09,var 10,var 11,var 12,
       #0, 0.519708226E+03, 0.519708226E+00,       0,       0, 0.893928012E+12, 0.138392746E+03, 0.488364124E+12, 0.158762941E+03, 0.345541473E+01, 0.341541756E+01, 0.362303463E+03, 0.133617472E+03, 0.217627654E+00, 0.624151741E-01, 0.184015824E+00, 0.323909381E+00,
