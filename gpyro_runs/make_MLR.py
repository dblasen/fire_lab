import os
import sys
import csv
import numpy as np

def generate_MLR_values(mass_losses, times, initial_mass):
    """gpyro requires the interesting MLR units:
    1000 * d(m/m_0)/dt
    so this function does the math and gives back the vector of values.
    """
    assert(len(mass_losses) == len(times)), "masses vector is not same length as times vector"
    ms = np.full((mass_losses.shape), initial_mass)
    #np.subtract(ms,mass_losses)
    ms = ms - np.abs(mass_losses)
    ms = ms / initial_mass
    dm = np.gradient(ms)
    dt = np.gradient(times)
    mlrs = dm/dt
    mlrs = 10 * np.abs(mlrs) #round 8
    #mlrs = 1 * np.abs(mlrs) #round 9
    #mlrs = 1000000 * np.abs(mlrs) #round 7
    #mlrs = 100000 * np.abs(mlrs) #round 6
    #mlrs = 10000 * np.abs(mlrs) #round 5
    #mlrs = 100 * np.abs(mlrs) #round 4
    #mlrs = 1000 * np.abs(mlrs) #normal?
    return(mlrs,ms)
    #return(mlrs,ms * 100)

def generate_second_times(times_m):
    """make sure times is a np.array!"""
    return(60.0 * np.array(times_m))

# ##Temp./oC,Time/min,DSC/(mW/mg),Mass loss/mg,DTG/(mg/min),Sensit./(uV/mW),,time (s),Mass/%,g/s
def open_file_and_add_columns(filename, initial_mass):
    print("opening %s\n" % filename)
    have_percentages = True
    new_filename = filename + "_new"
    time = "Time/min"
    temp_name = "##Temp./oC"
    mass_loss = "Mass loss/mg"
    mass_perc = "Mass/%"
    temps = []
    mass_losses = []
    mass_percentages = []
    times_m = []
    with open(filename, 'rb') as infile:
        reader = csv.DictReader(infile)
        if (mass_perc not in reader.fieldnames):
            have_percentages = False
        for row in reader:
            mass_losses.append(row[mass_loss])
            times_m.append(row[time])
            temps.append(row[temp_name])
            if(have_percentages):
                mass_percentages.append(row[mass_perc])

    times_m = np.array(times_m)
    times_m = times_m.astype(np.float)
    mass_losses = np.array(mass_losses)
    mass_losses = mass_losses.astype(np.float)
    times_s = generate_second_times(times_m)
    mlrs,ms = generate_MLR_values(mass_losses, times_s, initial_mass)
    if(not have_percentages):
        mass_percentages = ms

    names = [time, temp_name, mass_loss, mass_perc, "time (s)", "MLR"]
    with open(new_filename, 'wb') as outfile:
        writer = csv.DictWriter(outfile, fieldnames = names)
        writer.writeheader()
        for i in range(len(times_m)):
            writer.writerow({time:times_m[i], temp_name:temps[i], \
                    mass_loss:mass_losses[i], mass_perc:mass_percentages[i], \
                    "time (s)":times_s[i], "MLR":mlrs[i]})
    return(0)

try:
    infile_name = sys.argv[1]
    initial_mass = float(sys.argv[2])
    d = os.getcwd()
    infile = open_file_and_add_columns(d + "/" + infile_name, initial_mass )
except (IndexError) as e:
    print(e)
    print("Usage: python make_MLR.py <filepath> <inital mass>")
except (OSError) as e:
    print(e)
    print("OS couldn't access that file.")
except e:
    print(e)
