import os
import csv
import time
import matplotlib.pyplot as plt

# ##Temp./oC,Time/min,DSC/(mW/mg),Mass loss/mg,DTG/(mg/min),Sensit./(uV/mW),,time (s),Mass/%,g/s
def get_exp_file_data(filename):
    print("opening %s\n" % filename)
    xname = "time (s)"
    yname = "Mass/%"
    x = []
    y = []
    with open(filename, 'rb') as infile:
        reader = csv.DictReader(infile)
        for row in reader:
            x.append(row[xname])
            y.append(row[yname])
    a_zip = [x,y]
    return(a_zip)

# ##Temp./oC,Time/min,DSC/(mW/mg),Mass loss/mg,DTG/(mg/min),Sensit./(uV/mW),,time (s),Mass/%,g/s
def get_sim_file_data(filename):
    print("opening %s\n" % filename)
    xname = "t"
    yname = "002_M/M0( 0.0000_ 0.0000_ 0.0000)"
    x = []
    y = []
    with open(filename, 'rb') as infile:
        reader = csv.DictReader(infile)
        for row in reader:
            x.append(row[xname])
            y.append(float(row[yname])*100)
    a_zip = [x,y]
    return(a_zip)

exp_fname = "PSME_fol_5.csv"
sim_fname = "gpyro_summary_01_0001.csv"
d = os.getcwd()
#expdata = get_exp_file_data(d + "/fix_excel_00/experimental_data/" + exp_fname)
expdata = get_exp_file_data(d + "/" + exp_fname)
#simdata = get_sim_file_data(d + "/optimized_crossed_lines/" + sim_fname)
simdata = get_sim_file_data(d + "/multi_schedule_fit_03/" + sim_fname)


fig = plt.figure(figsize = (12,12))
ax = fig.add_subplot(111)
ax.clear()
ax.plot(expdata[0], expdata[1], color="blue", marker='.', linestyle="None", label="Measured")
ax.plot(simdata[0], simdata[1], color="red", marker='.', linestyle="None", label="Simulated")
#ax.plot(x_points, y5_points, color="red", marker='.', linestyle="None", label="thick fuel (sphere)")
ax.set_title("Mass loss in experiment vs simulation")
ax.set_xlabel("Time (s)")
ax.set_ylabel("Mass remaining (M/M0)")
ax.set_ylim([0, 100])
ax.legend()
fig.canvas.draw()
plt.show(block=True)
