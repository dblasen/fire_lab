import os
import csv
import time
import matplotlib.pyplot as plt

def get_file_data(filename, count):
    '''in this case, it is the last row's second column'''
    print("opening %s\n" % filename)
    xname = "Gen"
    yname1 = "Best fit"
    x = ""
    y1 = ""
    with open(filename, 'rb') as infile:
        reader = csv.DictReader(infile)
        for row in reader:
            x = row[xname]
            y1 = row[yname1]
    a_zip = [count,y1]
    return(a_zip)

def open_all_files(directory, filename, count):
    data = []
    for i in range(1, count):
        data.append(get_file_data(directory%count + filename))
    return(data)

sim_name = "gpyro_summary.csv"
sim_dirname = "/PICO_wood_reaction_count_series/PICO_wood_gpyro_%scount/"

d = os.getcwd()
#data = get_file_data(d + sim_dirname + sim_name)
count = 4
data = open_all_files)d + sim_dirname, sim_name, count)
#will this work?
x = [i[0] for i in data]
y = [i[1] for i in data]

plt.figure(figsize = (12,12))

ax1 = plt.subplot(111)
ax1.plot(x, y, color="blue", marker='.', markersize=2.5, linestyle="None", label="Best Fit")
ax1.set_title("Fitness as function of material count")
ax1.set_xlabel("# Materials (# Parameters)")
ax1.set_ylabel("Fitness")
ax1.grid(linestyle='--', linewidth=0.5)
ax1.legend(loc='lower right')
plt.subplots_adjust(left=0.1, wspace=0.1, top=0.8)
plt.show(block=True)

#Gen,Best fit,Avg fit,nrepop,nnotconverged,var 01,var 02,var 03,var 04,var 05,var 06,var 07,var 08,var 09,var 10,var 11,var 12,
       #0, 0.519708226E+03, 0.519708226E+00,       0,       0, 0.893928012E+12, 0.138392746E+03, 0.488364124E+12, 0.158762941E+03, 0.345541473E+01, 0.341541756E+01, 0.362303463E+03, 0.133617472E+03, 0.217627654E+00, 0.624151741E-01, 0.184015824E+00, 0.323909381E+00,
