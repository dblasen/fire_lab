import os
import csv
import time
import matplotlib.pyplot as plt

# ##Temp./oC,Time/min,DSC/(mW/mg),Mass loss/mg,DTG/(mg/min),Sensit./(uV/mW),,time (s),Mass/%,g/s
def get_exp_file_data(filename):
    print("opening %s\n" % filename)
    xname = "time (s)"
    yname = "Mass/%"
    x = []
    y = []
    with open(filename, 'rb') as infile:
        reader = csv.DictReader(infile)
        for row in reader:
            x.append(row[xname])
            y.append(row[yname])
    a_zip = [x,y]
    return(a_zip)

# ##Temp./oC,Time/min,DSC/(mW/mg),Mass loss/mg,DTG/(mg/min),Sensit./(uV/mW),,time (s),Mass/%,g/s
def get_sim_file_data(filename):
    print("opening %s\n" % filename)
    xname = "t"
    yname = "002_M/M0( 0.0000_ 0.0000_ 0.0000)"
    x = []
    y = []
    with open(filename, 'rb') as infile:
        reader = csv.DictReader(infile)
        for row in reader:
            x.append(row[xname])
            y.append(float(row[yname])*100)
    a_zip = [x,y]
    return(a_zip)

exp_fname_5 = "PICO_wd_5_new.csv"
sim_fname_5 = "gpyro_summary_01_0001.csv"
exp_fname_10 = "PICO_wd_10_new.csv"
sim_fname_10 = "gpyro_summary_01_0002.csv"
exp_fname_20 = "PICO_wd_20_new.csv"
sim_fname_20 = "gpyro_summary_01_0003.csv"
exp_dirname = "/PICO_wood_gpyro/new_experimental_data/"
sim_dirname = "/PICO_wood_gpyro/"

d = os.getcwd()
expdata5 = get_exp_file_data(d + exp_dirname + exp_fname_5)
simdata5 = get_sim_file_data(d + sim_dirname + sim_fname_5)
expdata10 = get_exp_file_data(d + exp_dirname + exp_fname_10)
simdata10 = get_sim_file_data(d + sim_dirname + sim_fname_10)
expdata20 = get_exp_file_data(d + exp_dirname + exp_fname_20)
simdata20 = get_sim_file_data(d + sim_dirname + sim_fname_20)


plt.rc('text', usetex=True)
plt.figure(figsize = (16,16))

ax1 = plt.subplot(221)
ax1.plot(expdata5[0], expdata5[1], color="blue", marker='.', markersize=1.5, linestyle="None", label="Measured")
ax1.plot(simdata5[0], simdata5[1], color="red", marker='.', markersize=1.5, linestyle="None", label="Simulated")
ax1.set_title("5 degree schedule")
ax1.set_xlabel("Time (s)")
ax1.set_ylabel(r'Mass remaining ($\frac{M}{M_{0}}$)')
ax1.set_ylim([0, 100])
ax1.grid(linestyle='--', linewidth=0.5)

ax2 = plt.subplot(222, sharey=ax1)
ax2.plot(expdata10[0], expdata10[1], color="blue", marker='.', markersize=1.5, linestyle="None", label="Measured")
ax2.plot(simdata10[0], simdata10[1], color="red", marker='.', markersize=1.5, linestyle="None", label="Simulated")
ax2.set_title("10 degree schedule")
ax2.set_xlabel("Time (s)")
ax2.set_ylim([0, 100])
ax2.grid(linestyle='--', linewidth=0.5)

ax3 = plt.subplot(223)
ax3.plot(expdata20[0], expdata20[1], color="blue", marker='.', markersize=1.5, linestyle="None", label="Measured")
ax3.plot(simdata20[0], simdata20[1], color="red", marker='.', markersize=1.5, linestyle="None", label="Simulated")
ax3.set_title("20 degree schedule")
ax3.set_xlabel("Time (s)")
ax3.set_ylim([0, 100])
ax3.grid(linestyle='--', linewidth=0.5)
ax3.legend()

plt.subplots_adjust(left=0.1, wspace=0.1, top=0.8)
plt.savefig(d+'/3pane_square.png')
