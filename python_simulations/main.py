import csv

import Heat_Models
import Simulation_Heat_Model
import Analytical_Heat_Model
import Lumped_Capacitance_Model
import Sim_Plot

def sim_driver(m, xmax = 100.0):
    """A basic driver to show off the object's usage"""
    temp_f = 400
    temp_a = 23.5
    h = 2.0
    k = 1.0
    tmax = 10
    sim = Simulation_Heat_Model.Simulated_Heat_Model_1D(temp_f, temp_a, m)
    sim.h = h
    #sim.k = k
    sim.tmax = tmax
    sim.xmax = xmax
    sim.xmin = 0.0
    sim.neumann_bc = "implicit"
    sim.dt = 0.01
    sim.dx = 0.01
    if sim.use_moisture:
        sim.run_simulation_with_moisture()
    else:
        sim.run_simulation()
    return(sim)

def analytical_driver(xs):
    """"""
    temp_f = 400
    temp_a = 20.0
    h = 0.0001
    k = 0.000012
    a = Analytical_Heat_Model.Analytical_Heat_Model_1D(xs, temp_f, temp_a, k, h)
    return(a)

def lcm_driver(xs):
    """"""
    temp_f = 400
    temp_a = 23.5
    h = 2.0
    l = Lumped_Capacitance_Model.LCM_Heat_Model_1D(xs, temp_f, temp_a, h)
    return(l)

def ignition_comparison(radius_vals, ignition_temp=327):
    errors = []
    for radius in radius_vals:
        sim_model = sim_driver(radius)
        lcm_model = lcm_driver(sim_model.coordinates)
        lcm_model.xs = sim_model.coordinates
        sim_model.run_simulation()
        sim_result = 0
        for i in range(0, len(sim_model.mesh_history)):
            if(sim_model.mesh_history[i][0] > ignition_temp):
                print(sim_model.mesh_history[i][0])
                sim_result = sim_model.ts[i]
                break
        l_vals = []
        lcm_result = None
        for timestep in sim_model.ts:
            #print(lcm_model.temperature_at_time(timestep)[0])
            l_val = lcm_model.temperature_at_time(timestep)
            l_vals.append(l_val)
            if(l_val[0] > ignition_temp and lcm_result == None):
                lcm_result = timestep
                #break
        print(sim_result, lcm_result)
        #Sim_Plot.chase_plot(sim_model.mesh_history, l_vals, sim_model.coordinates, sim_model.ts, "Albini", "LCM", 0.00)
        diff = abs(sim_result - lcm_result)
        errors.append(diff)
    Sim_Plot.lcm_sim_error(radius_vals, errors, "Albini", "Thin Fuels")

def fds_driver(filepath):
    """Load in a DEVC csv file to compare with our solutions."""
    values = []
    with open(filepath, 'r') as f:
        f.next()
        reader = csv.DictReader(f)
        for row in reader:
            values.append(row)
    frames = []
    for timestep in values:
        frame = {}
        frame["t"] = float(timestep["Time"])
        xs = []
        ys = []
        for k, v in timestep.iteritems():
            if(k == "Time"): continue
            num = float(k) #
            xs.append(num)
            ys.append(float(v))
        frame["temps"] = zip(xs,ys)
        frame["temps"] = sorted(frame["temps"], key = lambda x: x[0]) #sort into ascending x-axis order
        frames.append(frame)
    return(frames)

def main():
    #sim2 = sim_driver("basic")
    #sim2 = sim_driver("advanced")
    #sim = sim_driver(None)
    fpath = "fds_runs/thick_wall2/thick_wall_devc.csv"
    fds_points = fds_driver(fpath)

    xs = list(zip(*fds_points[0]["temps"])[0]) #unpack first entry into list of tuples (?)
    ts = [dictionary['t'] for dictionary in fds_points]
    fds_vals = [list(zip(*dictionary['temps'])[1]) for dictionary in fds_points]
    #fds_vals = [list(dictionary['temps'])[1] for dictionary in fds_points]
    a = analytical_driver(xs)
    a_vals = a.analytical_temperature_across_interval(ts)
    Sim_Plot.chase_plot(a_vals, fds_vals, xs, ts, "analytical", "FDS", 0.1)
    #Sim_Plot.diff_plot(sim.mesh_history, sim2.mesh_history, sim.coordinates, sim.ts, 0.0)
    #lcm = lcm_driver(sim.coordinates)
    #l_vals = lcm.temperature_across_interval(sim.ts)
    #radius_vals = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0]
    #ignition_comparison(radius_vals) 
    #Sim_Plot.chase_plot(sim.mesh_history, l_vals, sim.coordinates, sim.ts, "Albini", "LCM", 0.01)
    #Sim_Plot.diff_plot(sim.mesh_history, a_vals, sim.coordinates, sim.ts, 0.01)



if __name__ == "__main__":
    main()
