import os
import sys
import csv
import time
import re
import argparse

def open_fds_readfile(path):
    """Open the file and read its contents in order to parse.
    Parameters:
        path: a file path (string)
    Returns:
        lines: a list of the file's lines.
    """
    try:
        with open(path,'r') as f:
            lines = f.readlines()
        return(lines)
    except:
        print("In readfile, hit error: ",sys.exc_info()[0])
        raise

def open_fds_writefile(path,lines):
    """Open the file, write in our content, and save.
    Parameters:
        path: a file path (string)
        lines: the completed file contents (list of strings)
    Returns:
        status code (int)
    """
    try:
        with open(path,'w+') as f:
            for line in lines:
                f.write(line)
    except:
        print("In writefile, hit error: ",sys.exc_info()[0])
        raise
    return 0

def make_obst_from_tokens(token_queue):
    """use the tree tokens to make an obst entry for them."""
    new_lines = []
    in_xyz = False
    in_crown_width = False
    in_crown_base = False
    token_str = "".join(token_queue)
    # using regex to protect us against bad formatting; make those expressions!
    re_xyz = re.compile('XYZ\s*=\s*\-?\d+\.?\d*\s*,\s*\-?\d+\.?\d*\s*,\s*\-?\d+\.?\d*', flags=re.IGNORECASE)
    re_label = re.compile('LABEL\s*=\s*[\'\"].*[\'\"]', flags=re.IGNORECASE) #TODO fix this one
    re_tree_height = re.compile('TREE_HEIGHT\s*=\s*\d*\.?\d*', flags=re.IGNORECASE)
    re_crown_width = re.compile('CROWN_WIDTH\s*=\s*\d*\.?\d*', flags=re.IGNORECASE)
    # Now use those suckers.
    xyz = re.findall(re_xyz, token_str)
    label = re.findall(re_label, token_str)
    tree_height = re.findall(re_tree_height, token_str)
    crown_width = re.findall(re_crown_width, token_str)
    if(len(xyz) == 0):
        print("No XYZ found; this must be a shrub or something.")
        return(0)
    # Now generate the values of our new obstruction, i guess
    # get x,y,z from xyz
    coord_value = re.compile('\-?\d+\.?\d*')
    coordinates = re.findall(coord_value, xyz[0])
    x = coordinates[0]
    y = coordinates[1]
    z = coordinates[2]
    # get the label string
    label_value = re.compile('[\'\"].*[\'\"]')
    label_str = re.findall(label_value, label[0])
    label_str = re.sub('\'','',label_str[0])
    # get the tree's height
    tree_height_value = re.compile('\d+\.?\d*')
    tree_height_str = re.findall(tree_height_value, tree_height[0])
    tree_height_str = tree_height_str[0]
    # get the crown width
    crown_width_value = re.compile('\d+\.?\d*')
    crown_width_str= re.findall(crown_width_value, crown_width[0])
    crown_width_str= crown_width_str[0]

    #now to make our new values
    #NOTE we need to create our own &MATL it seems; &TREE doesn't use it...
    #our &MATL is a stand in; we can change it later if we choose.
    matl_id = label_str + "_fir"
    matl_str = "&MATL ID='%s', DENSITY=600, CONDUCTIVITY=0.12, SPECIFIC_HEAT=2.7 /\n" % (matl_id)
    # our &SURF describes the boundary condition of something in the fire environment
    surf_id = label_str + "_heated"
    obst_thickness = 0.3 # why? because it just is. #TODO how thick is a fucking tree? lol
    surf_str = "&SURF ID='%s', MATL_ID='%s', THICKNESS(1)=%f /\n" % (surf_id, matl_id, obst_thickness)
    #TODO may need a different &SURF for the top of the tree!
    #obst_width = obst_thickness #let's see if we can ignore width and length
    #obst_length = float(tree_height_str)
    obst_id = label_str + "_trunk"
    obst_xmin = float(x) - obst_thickness/2
    obst_xmax = float(x) + obst_thickness/2
    obst_ymin = float(y) - obst_thickness/2
    obst_ymax = float(y) + obst_thickness/2
    obst_zmin = float(z)
    obst_zmax = float(z) + float(tree_height_str) * 0.75 #shouldn't be full height...
    obst_str = "&OBST ID='%s', XB=%f,%f,%f,%f,%f,%f, SURF_ID6='%s','%s','%s','%s','%s','%s',COLOR='BROWN' /\n" \
            % (obst_id, obst_xmin, obst_xmax, obst_ymin, obst_ymax, obst_zmin, obst_zmax, \
               surf_id, surf_id, surf_id, surf_id, surf_id, surf_id)
    new_lines.append(matl_str)
    new_lines.append(surf_str)
    new_lines.append(obst_str)
    # Make some devc to examine heat transfer
    #surface of obst
    surf_devc_str = "&DEVC ID='%s', DEPTH=0.0, QUANTITY='WALL TEMPERATURE', XYZ=%f,%f,%f, IOR=-1 /\n" %(obst_id + "_0", obst_xmin, float(y), obst_zmax-1)
    new_lines.append(surf_devc_str)
    #inside the obst
    min_depth = 0.01
    max_depth = 0.10
    inc_depth = 0.01
    depth = min_depth
    while depth <= max_depth:
        devc_label = obst_id + "_" + str(depth)
        devc_x = obst_xmin
        devc_y = float(y)
        devc_z = obst_zmax - 1 #arbitrary height near the top
        devc_str = "&DEVC ID='%s', XYZ=%f,%f,%f, QUANTITY='INSIDE WALL TEMPERATURE', DEPTH=%f, IOR=-1 /\n" % (devc_label, devc_x, devc_y, devc_z, depth)
        new_lines.append(devc_str)
        depth += inc_depth
    return(new_lines)

#&MATL ID='fir', DENSITY=600, CONDUCTIVITY=0.12, SPECIFIC_HEAT=2.7 / -http://www.firebid.umd.edu/material-database.php (fir)
#&TREE PART_ID='TREE VEG',XYZ=4,-1.5,0,FUEL_GEOM='CONE',
#          CROWN_WIDTH=3,CROWN_BASE_HEIGHT=1,TREE_HEIGHT=6,
#          OUTPUT_TREE=.TRUE.,LABEL='tree_veg' /
#&OBST ID='wood_wall_f', XB=0.2,0.4,0,0.1,0.2,4, THICKEN=.TRUE. ,
#    SURF_ID6='heated','INERT','INERT','INERT','INERT','INERT' /
#    SURF_ID='heated'/
#&SURF ID='heated', MATL_ID='fir', THICKNESS(1)=3.8, GEOMETRY='CARTESIAN', LENGTH=0.1, WIDTH=0.2 /
#-&DEVC ID='0.000', DEPTH=0.0, QUANTITY='WALL TEMPERATURE', XYZ=0.3,0.1,0.2, IOR=-1 / 
#-&DEVC ID='0.001', XYZ=0.3,0.1,0.2, QUANTITY='INSIDE WALL TEMPERATURE', DEPTH=0.001, IOR=-1 / 

def parse_lines(lines):
    """Read through lines of input file and add &OBST wherever we have &TREEs
    Parameters:
        lines: a list of the file's lines (strings)
    Returns:
        new_lines: a list of the new file's lines (strings)
    """
    new_lines = []
    token_queue = [] #we will pile pieces of &TREE lines here.
    in_tree = False #track &TREE across multiple lines
    for line in lines:
        if ((not in_tree) and ("&TREE" not in line[0:5])): #0 interest to us, paste it in and move on.
            new_lines.append(line)
        elif((in_tree) or ("&TREE" in line[0:5])): #reading a multi-line &TREE OR a new tree?
            new_lines.append(line) #we still need this line, so write it in
            tokens = line.split() #NOTE this implicitly splits spaces and \n, maybe more
            token_queue += tokens
            if tokens[-1] == '/': #end of &TREE, so time to write our &OBST #TODO this could have issues!
                in_tree = False
                obst_lines = make_obst_from_tokens(token_queue)
                new_lines = new_lines + obst_lines #concatenate obst lines onto our file in place
                token_queue = [] #empty this thing out
            else: #there's another line, iterate and collect more tokens
                in_tree = True
        else:
            print("Something went very wrong here: " + line)
            raise
    return(new_lines)

def main(infile, outfile="new.fds"):
    lines = open_fds_readfile(infile)
    new_lines = parse_lines(lines)
    open_fds_writefile(outfile, new_lines)
    return(0)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Add thick fuels to WFDS elements of &TREE namespace.")
    parser.add_argument('infile', nargs='?', type=str)
    parser.add_argument('outfile', nargs='?',type=str)
    commands = parser.parse_args();
    if (commands.infile == None):
        parser.print_help()
    else:
        main(commands.infile)

