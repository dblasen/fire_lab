import abc
from fenics import *
from scipy.special import erfc
import numpy as np
import matplotlib.pyplot as plt

class Heat_Model:
    """Abstract class for defining new heat models. ABC should stop this from being used."""
    __metaclass__ = abc.ABCMeta

    def __init__(self, temp_f, temp_a):
        self.temp_F = temp_f
        self.temp_A = temp_a
