from fenics import *
from scipy.special import erfc
import numpy as np

import Heat_Models

class Simulated_Heat_Model_1D (Heat_Models.Heat_Model):
    """Simulates a 1D heat equation with Albini's BC.

    Attributes:
        temp_F (float): Fire temperature of the simulation in degrees C (default = 500.0)
        temp_A (float): Ambient temperature of the simulation in degrees C (default = 23.5)
        dx (float): size of a given finite element on the mesh (1e-4 ~ 1.0)
        dt (float): size of time step (1e-4 ~ 1.0)
        xmax (float): maximum depth of the simulation; used to make mesh.
        xmin (float): minimum depth of the simulation; used to make mesh. (default = 0.0)
        h (float): Newtonian heat film coefficient (default = 2.0)
        k (float): Medium heat transfer coefficient (default = 8.0) TODO: wtf is this
        neumann_bc (string): describes how surface boundary condition is applied to the simulation;
            can be either "explicit" or "implicit".
        tmax (float): the maximum duration of the simulation in seconds.
        coordinates (float[]): a list of all x positions in the simulation
        ts (float[]): a list of all time points in the simulation
        mesh_history (:obj:`list` of :obj:`numpy.ndarray`): all points in the simulation at each time
            step. depending on overhead, this feature may be dropped in the future.

    Internal Attributes: (please don't touch these)
        _dx (Measure): FEniCS object that describes integral of whole simulation
        _ds (Measure): FEniCS object that describes integral of boundary condition
        _u_n (Function): FEniCS object that holds initial simulation state (ambient temp)
        _u (Function): FEniCS object that serves as our Trial function
        _v (Function): FEniCS object that serves as Test function

    """
    def __init__(self, temp_f = 500.0, temp_a = 23.5, use_moisture = False):
        """Initialize the object with its fire and ambient temperatures. """
        Heat_Models.Heat_Model.__init__(self, temp_f, temp_a)
        self.tol= 1e-12
        self.mesh_history = []
        self.use_moisture = use_moisture
        set_log_level(30)

        #NOTE this is all from simulation methods; need to pull it into init!
        k_0 = 0.06 #something like this; check wiki for thermal conductivity numbers
        rho_w = 0.5 #wood dry density (kg / m^3)
        epsilon = 0.95  #wiki
        sigma = 5.67e-8 #wiki
        self.dv = 1 #variation in volume (todo do we need this yet?)
        self.m = Constant(30.0) #moisture fraction of weight
        self.k = Expression("k_0 + 4.27e-4 * rho_w * m", k_0=k_0, rho_w = rho_w, m=self.m, degree = 1) #albini k derivation


    def run_simulation_with_moisture(self):
        """
        Run the simulation with the specified parameters.
        Each iteration will be stored in the object for plotting purposes.
        Includes moisture-dependence using a second degree of freedom on the mesh.
        """
        # build some local variables; clear ts and mesh_history
        self.mesh_history = []
        self.ts = []
        nx = int((self.xmax - self.xmin) / self.dx)
        temp_f = Constant(self.temp_F)
        temp_a = Constant(self.temp_A)
        h = self.h *0.01
        k = self.k
        dt = self.dt

        # Create mesh and define function space
        #TODO may want to move mesh into the class variables and allow assignment
        #TODO e.g. "burn this object, not one you make yourself"
        mesh = IntervalMesh(nx, self.xmin, self.xmax)

        # element for temperature
        P1 = FiniteElement('CG', "interval", 1)
        # element for moisture
        P2 = FiniteElement('CG', "interval", 1)
        # make mixed element
        elt = MixedElement([P1, P2])
        # make function space using the mixed element
        V = FunctionSpace(mesh, elt)

        # Obtain the coordinates of the mesh
        self.coordinates = V.tabulate_dof_coordinates()[::-1]

        # Define initial value, variational problem
        v1, v2 = TestFunctions(V)
        u = TrialFunction(V)
        u_k, u_m = split(u)
        u_kn = interpolate(temp_a, V.sub(0).collapse()) #initial value: all at ambient temp
        u_mn = interpolate(self.m, V.sub(1).collapse()) #initial value: all at starting moisture
        self.current_temp = u_kn

        # Set up domains for neumann  and dirichlet boundary conditions
        domain_markers = CellFunction('size_t', mesh) #describe full domain (is this needed?)
        boundary_markers = FacetFunction('size_t', mesh)
        surface = Surface_1D(self.xmin) #surface = neumann
        surface.mark(boundary_markers, 0)
        interior = Internal_1D(self.xmax) #interior = dirichlet
        interior.mark(boundary_markers, 1)
        bc_k = DirichletBC(V.sub(0), temp_a, boundary_markers, 1) #fenics doing fenics stuff

        # Describe the domain for ds and dx (neumann boundary and full domain, respectively)
        _ds = Measure('ds', domain=mesh, subdomain_data=boundary_markers)
        _dx = Measure('dx', domain=mesh, subdomain_data=domain_markers)

        if(self.neumann_bc == "explicit"):
            self.F = (((u_k-u_kn)*v1)/dt)*_dx + ((inner(k*grad(u_k),grad(v1))) * _dx) \
                    - ((h *(temp_f - u_kn))* v1) * _ds(0)
        elif(self.neumann_bc == "implicit"):
            self.F = (((u_k-u_kn)*v1)/dt)*_dx + ((inner(k*grad(u_k),grad(v1))) * _dx) \
                    - ((h *(temp_f - u_k)) * v1) * _ds(0)
        else:
            raise("hit an unknown neumann boundary condition %s." % (self.neumann_bc))
        if(self.use_moisture == "basic"):
            #use a simple approximation of moisture content like albini
            Tb = 100
            r_ev = 1 * dt
            greater_Tb = conditional( gt(u_kn, Tb), 1.0, 0.0 )
            not_dry = conditional( gt(u_mn, 0.0+self.tol), 1.0, 0.0 )
            dryness = ((u_m - u_mn)/dt + greater_Tb * not_dry * r_ev)
            self.G = dryness * v2 * _dx
            self.F = self.F + self.G
        elif(self.use_moisture == "advanced"): #NOTE advanced not ready; see Alves eq. 3!
            self.G = ((u_m - u_mn) * v2 / dt)*_dx \
                    + (h * (u_kn - temp_f) + sigma * epsilon * (u_kn**4 - temp_f**4) * v2) * _ds(0)
            self.F = self.F + self.G
        else:
            self.F = self.F
        a, L = lhs(self.F), rhs(self.F)

        # Time-stepping
        u = Function(V) #re-declare u as per docs
        t = 0.00 #+ dt
        diffs = []

        while (t < self.tmax):
            solve(a == L, u) # Compute solution
            u_k, u_m = u.split(deepcopy=True)
            values = np.array(Function.vector(u_k))[::-1] # Get the values from the fenics Function
            values_m = np.array(Function.vector(u_mn))[::-1] # Get the values from the fenics Function
            #print(t,values[0], values_m[0])
            u_kn.assign(u_k) # Update previous solution (temp)
            self.current_temp.assign(u_k)
            u_mn.assign(u_m) # Update previous solution (moisture)
            m = u_mn
            k.user_parameters["m"] = m
            #print(float(k.user_parameters["m"]))
            # Append time, mesh values to list for plotting
            self.ts.append(t)
            self.mesh_history.append(values)
            #print('t = %.2f; surface temp = %.2f;' % (t,u(0)))
            t += dt # Update current time
        return(0)


    def run_simulation(self):
        """
        Run the simulation with the specified parameters.
        Each iteration will be stored in the object for plotting purposes.
        """
        # build some local variables; clear ts and mesh_history
        self.mesh_history = []
        self.ts = []
        nx = int((self.xmax - self.xmin) / self.dx)
        temp_f = Constant(self.temp_F)
        temp_a = Constant(self.temp_A)
        h = self.h *0.01
        k = self.k
        dt = self.dt

        # Create mesh and define function space
        #TODO may want to move mesh into the class variables and allow assignment
        #TODO e.g. "burn this object, not one you make yourself"
        mesh = IntervalMesh(nx, self.xmin, self.xmax)
        V = FunctionSpace(mesh, 'CG', 1)

        # Obtain the coordinates of the mesh
        self.coordinates = V.tabulate_dof_coordinates()[::-1]

        # Define initial value, variational problem
        u_n = interpolate(temp_a, V) #initial value: all at ambient temp
        self.current_temp = u_n
        u = TrialFunction(V)
        v = TestFunction(V)

        # Set up domains for neumann  and dirichlet boundary conditions
        domain_markers = CellFunction('size_t', mesh) #describe full domain (is this needed?)
        boundary_markers = FacetFunction('size_t', mesh)
        surface = Surface_1D(self.xmin) #surface = neumann
        surface.mark(boundary_markers, 0)
        interior = Internal_1D(self.xmax) #interior = dirichlet
        interior.mark(boundary_markers, 1)
        bc = DirichletBC(V, temp_a, boundary_markers, 1) #fenics doing fenics stuff

        # Describe the domain for ds and dx (neumann boundary and full domain, respectively)
        _ds = Measure('ds', domain=mesh, subdomain_data=boundary_markers)
        _dx = Measure('dx', domain=mesh, subdomain_data=domain_markers)

        #NOTE using _ds(0) will make heat flux only affect left boundary
        if(self.neumann_bc == "explicit"):
            self.F = (((u-u_n)*v)/dt)*_dx + ((inner(k*grad(u),grad(v))) * _dx) - ((h *(temp_f - u_n))* v) * _ds
        elif(self.neumann_bc == "implicit"):
            self.F = (((u-u_n)*v)/dt)*_dx + ((inner(k*grad(u),grad(v))) * _dx) - ((h *(temp_f - u)) * v) * _ds
        else:
            raise("hit an unknown neumann boundary condition %s." % (self.neumann_bc))
        a, L = lhs(self.F), rhs(self.F)

        # Time-stepping
        u = Function(V) #re-declare u as per docs
        u.assign(temp_a)
        self.current_temp.assign(u)
        #self.mesh_history.append(np.array(Function.vector(u))[::-1])
        t = 0.00 #+ dt
        diffs = []
        while (t < self.tmax):
            #solve(a == L, u, bc) # Compute solution
            solve(a == L, u)
            values = np.array(Function.vector(u))[::-1] # Get the values from the fenics Function
            u_n.assign(u) # Update previous solution
            self.current_temp.assign(u)
            # Append time, mesh values to list for plotting
            self.ts.append(t)
            self.mesh_history.append(values)
            #print('t = %.2f; surface temp = %.2f;' % (t,u(0)))
            t += dt # Update current time
        return(0)

    def initialize_moisture_advanced(self, V, _dx, _ds, dt):
        self.rho_w = 0.5 #wood dry density (kg / m^3)
        self.epsilon = 0.95  #wiki
        self.sigma = 5.67e-8 #wiki
        self.dv = 1 #variation in volume (todo do we need this yet?)
        return(0)


# Object to describe Neumann BC
class Surface_1D(SubDomain):
    def __init__(self, xmin, tol = 1e-12):
        self.xmin = xmin
        self.tol = tol
        SubDomain.__init__(self)

    def inside(self, x, on_boundary):
        return(near(x[0], self.xmin, self.tol) and on_boundary) #surface boundary gets heat

# Object to describe the Dirichlet BC
class Internal_1D(SubDomain):
    def __init__(self, xmax, tol=1e-12):
        self.xmax=xmax
        self.tol=tol
        SubDomain.__init__(self)

    def inside(self, x, on_boundary):
        return(near(x[0], self.xmax, self.tol) and on_boundary)
