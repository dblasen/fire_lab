from scipy.special import erfc
import numpy as np

import Heat_Models

class LCM_Heat_Model_1D:
    """
    Finite solid following Lumped Capacitance Model.

    Attributes:
        temp_F (float): Fire temperature of the simulation in degrees C (default = 500.0)
        temp_A (float): Ambient temperature of the simulation in degrees C (default = 23.5)
        h (float): Newtonian heat film coefficient (default = 2.0)
        xs (float[]): a list of all x positions in the model
        A_s (float): surface area of the object
        rho (float): mass density
        V (float): volume
        c (float): specific heat
    """
    def __init__(self, xs, temp_f, temp_a, h):
        self.xs = xs
        self.h = h
        self.temp_F = temp_f
        self.temp_A = temp_a
        self.generate_V()
        #construct these later?
        self.c = 1.76 #google
        self.rho = 0.5 #google
        self.A_s = 1 #?

    def generate_V(self):
        """generate the volume used in the LCM heat calculation.
        NOTE: using volume on a 1D model... ugh
        Parameters: None
        Returns: self.V (float): Volume of the object.
        """
        x_len = (self.xs[-1] - self.xs[0])
        y_len = 1
        z_len = 1
        self.V =  x_len * y_len * z_len
        return(self.V)

    def temperature_at_time(self, t):
        """
        get the current object temperature of the lumped capacitance model.
            -built from equation 5.6 in bergman et al, p.282
            -behavior looks appropriate, but some tweaking of constants is needed
        Parameters:
            t:  time iteration (float)
        Returns:
            vals:  list of temperatures (floats)
        """
        temp_f = self.temp_F
        temp_a = self.temp_A
        h = self.h
        A_s = self.A_s
        rho = self.rho
        V = self.V
        c = self.c
        val = temp_f + (temp_a - temp_f) * np.exp(-1 * t * ((h * A_s) / (rho * V * c)))
        vals = [val for x in self.xs]
        return(vals)

    def temperature_across_interval(self, ts):
        """
        Generate analytical solutions for each t.

        Parameters:
            ts (float[]): a list of times at which the solution will be evaluated.

        Returns:
            solutions (list of float[]): list of temperatures at each point at each time step.
        """
        solutions = []
        for t in ts:
            solution = self.temperature_at_time(t)
            solutions.append(solution)
        return(solutions)
