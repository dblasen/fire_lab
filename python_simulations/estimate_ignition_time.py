import sys
import csv
import Analytical_Heat_Model
import numpy as np

def analytical_driver(xs, rho, C, temp_f, temp_a, h, M, k):
    """note: first half (wet wood)"""
    #rho = 588
    #C = 2.85
    #temp_f = 600
    #temp_a = 20.0
    #h = 2
    #M = 0.1735
    #k = 0.195
    a = Analytical_Heat_Model.Analytical_Heat_Model_1D(xs, temp_f, temp_a, k, h)
    a.albini_params(rho, C, M)
    #a.generate_alpha()
    a.alpha = a.generate_albini_alpha()
    return(a)

def adjust_k(M, k_0, rho_0):
    '''k is dependent on moisture content
    so we need to be able to recalculate for
    high/low moisture.'''
    k = k_0 + 4.27E-4 * rho_0 * M
    return(k)

def adjust_rhoC(rho_0, C_0, M):
    '''rhoC is dependent on moisture content
    so we need to be able to recalculate for
    high/low moisture.'''
    C_w = 4.186 # J/(g*C)
    C = (C_0 + C_w * M)
    return(C)

def make_effective_h(gas_temp, surf_temp):
    C = 1.31
    diff = gas_temp - surf_temp
    ex = np.cbrt(diff)
    h = C * ex
    return(h)

def get_model_data(pointfile):
    with open(pointfile, 'r') as f:
        values = []
        # skip first line (units -> s, C, C, C, ... C)
        f.next()
        reader = csv.DictReader(f)
        for row in reader:
            values.append(row)
        times = []
        surf_temps = []
        fluxes = []
        air_temps = []
        initial_mass = 0.0 #will write over this in loop
        for timestep in values:
            time = float(timestep["Time"])
            times.append(time)
            temp = float(timestep["surf_temp"])
            surf_temps.append(temp)
            flux = float(timestep["total_flux"])
            fluxes.append(flux)
            air_temp = float(timestep["air_temp"])
            air_temps.append(air_temp)
    return([times,surf_temps,fluxes,air_temps])

# it occurs to me that there is no actual timestep in the analytical model.
# so pulling out the current air temperature is really fucking with the
# analytical model because it really assumes that temperature is constant.
# hmm.
def get_h_at_t(t, model_data, surf_temp=None):
    i = 0
    if surf_temp:
        model_surf_temp = surf_temp
    for i in range(len(model_data[0])):
        #print(model_data[0][i], t, model_data[0][i+1], ((model_data[0][i] >= float(t)) and (model_data[0][i+1] <= float(t))))
        if ((model_data[0][i] <= float(t)) and (model_data[0][i+1] >= float(t))):
            if not surf_temp:
                model_surf_temp = model_data[1][i]
            model_flux = model_data[2][i]
            temp_f = model_data[3][i]
            break
    #h = (t_inf - model_surf_temp) / model_flux
    h = abs(model_flux / (temp_f - model_surf_temp))
    #print(h)
    return(h, temp_f)

def get_average_flux(data, ignition_temp = 350):
    '''
    just get the average flux for use as a constant
    but probably just the flux until ignition temperature...
    '''
    pre_ignition_fluxes = []
    for temp,flux in zip(data[1],data[2]):
        if temp < ignition_temp:
            pre_ignition_fluxes.append(flux)
        else:
            break
    return(np.average(pre_ignition_fluxes))

def get_ignition_time(pointfile):
    '''
    according to albini, the rate of heat transfer to the
    fuel changes once the surface reaches drying temperature.

    is this just changes to C and k or should h also
    be affected? for that matter, what values of h even
    make sense??'''
    model_data = get_model_data(pointfile)
    if('albini_5' in pointfile):
        xmax = 0.024
        M = 0.1735
    elif('albini_6' in pointfile):
        xmax = 0.0503
        M = 0.0385
    elif('albini_7' in pointfile):
        xmax = 0.0503
        M = 0.3750
    else:
        raise(Exception("No valid albini model data inputted."))
    #start driver
    ignition_time = 0
    ignition_temp = 350
    dry_temp = 100
    xmin = 0
    #xmax = 0.0503
    xnum = 100
    rho = 588
    C_0 = 2.85
    C = adjust_rhoC(rho, C_0, M)
    temp_f = 500
    temp_a = 20.0
    h = 2
    k = 1.195
    dry_fraction_rise = dry_temp / temp_a
    ignite_fraction_rise = ignition_temp / temp_a
    xs = np.linspace(xmin, xmax, xnum)
    a = analytical_driver(xs, rho, C, temp_f, temp_a, h, M, k)
    #a.a = a.generate_albini_alpha()
    #heat from ambient to drying
    time = 1
    dry = False
    vals = [temp_a]
    while not dry:
        a.h = make_effective_h(temp_f, vals[0])
        #a.h, _ = get_h_at_t(time,model_data)
        vals = a.analytical_temperature_at_time(time)
        surf_fraction = vals[0] / temp_a
        if surf_fraction > ignite_fraction_rise:
            dry_time = time
            print(dry_time)
            dry = True
        else:
            time += 1
    #heat from dry to ignition
    C = adjust_rhoC(rho, C_0, 0)
    k = adjust_k(M, k, rho)
    a = analytical_driver(xs, rho, C, temp_f, dry_temp, h, M, k)
    #time = 1
    burning = False
    vals = [dry_temp]
    while not burning:
        a.h = make_effective_h(temp_f, vals[0])
        #a.h, _ = get_h_at_t(time,model_data)
        vals = a.analytical_temperature_at_time(time)
        surf_fraction = vals[0] / temp_a
        if surf_fraction > ignite_fraction_rise:
            ignition_time = time - ignition_time
            burning = True
        else:
            time += 1
    print(dry_time + ignition_time)
    print(vals)
    return(dry_time + ignition_time)

def get_ignition_time_albini_model(pointfile, xmax = None):
    '''
    according to albini, the rate of heat transfer to the
    fuel changes once the surface reaches drying temperature.
    is this just changes to C and k or should h also
    be affected? for that matter, what values of h even
    make sense??'''
    #albini and reinhardt used a scalar to establish proportionality
    #so we're doing it too
    regression_scalar = 1.00
    model_data = get_model_data(pointfile)
    q = get_average_flux(model_data)
    if('albini_5' in pointfile):
        if(xmax == None):
            xmax = 0.024
        M = 0.1735
    elif('albini_6' in pointfile):
        if(xmax == None):
            xmax = 0.0503
        M = 0.0385
    elif('albini_7' in pointfile):
        if(xmax == None):
            xmax = 0.0503
        M = 0.3750
    else:
        raise(Exception("No valid albini model data inputted."))
    ignition_time = 0
    ignition_temp = 350
    dry_temp = 100
    xmin = 0
    xnum = 100
    rho = 588
    C_0 = 2.850
    C = C_0
    C = adjust_rhoC(rho, C_0, M)
    temp_f = 650
    temp_a = 20.0
    h = 0.5
    k = 0.000125
    dry_fraction_rise = dry_temp / temp_a
    ignite_fraction_rise = ignition_temp / temp_a
    xs = np.linspace(xmin, xmax, xnum)
    a = analytical_driver(xs, rho, C, temp_f, temp_a, h, M, k)
    a.q = q
    time = 1
    dry = False
    vals = [temp_a]
    while not dry:
        vals = a.constant_surface_flux_at_time(time)
        surf_fraction = vals[0] / temp_a
        if surf_fraction > dry_fraction_rise:
            dry_time = time
            dry = True
        else:
            time += 1
    #C = adjust_rhoC(rho, C_0, 0)
    C = adjust_rhoC(rho, C_0, M)
    #k = adjust_k(M, k, rho)
    a = analytical_driver(xs, rho, C, temp_f, dry_temp, h, M, k)
    a.q = q
    time = 1
    burning = False
    vals = [dry_temp]
    while not burning:
        vals = a.constant_surface_flux_at_time(time)
        surf_fraction = vals[0] / temp_a
        if surf_fraction > ignite_fraction_rise:
            ignition_time = time
            burning = True
        else:
            time += 1
    return(regression_scalar * (dry_time + ignition_time), vals)


def main():
    pointfile = sys.argv[1]
    if (len(sys.argv) > 2):
        xmax = float(sys.argv[2])
    else:
        xmax = None
    get_ignition_time_albini_model(pointfile, xmax)
    #get_ignition_time(pointfile)

if __name__ == '__main__':
    main()
