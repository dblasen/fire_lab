import matplotlib.pyplot as plt
import numpy as np
import time

def chase_plot(values1, values2, xs, ts, name1="1", name2="2", delay=0.1):
    """
    generate a time-iterating plot of albini and analytical temps across x.

    Parameters:
        sim_values (float[]): values from the simulation.
        a_values (float[]): values from the analytical solution
        xs (float[]): x coordinates for the solutions
        ts (float[]): times at which the solutions are evaluated
        delay (float): sleep time between plots.
    """
    fig = plt.figure(figsize = (12,12))
    ax = fig.add_subplot(111)
    for i in range(0,len(ts)):
        ax.clear()
        ax.plot(xs, values1[i], color="red", label = name1)
        ax.plot(xs, values2[i], color="blue", label = name2)
        ax.set_title("%s vs. %s Heat Equations, t = %s" % (name1, name2, ts[i]))
        ax.set_xlabel("Distance into halfspace")
        ax.set_ylabel("Degrees C")
        ax.legend()
        fig.canvas.draw()
        fig.show()
        time.sleep(delay)

def diff_plot(values1, values2, xs, ts, name1="1", name2="2", delay=0.1):
    """
    generate a time-iterating difference plot across x.

    Parameters:
        sim_values (float[]): values from the simulation.
        a_values (float[]): values from the analytical solution
        xs (float[]): x coordinates for the solutions
        ts (float[]): times at which the solutions are evaluated
        delay (float): sleep time between plots.
    """
    fig = plt.figure(figsize = (12,12))
    ax = fig.add_subplot(111)
    for i in range(0,len(ts)):
        ax.clear()
        ax.plot(xs, values1[i] - values2[i], color="green", label = "%s - %s" % (name1,name2))
        ax.set_title("Difference between Models, t = %s" %ts[i] )
        ax.set_xlabel("Distance into halfspace")
        ax.set_ylabel("Degrees C")
        ax.legend()
        fig.canvas.draw()
        fig.show()
        time.sleep(delay)

def lcm_sim_error(rs, errors, name1, name2):
    fig = plt.figure(figsize = (12,12))
    ax = fig.add_subplot(111)
    ax.clear()
    ax.plot(rs, errors, color="green", label = "%s - %s" % (name1,name2))
    ax.set_title("Difference in Ignition temperatures at Different Radii")
    ax.set_xlabel("Radius (cm)")
    ax.set_ylabel("Ignition time (s)")
    ax.legend()
    fig.canvas.draw()
    fig.show()
    fig.savefig('sim_vs_lcm.png')

"""
def compare_FEM_to_analytical(dx, dt, neumann_bc="explicit", chase_plot=False, \
        range_error=False, diff_plot=False, surf_plot=False):
    Perform a FEniCS simulation of the heat equation with Albini's boundary condition
    and compare its results against an analytical solution to get the error.

    Parameters:
        dx:  the distance between elements on the x-axis (float)
        dt:  the distance between elements in time (float)
        neumann_bc:  "implicit" uses the surface temperature of the current iteration (u)
                     "explicit" uses the surface temperature of the previous iteration (u_n)
        chase_plot:  show the temperature across the simulation as it runs (both FEM and analytical)
        range_error:  show the error range across the simulation at its end. NOTE: buggy as hell.
    delta_x = dx
    nx = int((x_max - x_min) / dx)

    # Temperature variables
    temp_f = Constant("500") #fire
    temp_a = Constant("23.5") #ambient (room)
    temp_s = temp_a #surface

    # Create mesh and define function space
    mesh = IntervalMesh(nx, x_min, x_max)
    V = FunctionSpace(mesh, 'CG', 1)

    # Define initial value
    u_n = interpolate(temp_a, V)

    #Define variational problem
    u = TrialFunction(V)
    v = TestFunction(V)

    # Set up domain for neumann boundary condition
    domain_markers = CellFunction('size_t', mesh) #describe full domain (is this needed?)
    boundary_markers = FacetFunction('size_t', mesh)
    surface = Surface() #make
    surface.mark(boundary_markers, 0) #mark

    # Describe the domain for ds and dx (neumann boundary and full domain, respectively)
    dx = Measure('dx', domain=mesh, subdomain_data=domain_markers)
    ds = Measure('ds', domain=mesh, subdomain_data=boundary_markers)
    # Describe the domain for the dirichlet boundary condition (xmax)
    bc = DirichletBC(V, temp_a, internal_boundary)

    # Describe the heat source
    source = Expression('h * (temp_f - temp_s)', h=h, temp_f=temp_f, temp_s=temp_s, degree = 2)

    # = ((current state - previous state)/dt) + (heat dissipation in obj) - (heat source on boundary)
    if(neumann_bc == "explicit"):
        F = (((u-u_n)*v)/dt)*dx + ((inner(k*grad(u),grad(v))) * dx) - (source * v) * ds
    elif(neumann_bc == "implicit"):
        F = (((u-u_n)*v)/dt)*dx + ((inner(k*grad(u),grad(v))) * dx) - (((h *(temp_f - u)) * v) * ds)
    else:
        print("hit an unknown neumann boundary condition %s." % (neumann_bc))
        raise
    a, L = lhs(F), rhs(F)

    # Time-stepping
    u = Function(V) #re-declare u as per docs
    u.assign(temp_a )
    t = 0.0
    #t = 0.00 + dt
    ts = []
    diffs = []
    surface_albini = []
    surface_analytical = []
    # Obtain the coordinates of the mesh
    coord = V.tabulate_dof_coordinates()[::-1] #why is everything backwards in this godless package
    while (t < T):
        # Compute solution
        solve(a == L, u, bc)
        # Get the values from the fenics Function
        values = np.array(Function.vector(u))[::-1]
        surface_albini.append(values[0])
        # Update neumann boundary condition expression with new surface temp (thanks jake)
        temp_s = u(0)
        source.user_parameters["temp_s"] = Constant(temp_s)
        # Update previous solution
        u_n.assign(u)
        # Analytical Solution NOTE
        # Calculate analytical solution at this time step
        if(t > 0): #analytical solution diverges at t = 0; don't try to solve it.
            compare = analytical_temperature_at_time(t, temp_f, temp_a, coord)
        else:
            compare = values[:] #use sim calculations for first iteration
        surface_analytical.append(compare[0])
        # Error calculation
        err = np.trapz((values - compare), dx=delta_x)
        # Get a sum of squared differences to see what errors look like
        sum_sq_diff = sum((values - compare)**2)
        # Append time, sum_sq_diff to list for plotting
        ts.append(t)
        #diffs.append(sum_sq_diff)
        diffs.append(err)
        print('t = %.2f; surface temp = %.2f; analytical temp = %.2f' % (t,temp_s,compare[0]))
        #print('Depth: 10; Albini temp = %.2f; analytical temp = %.2f' % (float(values[9]),compare[9]))
        #print('Depth: 50; Albini temp = %.2f; analytical temp = %.2f' % (float(values[49]),compare[49]))
        # Update current time
        if(abs(values[-1] - compare[-1]) > tol):
            print("Analytical solution has violated Dirichlet boundary condition.")
            break
        t += dt
        if(chase_plot):
            ax.clear()
            ax.plot(coord, values, color="red", label = "Albini")
            ax.plot(coord, compare, color="blue", label = "Analytical")
            ax.set_title("Analytical vs. Albini Heat Equations")
            ax.legend()
            fig.canvas.draw()
            fig.show()
        elif(diff_plot):
            ax.clear()
            #ax.plot(coord, values, color="red", label = "Albini")
            ax.plot(coord, values - compare, color="green", label = "Albini - Actual")
            ax.set_title("Analytical vs. Albini Heat Equations")
            ax.legend()
            fig.canvas.draw()
            fig.show()
    if(range_error): #show the error across the whole simulation
        ax.clear()
        ax.set_title("Sum of Squared Differences")
        ax.plot(ts, diffs, color = "green")
        plt.show()
        interactive()
        ax.clear()
    if(surf_plot):
        # Save plot and move on
        ax.clear()
        ax.set_title("Surface temperature")
        plt.xlabel("t")
        plt.ylabel("T(x=0,t)")
        ax.plot(ts, surface_albini, label="Albini", color = "red")
        ax.plot(ts, surface_analytical, label="Analytical", color = "blue")
        fig.savefig('surfaces%s%s.png' % (neumann_bc,delta_x))
    return(diffs)


# Heat transfer variables
#h -> parameter that describes heat transfer from flame to object (becomes unstable if large and k is small)
h = 2.0
#k -> kappa parameter; describes heat transfer inside object (becomes unstable if small)
k = 8.000 #TODO as above

#the tolerance for float comparison (a little lenient)
tol = 1E-12

# Set up the matplotlib figure
fig = plt.figure(figsize = (12,12))
ax = fig.add_subplot(111)

# Time variables
T = 1000.0 #final time
totals = []
# Distance variables
x_min = 0.0 #edge of object which is heated
x_max = 100.0 #distance into object

dts = np.logspace(-2, 1, 5)
dxs = np.logspace(-4, 1, 10)

# Testing dt -> set a fixed dx NOTE
dx = 0.1
errors = []
for dt in dts:
    errors.append(compare_FEM_to_analytical(dx,dt,neumann_bc="explicit",diff_plot=True))
# Save plot and move on
ax.clear()
ax.set_title("dt vs. error: explicit")
plt.xlabel("dt")
plt.ylabel("Summed sum of squared differences")
ax.loglog(dts, errors)
fig.savefig('dt_explicit.png')

errors = []
for dt in dts:
    errors.append(compare_FEM_to_analytical(dx,dt,neumann_bc="implicit"))
# Save plot and move on
ax.clear()
ax.set_title("dt vs. error: implicit")
plt.xlabel("dt")
plt.ylabel("Summed sum of squared differences")
ax.loglog(dts, errors)
fig.savefig('dt_implicit.png')
# Testing dx -> set a fixed dt NOTE
dt = 0.01

errors = []
for dx in dxs:
    errors.append(sum(compare_FEM_to_analytical(dx,dt,neumann_bc="explicit")))
# Save plot and move on
ax.clear()
ax.set_title("dx vs. error: explicit")
plt.xlabel("dx")
plt.ylabel("Summed sum of squared differences")
ax.loglog(dxs, errors, '.')
fig.savefig('dx_explicit_6917.png')

errors = []
for dx in dxs:
    errors.append(sum(compare_FEM_to_analytical(dx,dt,neumann_bc="implicit")))
# Save plot and move on
ax.clear()
ax.set_title("dx vs. error: implicit")
plt.xlabel("dx")
plt.ylabel("Summed sum of squared differences")
ax.loglog(dxs, errors, '.')
fig.savefig('dx_implicit_6917.png')
"""
