from scipy.special import erfc, erf
import numpy as np

import Heat_Models

class Analytical_Heat_Model_1D:
    """
    Semi-infinite solid heated by convection.

    Attributes:
        temp_F (float): Fire temperature of the simulation in degrees C (default = 500.0)
        temp_A (float): Ambient temperature of the simulation in degrees C (default = 23.5)
        h (float): Newtonian heat film coefficient (default = 2.0)
        k (float): Medium heat transfer coefficient (default = 8.0)
        xs (float[]): a list of all x positions in the model
    """
    def __init__(self, xs, temp_f, temp_a, k, h):
        self.xs = xs
        self.k = k
        self.h = h
        self.temp_F = temp_f
        self.temp_A = temp_a
        self.alpha = self.generate_alpha()

    def albini_params(self, rho, C, M):
        """some extra physical parameters from albini not in bergman"""
        self.rho = rho
        self.C = C
        self.M = M
        self.generate_tau()

    def generate_tau(self):
        self.tau = self.k * self.rho * self.C / self.h**2
        return(self.tau)

    def generate_alpha(self):
        """
        Create the alpha used by the analytical solution
        NOTE: This appears to work really well, but it's so simple it worries me.
        Parameters: None (variables must be declared)
        Returns: alpha (float)
        """
        return(self.k / (1))

    def generate_albini_alpha(self):
        """
        albini 95 uses a different formulation from bergman, including in making alpha
        """
        return(self.k / (self.rho * self.C))

    def generate_biot_number(self):
        '''make the biot number for more albini calculations'''
        D = self.xs[-1] * 2
        self.bi = self.h * D / self.k
        return(self.bi)

    def generate_fourier_number(self,t):
        '''make the fourier number for albini calculations'''
        D = self.xs[-1] * 2
        self.fo = 4 * self.alpha * t / D**2
        return(self.fo)

    def constant_surface_flux_at_time(self, t):
        """
        try using the second format where flux is constant (average from simulation)
        and apply the heat equation inwards.
        Parameters:
            t:  time iteration (float)
        Returns:
            vals:  list of temperatures (floats)
        """
        q = self.q
        temp_a = self.temp_A
        a = self.alpha
        #a = self.generate_albini_alpha()
        C = self.C
        rho = self.rho
        _h = self.h
        k = self.k
        #tau = self.generate_tau()
        t_over_tau = self.generate_fourier_number(t) * self.generate_biot_number()**2 / 4
        vals = []
        for x in self.xs:
            u = x / np.sqrt(a * t)
            val =  ((2 * q * np.sqrt(a * t / np.pi)) / k) * np.exp(-1 * x**2 / (4 * a * t)) - (q * x / k) * erfc(x / (2 * np.sqrt(a * t))) + temp_a
            vals.append(val)
        return(vals)


    def analytical_temperature_at_time(self, t):
        """
        analytical solution to the problem:
            -treat it strictly as a surface convection problem (p.314 of bergman et al)
            -NOTE it seems like a and dt are closely tied; can we get a function that sets a?
        Parameters:
            t:  time iteration (float)
        Returns:
            vals:  list of temperatures (floats)
        """
        temp_f = self.temp_F
        temp_a = self.temp_A
        #self.alpha = self.generate_albini_alpha()
        a = self.alpha
        _h = self.h
        k = self.k
        vals = []
        for x in self.xs:
            val = (temp_f - temp_a) * (erfc(x/(2*np.sqrt(a*t))) \
                                        - (np.exp(((_h*x)/k) + ((_h*_h*a*t)/(k*k)))) \
                                        * (erfc((x/(2*np.sqrt(a*t))) + ((_h*np.sqrt(a*t))/k)))) \
                                    + temp_a
            vals.append(val)
        return(vals)

    def albini_temperature_at_time(self, t):
        """
        same as above, but with albini's equation which is slightly different
        Parameters:
            t:  time iteration (float)
        Returns:
            vals:  list of temperatures (floats)
        """
        temp_f = self.temp_F
        temp_a = self.temp_A
        #a = self.alpha
        a = self.generate_albini_alpha()
        C = self.C
        rho = self.rho
        _h = self.h
        k = self.k
        #tau = self.generate_tau()
        t_over_tau = self.generate_fourier_number(t) * self.generate_biot_number()**2 / 4
        vals = []
        for x in self.xs:
            u = x / np.sqrt(a * t)
            #val = (temp_f - temp_a) * (1 - erf(u) + (np.exp(np.sqrt(t/tau) * (u + np.sqrt(t/tau))) * erfc(u + np.sqrt(t/tau)))) + temp_a
            #val = (temp_f - temp_a) * (1 - erfc(u) + (np.exp(np.sqrt(t/tau) * (u + np.sqrt(t/tau))) * erfc(u + np.sqrt(t/tau)))) + temp_a
            #val = (temp_f - temp_a) * (1 - erfc(u) + (np.exp(np.sqrt(t_over_tau) * (u + np.sqrt(t_over_tau))) * erfc(u + np.sqrt(t_over_tau)))) + temp_a
            val = (temp_f - temp_a) * (erfc(u) - (np.exp(np.sqrt(t_over_tau) * (u + np.sqrt(t_over_tau))) * erfc(u + np.sqrt(t_over_tau)))) + temp_a
            #val = (1 - erfc(u) + (np.exp(np.sqrt(t/tau) * (u + np.sqrt(t/tau))) * erfc(u + np.sqrt(t/tau))))
            #from lamorlette et al; where's x?
            #val = (temp_f - temp_a) * (1 - np.exp(t_over_tau)*erfc(_h * np.sqrt(t) / (np.sqrt(k * rho * C))))
            vals.append(val)
        return(vals)

    def analytical_temperature_across_interval(self, ts):
        """
        Generate analytical solutions for each t.

        Parameters:
            ts (float[]): a list of times at which the solution will be evaluated.

        Returns:
            solutions (list of float[]): list of temperatures at each point at each time step.
        """
        solutions = []
        for t in ts:
            solution = self.analytical_temperature_at_time(t)
            solutions.append(solution)
        return(solutions)
