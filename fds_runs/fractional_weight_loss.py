import os
import csv
import time
import itertools
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate

def fractional_loss(current, initial):
    return((initial - current)/initial)

def albini_fig_5_fn():
    xs = [0,60,120,180,240]
    ys = [0,0.07,0.13,0.20,0.265]
    points = [xs, ys]
    return(points)

def albini_fig_6_fn():
    xs = [0,100,200,300,400,500]
    ys = [0,0.10,0.15,0.23,0.29,0.35]
    points = [xs, ys]
    return(points)

def albini_fig_7_fn():
    xs = [0,120,240,360,480]
    ys = [0,0.03,0.055,0.085,0.105]
    points = [xs, ys]
    return(points)

#pointfile = os.getcwd() + "/" + "albini_batch_runs_6/albini_48mm_pyro/albini_model_devc.csv"
#alb_data = albini_fig_5_fn()
#ylims = [0,0.4]
#xlims = [0,300]
#xtics = [0,60,120,180,240,300]
#pointfile = os.getcwd() + "/" + "albini_batch_runs_6/albini_107mm_pyro1/albini_model_devc.csv"
#alb_data = albini_fig_6_fn()
#ylims = [0,0.4]
#xlims = [0,500]
#xtics = [0,100,200,300,400,500]
#pointfile = os.getcwd() + "/" + "albini_batch_runs_6/albini_107mm_pyro2/albini_model_devc.csv"
dirnames = ["".join(seq) for seq in itertools.product("012", repeat=4)]
for experiment_number in dirnames:
    try:
        pointfile = str(os.getcwd() + "/" + \
                "albini_material_mass_conversion_series/runs_4/%s/albini_model_devc.csv" % experiment_number)
        alb_data = albini_fig_7_fn()
        ylims = [0,0.2]
        xlims = [0,600]
        xtics = [0,120,240,360,480,600]
        values = []
        with open(pointfile, 'r') as f:
            # skip first line (units -> s, C, C, C, ... C)
            f.next()
            reader = csv.DictReader(f)
            for row in reader:
                values.append(row)
            tag = "MH"
            frames = []
            times = []
            masses = []
            initial_mass = 0.0 #will write over this in loop
            for timestep in values:
                if len(times) == 0: #first time iteration
                    initial_mass = float(timestep["mass"])
                t = float(timestep["Time"])
                times.append(t)
                mass = fractional_loss(float(timestep["mass"]) , initial_mass)
                masses.append(mass)
            delay = 0.00
            fig = plt.figure(figsize = (12,12))
            ax = fig.add_subplot(111)
            ax.clear()
            ax.plot(times, masses, color='black', marker='.', label='Simulation', linestyle="None")
            ax.plot(times, masses, color='black', label='Simulation')
            xnew = np.linspace(alb_data[0][0], alb_data[0][-1], 10)
            yfn = scipy.interpolate.interp1d(alb_data[0], alb_data[1], kind='slinear')
            ynew = yfn(xnew)
            #print(len(xnew),len(ynew))
            ax.plot(alb_data[0], alb_data[1], color='red', marker='.', linestyle="None" )
            ax.plot(xnew, ynew, color='red', label='Albini data')
            ax.set_title("Fractional weight loss of fuel element")
            ax.set_xlabel("Time (s)")
            ax.set_ylabel("Fractional weight loss")
            ax.set_ylim(ylims)
            ax.set_xticks(xtics)
            ax.legend()
            fig.canvas.draw()
            #fig.show()
            plt.savefig('figs/figs_mass_conversion_series/runs_4/%s_albini_mass_pltfig.png' % experiment_number)
            time.sleep(delay)
    except:
        print("No usable data found in %s", experiment_number)
