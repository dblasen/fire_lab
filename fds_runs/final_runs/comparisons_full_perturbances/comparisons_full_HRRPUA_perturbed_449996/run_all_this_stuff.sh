gitfds2='/home/db244348/FDS-SMV//fds/Build/mpi_gnu_linux_64/fds_mpi_gnu_linux_64'

run_series(){
    cd albini_5 &&
    echo $PWD &&
    ($gitfds2 albini_model.fds) &&
    cd .. &&

    cd albini_6 &&
    echo $PWD &&
    ($gitfds2 albini_model.fds) &&
    cd .. &&

    cd albini_7 &&
    echo $PWD &&
    ($gitfds2 albini_model.fds) &&
    cd ..
}

run_all_sizes(){
    cd 0p1cm &&
    echo $PWD &&
    run_series &&
    cd .. &&

    cd 0p15cm &&
    echo $PWD &&
    run_series &&
    cd .. &&

    cd 0p2cm &&
    echo $PWD &&
    run_series &&
    cd .. &&

    cd 0p25cm &&
    echo $PWD &&
    run_series &&
    cd .. &&

    cd 0p3cm &&
    echo $PWD &&
    run_series &&
    cd .. &&

    cd 0p35cm &&
    echo $PWD &&
    run_series &&
    cd .. &&

    cd 0p4cm &&
    echo $PWD &&
    run_series &&
    cd .. &&

    cd 0p45cm &&
    echo $PWD &&
    run_series &&
    cd .. &&

    cd 0p5cm &&
    echo $PWD &&
    run_series &&
    cd ..
}

cd thick &&
run_all_sizes &&
cd .. &&
cd thin &&
 run_all_sizes &&
cd ..

