&HEAD CHID='albini_model', TITLE='Albini experimental system' /

&MESH IJK=20,20,10, XB=0.5,2.5,0.5,2.5,0.0,1 /
&TIME T_END=300., DT=0.05 / 

&SPEC ID='WATER VAPOR' /
&SPEC ID='CARBON DIOXIDE' /
&SPEC ID='CELLULOSE'
    FORMULA = 'C6H10O5'
/

&REAC ID = 'CELLULOSE'
    FUEL = 'CELLULOSE'
    O = 2.5
    C = 3.4
    H = 6.2
    HEAT_OF_COMBUSTION = 17700.
/

- Albini boundary conditions for simulation (open air)
&VENT MB='XMIN', SURF_ID='OPEN' /
&VENT MB='XMAX', SURF_ID='OPEN' /
&VENT MB='YMIN', SURF_ID='OPEN' /
&VENT MB='YMAX', SURF_ID='OPEN' /
&VENT MB='ZMAX', SURF_ID='OPEN' /

- a 10.7cm thick stick; length is size of cell #NOTE
&SURF ID='stick_surf' 
    MATL_ID(1,1:2) ='DRY VEGETATION', 'MOISTURE' 
    MATL_MASS_FRACTION(1,1:2) = 0.9615,0.0385
    THICKNESS=0.00075
    LENGTH=0.3
    N_LAYER_CELLS_MAX = 1
    GEOMETRY='CYLINDRICAL' /

- burner runs on cellulose to produce open flame; chemistry is otherwise unimportant.
&SURF ID='propane_burner', SPEC_ID(1)='CELLULOSE', HRRPUA=224.995 /
&OBST XB=1.0,2.0,1.0,2.0,0.00,0.05, SURF_ID6='INERT','INERT','INERT','INERT','INERT','propane_burner', COLOR='BRICK' /

-- http://www.sauereisen.com/wp-content/uploads/8.pdf
&MATL ID = 'SAUEREISEN CEMENT NO 8'
    VEGETATION = .FALSE.
    DENSITY =  2560
    CONDUCTIVITY = 1.01
    SPECIFIC_HEAT= 0.111
/

- heat_of_combustion taken from birch tga
- rest is from our experimental TGA
&MATL ID                       = 'DRY VEGETATION'
      CONDUCTIVITY_RAMP        = 'RAMP_gpyro_01'
      DENSITY                  = 588.00
      SPECIFIC_HEAT            = 2.85
      EMISSIVITY               = 0.950
      ABSORPTION_COEFFICIENT   = 0.9000E+10
      A(01)                    = 0.2452E+14
      E(01)                    = 0.1786E+06
      N_S(01)                  = 1.00
      THRESHOLD_TEMPERATURE(01)= 0.00
      HEAT_OF_REACTION         = 100.0
      SPEC_ID(01,01)           = 'CELLULOSE'
      NU_SPEC(01,01)           = 0.750
      NU_MATL(01,01)           = 0.250
      MATL_ID(01,01)           = 'CHAR'
      N_REACTIONS              = 01 
/
      HEAT_OF_COMBUSTION       = 40000
      DENSITY                  = 446.00

- char is from Mell's needle experiments; not sure how they got these.
&MATL ID                       = 'CHAR'
      CONDUCTIVITY_RAMP        = 'RAMP_gpyro_02'
      DENSITY                  = 134.00
      SPECIFIC_HEAT            = 1.73
      EMISSIVITY               = 0.900
      ABSORPTION_COEFFICIENT   = 0.9000E+10
      N_REACTIONS              = 1
      N_S                      = 1.
      HEAT_OF_REACTION         = 100.0
      A                        = 430.
      E                        = 74826.
      NU_MATL                  = 0.00
      MATL_ID                  = 'ASH'
      NU_SPEC                  = 1.00
      SPEC_ID                  = 'CELLULOSE'
      GAS_DIFFUSION_DEPTH      = 10
/

&MATL ID                       = 'ASH'
      CONDUCTIVITY             = 0.100
      DENSITY                  = 67.
      SPECIFIC_HEAT            = 0.85
      EMISSIVITY               = 0.500
      ABSORPTION_COEFFICIENT   = 0.9000E+10
      N_REACTIONS              = 00 
      GAS_DIFFUSION_DEPTH      = 10
/


&MATL ID = 'MOISTURE'
      DENSITY                  = 1000.0
      CONDUCTIVITY             = 0.6
      SPECIFIC_HEAT            = 4.184
      N_REACTIONS              = 1
      REFERENCE_TEMPERATURE    = 100.0
      NU_SPEC                  = 1.0
      SPEC_ID                  = 'WATER VAPOR'
      HEAT_OF_REACTION         = 2500.0 /

-- these ramps modify the conductivity
-- of the materials with respect to temperature
&RAMP ID = 'RAMP_gpyro_01', T=   26.9, F =  0.1899 /
&RAMP ID = 'RAMP_gpyro_01', T=   76.9, F =  0.1911 /
&RAMP ID = 'RAMP_gpyro_01', T=  126.9, F =  0.1920 /
&RAMP ID = 'RAMP_gpyro_01', T=  176.9, F =  0.1929 /
&RAMP ID = 'RAMP_gpyro_01', T=  226.9, F =  0.1937 /
&RAMP ID = 'RAMP_gpyro_01', T=  276.9, F =  0.1944 /
&RAMP ID = 'RAMP_gpyro_01', T=  326.9, F =  0.1951 /
&RAMP ID = 'RAMP_gpyro_01', T=  376.9, F =  0.1957 /
&RAMP ID = 'RAMP_gpyro_01', T=  426.9, F =  0.1962 /
&RAMP ID = 'RAMP_gpyro_01', T=  476.9, F =  0.1968 /
&RAMP ID = 'RAMP_gpyro_01', T=  526.9, F =  0.1972 /
&RAMP ID = 'RAMP_gpyro_01', T=  576.9, F =  0.1977 /
&RAMP ID = 'RAMP_gpyro_01', T=  626.9, F =  0.1981 /
&RAMP ID = 'RAMP_gpyro_01', T=  676.9, F =  0.1986 /

&RAMP ID = 'RAMP_gpyro_02', T=   26.9, F =  0.0951 /
&RAMP ID = 'RAMP_gpyro_02', T=   76.9, F =  0.0972 /
&RAMP ID = 'RAMP_gpyro_02', T=  126.9, F =  0.0991 /
&RAMP ID = 'RAMP_gpyro_02', T=  176.9, F =  0.1007 /
&RAMP ID = 'RAMP_gpyro_02', T=  226.9, F =  0.1023 /
&RAMP ID = 'RAMP_gpyro_02', T=  276.9, F =  0.1037 /
&RAMP ID = 'RAMP_gpyro_02', T=  326.9, F =  0.1050 /
&RAMP ID = 'RAMP_gpyro_02', T=  376.9, F =  0.1062 /
&RAMP ID = 'RAMP_gpyro_02', T=  426.9, F =  0.1073 /
&RAMP ID = 'RAMP_gpyro_02', T=  476.9, F =  0.1084 /
&RAMP ID = 'RAMP_gpyro_02', T=  526.9, F =  0.1094 /
&RAMP ID = 'RAMP_gpyro_02', T=  576.9, F =  0.1103 /
&RAMP ID = 'RAMP_gpyro_02', T=  626.9, F =  0.1112 /
&RAMP ID = 'RAMP_gpyro_02', T=  676.9, F =  0.1121 /

&PART ID='stick', SAMPLING_FACTOR=1, SURF_ID='stick_surf', PROP_ID='wood image'
      QUANTITIES='PARTICLE TEMPERATURE','PARTICLE MASS','PARTICLE DIAMETER', STATIC=.TRUE. /

&INIT ID='stick', XYZ=1.5,1.5,0.60, N_PARTICLES=1, DX=0.1, PART_ID='stick' /

-new one
&PROP ID='wood image', SMOKEVIEW_ID='tube', SMOKEVIEW_PARAMETERS(1:6)='R=255','B=120','G=255','D=0.048','L=0.3','DIRX=1' /

- Temperature sensor next to particle (in cement, thermal inertia stabilizes values)
&PROP ID='ball', SMOKEVIEW_ID='SPHERE', SMOKEVIEW_PARAMETERS(1)='D=0.02' /
&SURF ID='sensor_surf', MATL_ID = 'SAUEREISEN CEMENT NO 8', RADIUS=0.01, GEOMETRY='SPHERICAL'/
&PART ID='sensor_ball', SAMPLING_FACTOR=1, SURF_ID='sensor_surf', PROP_ID='ball', STATIC=.TRUE./
&INIT ID='sensor_ball', PART_ID='sensor_ball', XYZ=1.55,1.55,0.75, N_PARTICLES=1 /
&DEVC ID ='sensor_temp', QUANTITY='INSIDE WALL TEMPERATURE', INIT_ID='sensor_ball', DEPTH=0.01, IOR=-3/

- Raw air temp (jumps all over)
&DEVC ID='air_temp', QUANTITY='TEMPERATURE', XYZ=1.75,1.55,0.75 / 


-measure particle burning with these quantities
&DEVC ID='burn_rate', QUANTITY='BURNING RATE',INIT_ID='stick' /
&DEVC ID='mass_flux_water', QUANTITY='MASS FLUX',INIT_ID='stick', SPEC_ID='WATER VAPOR' /
&DEVC ID='mass_flux_wood', QUANTITY='MASS FLUX',INIT_ID='stick', SPEC_ID='CELLULOSE' /
&DEVC ID='mass', QUANTITY='PARTICLE MASS', INIT_ID='stick' /

-- measure particle ignition (350C surface temp)
&DEVC ID='surf_temp', QUANTITY='WALL TEMPERATURE', DEPTH=0.0, INIT_ID='stick', IOR=-3/

- slice files to show air patterns
&SLCF PBY=1.5, QUANTITY='TEMPERATURE', CELL_CENTERED=.TRUE., VECTOR=.TRUE. /
&SLCF PBY=1.5, QUANTITY='VELOCITY', CELL_CENTERED=.TRUE. /
&SLCF PBX=1.5, QUANTITY='TEMPERATURE', CELL_CENTERED=.TRUE., VECTOR=.TRUE. /
&SLCF PBX=1.5, QUANTITY='VELOCITY', CELL_CENTERED=.TRUE. /
&SLCF PBY=1.5, QUANTITY='MASS FRACTION', SPEC_ID='WATER VAPOR', CELL_CENTERED=.TRUE. /
&SLCF PBY=1.5, QUANTITY='RELATIVE HUMIDITY', CELL_CENTERED=.TRUE. /
&DUMP MASS_FILE=.TRUE. /

# profiles of particle (temperature, material density)
&PROF ID='particle_temperature', INIT_ID='stick', QUANTITY='TEMPERATURE'/
&PROF ID='particle_dryvegetation', INIT_ID='stick', QUANTITY='DRY VEGETATION'/
&PROF ID='particle_char', INIT_ID='stick', QUANTITY='CHAR'/
&PROF ID='particle_ash', INIT_ID='stick', QUANTITY='ASH'/
&PROF ID='particle_moisture', INIT_ID='stick', QUANTITY='MOISTURE'/

&TAIL /
