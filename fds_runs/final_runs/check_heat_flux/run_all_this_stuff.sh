gitfds2='/home/david/FDS-SMV/fds_new/fds/Build/mpi_gnu_linux_64/fds_mpi_gnu_linux_64'

run_series(){
    cd albini_5 &&
    echo $PWD &&
    ($gitfds2 albini_model.fds) &&
    cd .. &&

    cd albini_6 &&
    echo $PWD &&
    ($gitfds2 albini_model.fds) &&
    cd .. &&

    cd albini_7 &&
    echo $PWD &&
    ($gitfds2 albini_model.fds) &&
    cd ..
}

run_all_sizes(){
    cd 1p0cm &&
    echo $PWD &&
    run_series &&
    cd .. &&

    cd 2p0cm &&
    echo $PWD &&
    run_series &&
    cd .. &&

    cd 3p0cm &&
    echo $PWD &&
    run_series &&
    cd .. &&

    cd 4p0cm &&
    echo $PWD &&
    run_series &&
    cd .. &&

    cd 5p0cm &&
    echo $PWD &&
    run_series &&
    cd .. &&

    cd 6p0cm &&
    echo $PWD &&
    run_series &&
    cd .. &&

    cd 7p0cm &&
    echo $PWD &&
    run_series &&
    cd .. &&

    cd 8p0cm &&
    echo $PWD &&
    run_series &&
    cd .. &&

    cd 9p0cm &&
    echo $PWD &&
    run_series &&
    cd ..

    cd 10p0cm &&
    echo $PWD &&
    run_series &&
    cd ..
}

cd thick &&
run_all_sizes &&
#cd .. &&
#cd thin &&
# run_all_sizes &&
cd ..

