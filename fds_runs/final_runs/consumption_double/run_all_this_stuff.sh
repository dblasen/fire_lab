gitfds2='/home/david/FDS-SMV/fds_new/fds/Build/mpi_gnu_linux_64/fds_mpi_gnu_linux_64'

run_series(){
    cd albini_5 &&
    echo $PWD &&
    ($gitfds2 albini_model.fds) &&
    cd .. &&

    cd albini_6 &&
    echo $PWD &&
    ($gitfds2 albini_model.fds) &&
    cd .. &&

    cd albini_7 &&
    echo $PWD &&
    ($gitfds2 albini_model.fds) &&
    cd ..
}

run_all_sizes(){
    cd 0p1cm &&
    echo $PWD &&
    run_series &&
    cd .. &&


    cd 0p3cm &&
    echo $PWD &&
    run_series &&
    cd .. &&


    cd 0p5cm &&
    echo $PWD &&
    run_series &&
    cd ..
}

cd thick &&
run_all_sizes &&
cd .. &&
cd thin &&
 run_all_sizes &&
cd ..

