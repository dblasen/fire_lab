import os
import sys

'''
Synopsis: python delete_non_input_files.py <directory>

recursively traverses directories and deletes anything that isn't:
    - an FDS input file
    - a .gitignore
    - a python script
    - a bash script

if it's not abundantly clear, you shouldn't run this while tired or hungry.
'''

def save_file(fname):
    save_me = False
    if(".gitignore" in fname):
        save_me = True
    elif(fname.endswith('.fds')):
        save_me = True
    elif(fname.endswith('.sh')):
        save_me = True
    elif(fname.endswith('.py')):
        save_me = True
    return(save_me)


def walk(directory):
    in_dir = os.listdir(directory)
    for element in in_dir:
        if os.path.isdir(directory + '/' + element):
            walk(directory + '/' + element)
        elif os.path.isfile(directory + '/' + element):
            if(save_file(directory + '/' + element)):
                print("Saving " + directory + '/' + element)
            else:
                print("Deleting " + directory + '/' + element)
                os.remove(directory + '/' + element)
        else:pass
    return

if __name__ == "__main__":
    try:
        walk(sys.argv[1])
    except:
        print("Please read this file for syntax.")
