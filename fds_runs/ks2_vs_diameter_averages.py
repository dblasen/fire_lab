import os
import sys
import csv
import time
import string
import operator
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate
import scipy.stats

def get_dmdt(data):
    '''
    get the mass loss rate to check
    whether this data point belongs in the KS statistic
    '''
    dm = np.gradient(data[1])
    dt = np.gradient(data[0])
    dmdt = dm / dt
    return(dmdt)

def sample_for_stat(data1, data2, snuff_tol = 0.001, ignite_tol = 0.01):
    dmdt1 = get_dmdt(data1)
    dmdt2 = get_dmdt(data2)
    new_data1 = [[],[]]
    new_data2 = [[],[]]
    has_ignited1 = False
    has_ignited2 = False
    for i in range(len(dmdt1)):
        if ((has_ignited1 and has_ignited2) and (dmdt1[i] < snuff_tol and dmdt2[i] < snuff_tol)):
            break
        elif (not has_ignited1 or not has_ignited2):
            if(dmdt1[i] > ignite_tol):
                has_ignited1 = True
            if(dmdt2[i] > ignite_tol):
                has_ignited2 = True
            new_data1[0].append(data1[0][i])
            new_data1[1].append(data1[1][i])
            new_data2[0].append(data2[0][i])
            new_data2[1].append(data2[1][i])
        elif ((has_ignited1 and has_ignited2) and (dmdt1[i] > snuff_tol or dmdt2[i] > snuff_tol)):
            new_data1[0].append(data1[0][i])
            new_data1[1].append(data1[1][i])
            new_data2[0].append(data2[0][i])
            new_data2[1].append(data2[1][i])
        else:
            print("Should never happen")
    return(new_data1, new_data2)

def find_matching_dict(data, member):
    for dictionary in data:
        if((dictionary["exp"] == member["exp"]) and (dictionary["diameter"] == member["diameter"])\
                and (dictionary["type"] != member["type"])):
            return(dictionary)
    return(-1)


def make_line(data, experiment_number):
    new_set1 = [] #thick
    new_set2 = [] #thin
    thick = "thick" # compare below...
    thin = "thin"
    for dictionary in data:
        if (dictionary["values"] == -1): continue
        if (dictionary["exp"] == experiment_number and dictionary['type'] == thick):
            #new_set.append([dictionary["diameter"],dictionary["value"]])
            other_dict = find_matching_dict(data, dictionary)
            if (other_dict == -1): raise Exception("Couldn't match up a thin/thick dictionary pair!")
            values1, values2 = sample_for_stat([dictionary["values"][0], dictionary["values"][1]], [other_dict["values"][0],other_dict["values"][1]]) #screams
            new_set1.append([dictionary["diameter"],values1])
            new_set2.append([other_dict["diameter"],values2])
    new_set1 = sorted(new_set1, key=operator.itemgetter(0))
    new_set2 = sorted(new_set2, key=operator.itemgetter(0))
    xs1 = [i[0] for i in new_set1]
    xs2 = [i[0] for i in new_set2]
    ys1 = []
    ps1 = []
    ys2 = []
    ps2 = []
    for i,j in zip(new_set1,new_set2):
        ys, ps = scipy.stats.ks_2samp(i[1][1], j[1][1])
        ys1.append(ys)
        ps1.append(ps)
    return([xs1,ys1],[xs1,ps1])

def gather_data(path):
    data = []
    dir_contents = os.listdir(path)
    for item in dir_contents:
        if "base" in item:
            continue
        elif "ignore" in item:
            continue
        elif "thin" in item:
            key0 = "thin"
        elif "thick" in item:
            key0 = "thick"
        else:continue
        #need to explore individual experiments now
        path2 = path + item
        dir_contents2 = sorted(os.listdir(path2))
        for entry in dir_contents2:
            key1 = entry[:-2] #diameter
            key1 = string.replace(key1, 'p', '.')
            path3 = path2 + '/' + entry
            dir_contents3 = os.listdir(path3)
            for elt in dir_contents3:
                if not os.path.isdir(path3 + '/' + elt):
                    continue
                if "gpyro" in elt:
                    continue
                key2 = elt[-1] #5,6, or 7th figure
                #value = get_ignition_time(path3 + '/' + elt + '/' + 'albini_model_devc.csv')
                all_data = get_exp_data(path3 + '/' + elt + '/' + 'albini_model_devc.csv')
                # NOTE *10 below to convert to mm from cm
                data.append({'diameter':float(key1)*10, 'type':str(key0), 'exp':int(key2), 'values':all_data})
    return(data)

def get_exp_data(pointfile):
    with open(pointfile, 'r') as f:
        values = []
        # skip first line (units -> s, C, C, C, ... C)
        f.next()
        reader = csv.DictReader(f)
        for row in reader:
            values.append(row)
        times = []
        masses = []
        initial_mass = 0.0 #will write over this in loop
        for timestep in values:
            if len(times) == 0: #first time iteration
                initial_mass = float(timestep["mass"])
            t = float(timestep["Time"])
            times.append(t)
            mass = fractional_loss(float(timestep["mass"]) , initial_mass)
            masses.append(mass)
    return([times,masses])

def get_ignition_point(pointfile, ignition_temp=350):
    '''get the ignition point based on some surface temperature (350C to start)'''

def fractional_loss(current, initial):
    return((initial - current)/initial)

def albini_fig_5_fn():
    xs = [0,60,120,180,240]
    ys = [0,0.07,0.13,0.20,0.265]
    points = [xs, ys]
    return(points)

def albini_fig_6_fn():
    xs = [0,100,200,300,400,500]
    ys = [0,0.10,0.15,0.23,0.29,0.35]
    points = [xs, ys]
    return(points)

def albini_fig_7_fn():
    xs = [0,120,240,360,480]
    ys = [0,0.03,0.055,0.085,0.105]
    points = [xs, ys]
    return(points)

def get_average_lines(alldata):
    allthick5s = []
    allthin5s = []
    allthick6s = []
    allthin6s = []
    allthick7s = []
    allthin7s = []
    for data in alldata:
        thick_xs1, thick_ys1 = make_line(data['data'], 5, "thick")
        thin_xs1, thin_ys1 = make_line(data['data'], 5, "thin")
        allthin5s.append(thin_ys1)
        allthick5s.append(thick_ys1)
        thick_xs1, thick_ys1 = make_line(data['data'], 6, "thick")
        thin_xs1, thin_ys1 = make_line(data['data'], 6, "thin")
        allthin6s.append(thin_ys1)
        allthick6s.append(thick_ys1)
        thick_xs1, thick_ys1 = make_line(data['data'], 7, "thick")
        thin_xs1, thin_ys1 = make_line(data['data'], 7, "thin")
        allthin7s.append(thin_ys1)
        allthick7s.append(thick_ys1)
    thick5avg = np.average(allthick5s, axis=0)
    thin5avg = np.average(allthin5s, axis=0)
    thick6avg = np.average(allthick6s, axis=0)
    thin6avg = np.average(allthin6s, axis=0)
    thick7avg = np.average(allthick7s, axis=0)
    thin7avg = np.average(allthin7s, axis=0)
    return(thick5avg, thin5avg, thick6avg, thin6avg, thick7avg, thin7avg)

dirname1 = sys.argv[1]
timelen = 300 #NOTE magic is not good
pointfile_dir1 = os.getcwd() + "/" + dirname1

data = gather_data(dirname1)

fig, (ax2, ax1, ax3) = plt.subplots(1,3,figsize=(24,8), sharey=True)

sample51, sample52 = make_line(data, 5)
print(sample51)
print(sample52)

ax1.plot(sample51[0], sample51[1], color='black', marker='.', label='KS_2')
ax1.set_title("21% moisture")
ax1.set_xlabel("Diameter (mm)")
ax1.grid()

sample61, sample62 = make_line(data, 6)

ax2.plot(sample61[0], sample61[1], color='black', marker='.', label='KS_2')
ax2.set_ylabel("KS2 statistic")
ax2.set_title("4% moisture")
ax2.set_xlabel("Diameter (mm)")
ax2.grid()

sample71, sample72 = make_line(data, 7)

ax3.plot(sample71[0], sample71[1], color='black', marker='.', label='KS_2')
ax3.set_title("60% moisture")
ax3.set_xlabel("Diameter (mm)")
ax3.legend(loc=4)
ax3.grid()

plt.subplots_adjust(left=0.1, wspace=0.1, top=0.8)
plt.show(block=True)
