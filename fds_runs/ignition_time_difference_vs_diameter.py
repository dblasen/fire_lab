import os
import sys
import csv
import time
import string
import operator
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate

def get_ignition_time(pointfile):
    ignition_temp = 350
    with open(pointfile, 'r') as f:
        # skip first line (units -> s, C, C, C, ... C)
        values = []
        f.next()
        reader = csv.DictReader(f)
        ignition_time = -1 #overwrite if valid, error if not
        for row in reader:
            if is_ignited(row["surf_temp"],ignition_temp): #first time iteration
                ignition_time = float(row["Time"])
                return(ignition_time)
            else:pass
    return(ignition_time)

def make_line(data, experiment_number, fuel_type):
    new_set = []
    for dictionary in data:
        if (dictionary["value"] == -1): continue
        if (dictionary["exp"] == experiment_number and dictionary['type'] == fuel_type):
            new_set.append([dictionary["diameter"],dictionary["value"]])
    new_set = sorted(new_set, key=operator.itemgetter(0))
    xs = [i[0] for i in new_set]
    ys = [i[1] for i in new_set]
    return(xs,ys)


def gather_data(path):
    data = []
    dir_contents = os.listdir(path)
    for item in dir_contents:
        if "base" in item:
            continue
        elif "ignore" in item:
            continue
        elif "thin" in item:
            key0 = "thin"
        elif "thick" in item:
            key0 = "thick"
        else:continue
        #need to explore individual experiments now
        path2 = path + item
        dir_contents2 = sorted(os.listdir(path2))
        for entry in dir_contents2:
            key1 = entry[:-2] #diameter
            key1 = string.replace(key1, 'p', '.')
            path3 = path2 + '/' + entry
            dir_contents3 = os.listdir(path3)
            for elt in dir_contents3:
                if not os.path.isdir(path3 + '/' + elt):
                    continue
                if "gpyro" in elt:
                    continue
                key2 = elt[-1] #5,6, or 7th figure
                value = get_ignition_time(path3 + '/' + elt + '/' + 'albini_model_devc.csv')
                # NOTE *10 below to convert to mm from cm
                data.append({'diameter':float(key1)*10, 'type':str(key0), 'exp':int(key2), 'value':float(value)})
    return(data)

def is_ignited(temperature, ignition_temp=350):
    return(float(temperature) > ignition_temp)

def get_differences(vector1, vector2):
    assert (len(vector1) == len(vector2))
    new_vector = []
    for i in range(len(vector1)):
        difference = (abs(vector1[i] - vector2[i]) / vector1[i]) * 100
        #new_vector.append(abs(vector1[i] / vector2[i]))
        new_vector.append(difference)
    return(new_vector)

dirname1 = sys.argv[1]
dirname2 = sys.argv[2]
dirname3 = sys.argv[3]
dirname4 = sys.argv[4]
data_dir1 = os.getcwd() + "/" + dirname1
data_dir2 = os.getcwd() + "/" + dirname2
data_dir3 = os.getcwd() + "/" + dirname3
data_dir4 = os.getcwd() + "/" + dirname4
data1 = gather_data(data_dir1) #half
data2 = gather_data(data_dir2) #full
data3 = gather_data(data_dir3) #double
data4 = gather_data(data_dir4) #double

#plt.rc('text', usetex=True)
#fig = plt.figure(figsize = (24,8))
fig, (ax2, ax1, ax3) = plt.subplots(1,3,figsize=(24,8), sharey=True)

thick_xs1, thick_ys1 = make_line(data1, 5, "thick")
thin_xs1, thin_ys1 = make_line(data1, 5, "thin")
thick_xs2, thick_ys2 = make_line(data2, 5, "thick")
thin_xs2, thin_ys2 = make_line(data2, 5, "thin")
thick_xs3, thick_ys3 = make_line(data3, 5, "thick")
thin_xs3, thin_ys3 = make_line(data3, 5, "thin")
thick_xs4, thick_ys4 = make_line(data4, 5, "thick")
thin_xs4, thin_ys4 = make_line(data4, 5, "thin")
ys1 = get_differences(thin_ys1, thick_ys1)
ys2 = get_differences(thin_ys2, thick_ys2)
ys3 = get_differences(thin_ys3, thick_ys3)
ys4 = get_differences(thin_ys4, thick_ys4)
ax1.plot(thick_xs1, ys1, color='red', marker='.', label='225 kW/m^2')
ax1.plot(thick_xs1, ys2, color='blue', marker='.', label='450 kW/m^2')
ax1.plot(thick_xs1, ys3, color='green', marker='.', label='900 kW/m^2')
ax1.plot(thick_xs1, ys4, color='lightgreen', marker='.', label='112.5 kW/m^2')
ax1.set_title(str("21% Moisture"))
ax1.set_xlabel("Particle Diameter (mm)")
ax1.grid()

thick_xs1, thick_ys1 = make_line(data1, 6, "thick")
thin_xs1, thin_ys1 = make_line(data1, 6, "thin")
thick_xs2, thick_ys2 = make_line(data2, 6, "thick")
thin_xs2, thin_ys2 = make_line(data2, 6, "thin")
thick_xs3, thick_ys3 = make_line(data3, 6, "thick")
thin_xs3, thin_ys3 = make_line(data3, 6, "thin")
thick_xs4, thick_ys4 = make_line(data4, 6, "thick")
thin_xs4, thin_ys4 = make_line(data4, 6, "thin")
ys1 = get_differences(thin_ys1, thick_ys1)
ys2 = get_differences(thin_ys2, thick_ys2)
ys3 = get_differences(thin_ys3, thick_ys3)
ys4 = get_differences(thin_ys4, thick_ys4)
ax2.plot(thick_xs1, ys1, color='red', marker='.', label='225 kW/m^2')
ax2.plot(thick_xs1, ys2, color='blue', marker='.', label='450 kW/m^2')
ax2.plot(thick_xs1, ys3, color='green', marker='.', label='900 kW/m^2')
ax2.plot(thick_xs1, ys4, color='lightgreen', marker='.', label='112.5 kW/m^2')
ax2.set_title(str("4% Moisture"))
ax2.set_xlabel("Particle Diameter (mm)")
ax2.set_ylabel("Thin ignition time (percentage over thick)")
ax2.grid()

thick_xs1, thick_ys1 = make_line(data1, 7, "thick")
thin_xs1, thin_ys1 = make_line(data1, 7, "thin")
thick_xs2, thick_ys2 = make_line(data2, 7, "thick")
thin_xs2, thin_ys2 = make_line(data2, 7, "thin")
thick_xs3, thick_ys3 = make_line(data3, 7, "thick")
thin_xs3, thin_ys3 = make_line(data3, 7, "thin")
thick_xs4, thick_ys4 = make_line(data4, 7, "thick")
thin_xs4, thin_ys4 = make_line(data4, 7, "thin")
ys1 = get_differences(thin_ys1, thick_ys1)
ys2 = get_differences(thin_ys2, thick_ys2)
ys3 = get_differences(thin_ys3, thick_ys3)
ys4 = get_differences(thin_ys4, thick_ys4)
ax3.plot(thick_xs1, ys1, color='red', marker='.', label='225 kW/m^2')
ax3.plot(thick_xs1, ys2, color='blue', marker='.', label='450 kW/m^2')
ax3.plot(thick_xs1, ys3, color='green', marker='.', label='900 kW/m^2')
ax3.plot(thick_xs1, ys4, color='lightgreen', marker='.', label='112.5 kW/m^2')
ax3.set_title(str("60% Moisture"))
ax3.set_xlabel("Particle Diameter (mm)")
ax3.legend(loc=2)
ax3.grid()

plt.subplots_adjust(left=0.1, wspace=0.1, top=0.8)
plt.show(block=True)


