import os
import sys
import csv
import time
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate
import scipy.stats
import seaborn as sns

def get_dmdt(data):
    '''
    get the mass loss rate to check
    whether this data point belongs in the KS statistic
    '''
    dm = np.gradient(data[1])
    dt = np.gradient(data[0])
    dmdt = dm / dt
    return(dmdt)

def sample_for_stat(data1, data2, snuff_tol = 0.0001, ignite_tol = 0.001):
    dmdt1 = get_dmdt(data1)
    dmdt2 = get_dmdt(data2)
    new_data1 = [[],[]]
    new_data2 = [[],[]]
    has_ignited1 = False
    has_ignited2 = False
    for i in range(len(dmdt1)):
        if ((has_ignited1 and has_ignited2) and (dmdt1[i] < snuff_tol and dmdt2[i] < snuff_tol)):
            break
        elif (not has_ignited1 or not has_ignited2):
            if(dmdt1[i] > ignite_tol):
                has_ignited1 = True
            if(dmdt2[i] > ignite_tol):
                has_ignited2 = True
            new_data1[0].append(data1[0][i])
            new_data1[1].append(data1[1][i])
            new_data2[0].append(data2[0][i])
            new_data2[1].append(data2[1][i])
        elif ((has_ignited1 and has_ignited2) and (dmdt1[i] > snuff_tol or dmdt2[i] > snuff_tol)):
            new_data1[0].append(data1[0][i])
            new_data1[1].append(data1[1][i])
            new_data2[0].append(data2[0][i])
            new_data2[1].append(data2[1][i])
        else:
            print("Should never happen")
    return(new_data1, new_data2)

def get_exp_data(pointfile):
    with open(pointfile, 'r') as f:
        values = []
        # skip first line (units -> s, C, C, C, ... C)
        f.next()
        reader = csv.DictReader(f)
        for row in reader:
            values.append(row)
        times = []
        masses = []
        initial_mass = 0.0 #will write over this in loop
        for timestep in values:
            if len(times) == 0: #first time iteration
                initial_mass = float(timestep["mass"])
            t = float(timestep["Time"])
            times.append(t)
            mass = fractional_loss(float(timestep["mass"]) , initial_mass)
            masses.append(mass)
    return([times,masses])

def get_ignition_point(pointfile, ignition_temp=350):
    with open(pointfile, 'r') as f:
        # skip first line (units -> s, C, C, C, ... C)
        f.next()
        reader = csv.DictReader(f)
        ignition_time = 0
        for row in reader:
            if (float(row["surf_temp"]) > ignition_temp):
                ignition_time = float(row["Time"])
                break
    return(ignition_time)


def fractional_loss(current, initial):
    return((initial - current)/initial)

def albini_fig_5_fn():
    xs = [0,60,120,180,240]
    ys = [0,0.07,0.13,0.20,0.265]
    points = [xs, ys]
    return(points)

def albini_fig_6_fn():
    xs = [0,100,200,300,400,500]
    ys = [0,0.10,0.15,0.23,0.29,0.35]
    points = [xs, ys]
    return(points)

def albini_fig_7_fn():
    xs = [0,120,240,360,480]
    ys = [0,0.03,0.055,0.085,0.105]
    points = [xs, ys]
    return(points)

def plot_single_pair(dirname1, dirname2, timelen, ax):
    fname = "albini_model_devc.csv"
    exp_1 = get_exp_data(dirname1 + '/'  + fname) #thick/albini5
    exp_2 = get_exp_data(dirname2 + '/'  + fname) #thin/albini5
    t1 = get_ignition_point(dirname1 + '/'  + fname)
    t2 = get_ignition_point(dirname2 + '/'  + fname)
    print(t1,t2)
    sample1, sample2 = sample_for_stat(exp_1, exp_2)
    ax.plot(sample1[0], sample1[1], color='black', marker='.', label='Thick')
    ax.plot(sample2[0], sample2[1], color='blue', marker='.', label='Thin')
    ax.axvline(x=t1, color='black', linestyle = '--', label="Ignition")
    ax.axvline(x=t2, color='blue', linestyle = '--', label="Ignition")
    ax.set_xlim(0,timelen)
    s,p = scipy.stats.ks_2samp(exp_1[1], exp_2[1])
    return(ax)

def main():
    if (len(sys.argv) < 3):
        raise(Exception("Syntax: python fractional_weight_loss_3pane_compare.py <thick directory> <thin directory> <seconds>"))
    dirname1 = sys.argv[1] #thick
    dirname2 = sys.argv[2] #thin
    timelen = int(sys.argv[3])
    pointfile_dir1 = os.getcwd() + "/" + dirname1
    pointfile_dir2 = os.getcwd() + "/" + dirname2

    #find our experiments so we can open them up
    dir_contents = os.listdir(pointfile_dir1)
    fname = "albini_model_devc.csv"
    for item in dir_contents:
        if "_5" in item:
            exp_5_1 = get_exp_data(dirname1 + '/' + item + '/' + fname)
        elif "_6" in item:
            exp_6_1 = get_exp_data(dirname1 + '/' + item + '/' + fname)
        elif "_7" in item:
            exp_7_1 = get_exp_data(dirname1 + '/' + item + '/' + fname)
        else:
            pass

    #find our experiments so we can open them up
    dir_contents = os.listdir(pointfile_dir2)
    fname = "albini_model_devc.csv"
    for item in dir_contents:
        if "_5" in item:
            exp_5_2 = get_exp_data(dirname2 + '/' + item + '/' + fname)
        elif "_6" in item:
            exp_6_2 = get_exp_data(dirname2 + '/' + item + '/' + fname)
        elif "_7" in item:
            exp_7_2 = get_exp_data(dirname2 + '/' + item + '/' + fname)
        else:
            pass

    sns.set_style('whitegrid')
    fig, (ax2, ax1, ax3) = plt.subplots(1,3,figsize=(24,8), sharey=True)

    sample51, sample52 = sample_for_stat(exp_5_1, exp_5_2)

    ax1.plot(sample51[0], sample51[1], color='black', marker='.', label='Simulation (thick)')
    ax1.plot(sample52[0], sample52[1], color='blue', marker='.', label='Simulation (thin)')
    ax1.set_title("21% moisture")
    ax1.set_xlabel("Time (s)")
    ax1.set_xlim(0,timelen)
    s1,p1 = scipy.stats.ks_2samp(exp_5_1[1], exp_5_2[1])

    sample61, sample62 = sample_for_stat(exp_6_1, exp_6_2)

    ax2.plot(sample61[0], sample61[1], color='black', marker='.', label='Simulation (thick)')
    ax2.plot(sample62[0], sample62[1], color='blue', marker='.', label='Simulation (thin)')
    ax2.set_ylabel("Fractional weight loss")
    ax2.set_title("4% moisture")
    ax2.set_xlabel("Time (s)")
    ax2.set_xlim(0,timelen)
    s2,p2 = scipy.stats.ks_2samp(exp_6_1[1], exp_6_2[1])

    sample71, sample72 = sample_for_stat(exp_7_1, exp_7_2)

    ax3.plot(sample71[0], sample71[1], color='black', marker='.', label='Simulation (thick)')
    ax3.plot(sample72[0], sample72[1], color='blue', marker='.', label='Simulation (thin)')
    ax3.set_title("60% moisture")
    ax3.set_xlabel("Time (s)")
    ax3.set_xlim(0,timelen)
    s3,p3 = scipy.stats.ks_2samp(exp_7_1[1], exp_7_2[1])
    ax3.legend(loc=4)

    plt.subplots_adjust(left=0.1, wspace=0.1, top=0.8)
    plt.show(block=True)

if __name__ == '__main__':
    main_1plot()
