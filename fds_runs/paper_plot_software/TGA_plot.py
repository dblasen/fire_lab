import os
import sys
import csv
import time
import matplotlib.pyplot as plt
import seaborn as sns

# ##Temp./oC,Time/min,DSC/(mW/mg),Mass loss/mg,DTG/(mg/min),Sensit./(uV/mW),,time (s),Mass/%,g/s
def get_exp_file_data(filename):
    print("opening %s\n" % filename)
    xname = "time (s)"
    yname = "Mass/%"
    x = []
    y = []
    with open(filename, 'rb') as infile:
        reader = csv.DictReader(infile)
        for row in reader:
            x.append(row[xname])
            y.append(row[yname])
    a_zip = [x,y]
    return(a_zip)

def get_sim_file_data(filename):
    print("opening %s\n" % filename)
    xname = "t"
    yname = "002_M/M0( 0.0000_ 0.0000_ 0.0000)"
    x = []
    y = []
    with open(filename, 'rb') as infile:
        reader = csv.DictReader(infile)
        for row in reader:
            x.append(row[xname])
            y.append(float(row[yname])*100)
    a_zip = [x,y]
    return(a_zip)

def make_single_pane_plot_small_legend(dirname,ax):
    plt.rc('text', usetex=True)
    exp_fname_5  = "PICO_wd_dry_5_new.csv"
    exp_fname_10 = "PICO_wd_dry_10_new.csv"
    exp_fname_20 = "PICO_wd_dry_20_new.csv"
    sim_fname_5  = "gpyro_summary_01_0001.csv"
    sim_fname_10 = "gpyro_summary_01_0002.csv"
    sim_fname_20 = "gpyro_summary_01_0003.csv"
    sim_dirname =  "/" + dirname
    exp_dirname = sim_dirname + "experiments/"
    d = os.getcwd()
    expdata5  = get_exp_file_data(d + exp_dirname + exp_fname_5)
    simdata5  = get_sim_file_data(d + sim_dirname + sim_fname_5)
    expdata10 = get_exp_file_data(d + exp_dirname + exp_fname_10)
    simdata10 = get_sim_file_data(d + sim_dirname + sim_fname_10)
    expdata20 = get_exp_file_data(d + exp_dirname + exp_fname_20)
    simdata20 = get_sim_file_data(d + sim_dirname + sim_fname_20)

    ax.plot(expdata5[0], expdata5[1],  marker='.', markersize=1.5, color="red", linestyle="-")
    ax.plot(simdata5[0], simdata5[1],  marker='.', markersize=1.5, color="blue", linestyle="-")
    ax.plot(expdata10[0], expdata10[1],  marker='.', markersize=1.5, color="red", linestyle="-")
    ax.plot(simdata10[0], simdata10[1],  marker='.', markersize=1.5,color="blue", linestyle="-")
    ax.plot(expdata20[0], expdata20[1],  marker='.', markersize=1.5, color="red", linestyle="-", label=r"Measured")
    ax.plot(simdata20[0], simdata20[1],  marker='.', markersize=1.5,color="blue", linestyle="-", label=r"Simulated")
    ax.set_xlabel("Time (s)")
    ax.set_ylabel(r'Mass Remaining ($\frac{M}{M_{0}}$)')
    ax.set_ylim([0, 100])
    return(ax)



def make_single_pane_plot(dirname,ax):
    plt.rc('text', usetex=True)
    exp_fname_5  = "PICO_wd_dry_5_new.csv"
    exp_fname_10 = "PICO_wd_dry_10_new.csv"
    exp_fname_20 = "PICO_wd_dry_20_new.csv"
    sim_fname_5  = "gpyro_summary_01_0001.csv"
    sim_fname_10 = "gpyro_summary_01_0002.csv"
    sim_fname_20 = "gpyro_summary_01_0003.csv"
    sim_dirname =  "/" + dirname
    exp_dirname = sim_dirname + "experiments/"
    d = os.getcwd()
    expdata5  = get_exp_file_data(d + exp_dirname + exp_fname_5)
    simdata5  = get_sim_file_data(d + sim_dirname + sim_fname_5)
    expdata10 = get_exp_file_data(d + exp_dirname + exp_fname_10)
    simdata10 = get_sim_file_data(d + sim_dirname + sim_fname_10)
    expdata20 = get_exp_file_data(d + exp_dirname + exp_fname_20)
    simdata20 = get_sim_file_data(d + sim_dirname + sim_fname_20)

    ax.plot(expdata5[0], expdata5[1],  marker='.', markersize=1.5, linestyle="-", label=r"Measured $5^\circ$C/minute")
    ax.plot(simdata5[0], simdata5[1],  marker='.', markersize=1.5, linestyle="-", label=r"Simulated $5^\circ$C/minute")
    ax.plot(expdata10[0], expdata10[1],  marker='.', markersize=1.5, linestyle="-", label=r"Measured $10^\circ$C/minute")
    ax.plot(simdata10[0], simdata10[1],  marker='.', markersize=1.5, linestyle="-", label=r"Simulated $10^\circ$C/minute")
    ax.plot(expdata20[0], expdata20[1],  marker='.', markersize=1.5, linestyle="-", label=r"Measured $20^\circ$C/minute")
    ax.plot(simdata20[0], simdata20[1],  marker='.', markersize=1.5, linestyle="-", label=r"Simulated $20^\circ$C/minute")
    ax.set_xlabel("Time (s)")
    ax.set_ylabel(r'Mass remaining ($\frac{M}{M_{0}}$)')
    ax.set_ylim([0, 100])
    return(ax)

def simplemain():
    dirname = sys.argv[1]
    sns.set_style('whitegrid')
    plt.rc('text', usetex=True)
    plt.figure(figsize = (8,8))
    ax = plt.subplot(111)
    ax = make_single_pane_plot(dirname, ax)
    ax.legend(loc=1)
    plt.show(block=True)

def main():
    exp_fname_5  = "PICO_wd_dry_5_new.csv"
    exp_fname_10 = "PICO_wd_dry_10_new.csv"
    exp_fname_20 = "PICO_wd_dry_20_new.csv"
    sim_fname_5  = "gpyro_summary_01_0001.csv"
    sim_fname_10 = "gpyro_summary_01_0002.csv"
    sim_fname_20 = "gpyro_summary_01_0003.csv"
    sim_dirname =  "/" + sys.argv[1]
    exp_dirname = sim_dirname + "experiments/"

    d = os.getcwd()
    expdata5  = get_exp_file_data(d + exp_dirname + exp_fname_5)
    simdata5  = get_sim_file_data(d + sim_dirname + sim_fname_5)
    expdata10 = get_exp_file_data(d + exp_dirname + exp_fname_10)
    simdata10 = get_sim_file_data(d + sim_dirname + sim_fname_10)
    expdata20 = get_exp_file_data(d + exp_dirname + exp_fname_20)
    simdata20 = get_sim_file_data(d + sim_dirname + sim_fname_20)

    sns.set_style('whitegrid')
    plt.rc('text', usetex=True)
    plt.figure(figsize = (24,8))

    ax1 = plt.subplot(131)
    ax1.plot(expdata5[0], expdata5[1], color="blue", marker='.', markersize=1.5, linestyle="-", label="Measured")
    ax1.plot(simdata5[0], simdata5[1], color="red", marker='.', markersize=1.5, linestyle="-", label="Simulated")
    ax1.set_title("5 degree schedule")
    ax1.set_xlabel("Time (s)")
    ax1.set_ylabel(r'Mass remaining ($\frac{M}{M_{0}}$)')
    ax1.set_ylim([0, 100])
    #ax1.grid(linestyle='--', linewidth=0.5)

    ax2 = plt.subplot(132, sharey=ax1)
    ax2.plot(expdata10[0], expdata10[1], color="blue", marker='.', markersize=1.5, linestyle="-", label="Measured")
    ax2.plot(simdata10[0], simdata10[1], color="red", marker='.', markersize=1.5, linestyle="-", label="Simulated")
    ax2.set_title("10 degree schedule")
    ax2.set_xlabel("Time (s)")
    ax2.set_ylim([0, 100])
    #ax2.grid(linestyle='--', linewidth=0.5)

    ax3 = plt.subplot(133, sharey=ax1)
    ax3.plot(expdata20[0], expdata20[1], color="blue", marker='.', markersize=1.5, linestyle="-", label="Measured")
    ax3.plot(simdata20[0], simdata20[1], color="red", marker='.', markersize=1.5, linestyle="-", label="Simulated")
    ax3.set_title("20 degree schedule")
    ax3.set_xlabel("Time (s)")
    ax3.set_ylim([0, 100])
    #ax3.grid(linestyle='--', linewidth=0.5)
    ax3.legend()

    plt.subplots_adjust(left=0.1, wspace=0.1, top=0.8)
    plt.show(block=True)

if __name__ == "__main__":
    simplemain()
