import os
import sys
import csv
import time
import string
import operator
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate
import scipy.stats
import scipy.spatial
import seaborn as sns
import pandas as pd
from matplotlib import gridspec
import ignition_time_average_noise_check_seaborn



def get_plot_title(dirname):
    if 'quarter' in dirname:
        return(r"$Flux=112.5 kW/m^2$")
    elif 'half' in dirname:
        return(r"$Flux=225 kW/m^2$")
    elif 'full' in dirname:
        return(r"$Flux=450 kW/m^2$")
    elif 'double' in dirname:
        return(r"$Flux=900 kW/m^2$")
    else:
        return("Flux Unknown")

def main():
    dirname1 = sys.argv[1]
    dirname2 = sys.argv[2]

    sns.set(font_scale=2)
    sns.set_style('whitegrid', {"grid.linewidth":50.0, "grid.color":"000000"})
    fig = plt.figure(figsize = (16,20))
    gs = gridspec.GridSpec(3,2, height_ratios=[1,1,1], width_ratios=[1,1])
    plt.rc('text', usetex=True)
    ax1 = plt.subplot(gs[0,0])
    ax2 = plt.subplot(gs[0,1])
    ax3 = plt.subplot(gs[1,0])
    ax4 = plt.subplot(gs[1,1])
    ax5 = plt.subplot(gs[2,0])
    ax6 = plt.subplot(gs[2,1])

    #ax2.set_title(str("4% Moisture"))
    #ax2.set_ylabel("Ignition time (s)")
    #fig, (ax2, ax1, ax3, ax5, ax4, ax6) = plt.subplots(3,2,figsize=(8,10))
    ax1, ax3, ax5 = ignition_time_average_noise_check_seaborn.make_axes(dirname1, ax1, ax3, ax5)
    ax2, ax4, ax6 = ignition_time_average_noise_check_seaborn.make_axes(dirname2, ax2, ax4, ax6)
    ax1.set_ylabel("Ignition time (s)")
    ax3.set_ylabel("Ignition time (s)")
    ax5.set_ylabel("Ignition time (s)")
    ax2.set_ylabel("")
    ax4.set_ylabel("")
    ax6.set_ylabel("")

    ax5.set_xlabel("Diameter (mm)")
    ax6.set_xlabel("Diameter (mm)")
    ax1.set_xlabel("")
    ax2.set_xlabel("")
    ax3.set_xlabel("")
    ax4.set_xlabel("")
    ax1.get_xaxis().set_ticklabels([])
    ax2.get_xaxis().set_ticklabels([])
    ax3.get_xaxis().set_ticklabels([])
    ax4.get_xaxis().set_ticklabels([])

    titlefont = {'fontweight':'heavy', 'size':36}
    ax4.legend(loc=2, frameon=True)

    ax1.set_title('a', loc='left', fontdict=titlefont)
    ax2.set_title('d', loc='left', fontdict=titlefont)
    ax3.set_title('b', loc='left', fontdict=titlefont)
    ax4.set_title('e', loc='left', fontdict=titlefont)
    ax5.set_title('c', loc='left', fontdict=titlefont)
    ax6.set_title('f', loc='left', fontdict=titlefont)

    ax1.set_title(get_plot_title(dirname1), loc='center')
    ax2.set_title(get_plot_title(dirname2), loc='center')

    #plt.subplots_adjust(left=0.1, wspace=0.1, top=0.8)
    plt.subplots_adjust(hspace=0.1, wspace=0.15)
    #plt.savefig('figs/ignition_6pane.png')
    plt.savefig('figs/ignition_6pane.pdf', bbox_inches='tight')
    #plt.show(block=True)
    return(0)

if __name__ == '__main__':
    main()
