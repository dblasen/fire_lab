import os
import sys
import csv
import time
import string
import operator
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate
import scipy.stats
import scipy.spatial
import seaborn as sns
import pandas as pd
from matplotlib import gridspec
import matplotlib.image as mpimg


def main():
    imgpath1 = sys.argv[1]
    imgpath2 = sys.argv[2]
    imgpath3 = sys.argv[3]

    #sns.set(font_scale=2)
    #sns.set_style('whitegrid', {"grid.linewidth":50.0, "grid.color":"000000"})
    fig = plt.figure(figsize = (16,20))
    gs = gridspec.GridSpec(3,1, height_ratios=[1,1,1])
    plt.rc('text', usetex=True)
    ax1 = plt.subplot(gs[0])
    ax2 = plt.subplot(gs[1])
    ax3 = plt.subplot(gs[2])

    img1 = mpimg.imread(imgpath1)
    img2 = mpimg.imread(imgpath2)
    img3 = mpimg.imread(imgpath3)

    ax1.set_xticklabels([])
    ax1.set_yticklabels([])
    ax1.grid(False)
    ax2.set_xticklabels([])
    ax2.set_yticklabels([])
    ax2.grid(False)
    ax3.set_xticklabels([])
    ax3.set_yticklabels([])
    ax3.grid(False)

    ax1.imshow(img1)
    ax2.imshow(img2)
    ax3.imshow(img3)

    plt.subplots_adjust(hspace=0.1)

    plt.savefig('figs/compositions_6pane.pdf', bbox_inches='tight')
    return(0)

if __name__ == '__main__':
    main()
