import os
import sys
import csv
import time
import string
import operator
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate
import scipy.stats
import scipy.spatial
import seaborn as sns
import pandas as pd
from matplotlib import gridspec
import consumption_plot

def get_plot_title(dirname):
    if 'quarter' in dirname:
        return(r"$Flux=112.5 kW/m^2$")
    elif 'half' in dirname:
        return(r"$Flux=225 kW/m^2$")
    elif 'full' in dirname:
        return(r"$Flux=450 kW/m^2$")
    elif 'double' in dirname:
        return(r"$Flux=900 kW/m^2$")
    else:
        return("Flux Unknown")

    #half consumption ignition times:
    #thick:
    #   5: 20   6: 14   7: 31
    #thin:
    #   5: 32   6: 24   7: 46

#def make_one_ax_dense(ax, dirname1, dirname1a, dirname2, dirname2a, dirname3, dirname3a, timelen, plot_temps = False):

def main(plot_size = 'large'):
    dirname1 = sys.argv[1]
    dirname2 = sys.argv[2]
    plot_size = sys.argv[3]
    #plot_temps = False
    plot_temps = True

    sns.set(font_scale=2)
    sns.set_style('whitegrid', {"grid.linewidth":50.0, "grid.color":"000000"})
    fig = plt.figure(figsize = (16,20))
    gs = gridspec.GridSpec(3,2, height_ratios=[1,1,1], width_ratios=[1,1])
    plt.rc('text', usetex=True)
    ax1 = plt.subplot(gs[0,0])
    ax2 = plt.subplot(gs[1,0])
    ax3 = plt.subplot(gs[2,0])
    ax4 = plt.subplot(gs[0,1])
    ax5 = plt.subplot(gs[1,1])
    ax6 = plt.subplot(gs[2,1])

    large_dirname1_thick_5 = dirname1  + '/thick/0p5cm/albini_5/'
    large_dirname1_thick_6 = dirname1  + '/thick/0p5cm/albini_6/'
    large_dirname1_thick_7 = dirname1  + '/thick/0p5cm/albini_7/'
    large_dirname1_thin_5 = dirname1   + '/thin/0p5cm/albini_5/'
    large_dirname1_thin_6 = dirname1   + '/thin/0p5cm/albini_6/'
    large_dirname1_thin_7 = dirname1   + '/thin/0p5cm/albini_7/'
    large_dirname2_thick_5 = dirname2  + '/thick/0p5cm/albini_5/'
    large_dirname2_thick_6 = dirname2  + '/thick/0p5cm/albini_6/'
    large_dirname2_thick_7 = dirname2  + '/thick/0p5cm/albini_7/'
    large_dirname2_thin_5 = dirname2   + '/thin/0p5cm/albini_5/'
    large_dirname2_thin_6 = dirname2   + '/thin/0p5cm/albini_6/'
    large_dirname2_thin_7 = dirname2   + '/thin/0p5cm/albini_7/'

    med_dirname1_thick_5 = dirname1  + '/thick/0p3cm/albini_5/'
    med_dirname1_thick_6 = dirname1  + '/thick/0p3cm/albini_6/'
    med_dirname1_thick_7 = dirname1  + '/thick/0p3cm/albini_7/'
    med_dirname1_thin_5 = dirname1   + '/thin/0p3cm/albini_5/'
    med_dirname1_thin_6 = dirname1   + '/thin/0p3cm/albini_6/'
    med_dirname1_thin_7 = dirname1   + '/thin/0p3cm/albini_7/'
    med_dirname2_thick_5 = dirname2  + '/thick/0p3cm/albini_5/'
    med_dirname2_thick_6 = dirname2  + '/thick/0p3cm/albini_6/'
    med_dirname2_thick_7 = dirname2  + '/thick/0p3cm/albini_7/'
    med_dirname2_thin_5 = dirname2   + '/thin/0p3cm/albini_5/'
    med_dirname2_thin_6 = dirname2   + '/thin/0p3cm/albini_6/'
    med_dirname2_thin_7 = dirname2   + '/thin/0p3cm/albini_7/'

    small_dirname1_thick_5 = dirname1  + '/thick/0p1cm/albini_5/'
    small_dirname1_thick_6 = dirname1  + '/thick/0p1cm/albini_6/'
    small_dirname1_thick_7 = dirname1  + '/thick/0p1cm/albini_7/'
    small_dirname1_thin_5 = dirname1   + '/thin/0p1cm/albini_5/'
    small_dirname1_thin_6 = dirname1   + '/thin/0p1cm/albini_6/'
    small_dirname1_thin_7 = dirname1   + '/thin/0p1cm/albini_7/'
    small_dirname2_thick_5 = dirname2  + '/thick/0p1cm/albini_5/'
    small_dirname2_thick_6 = dirname2  + '/thick/0p1cm/albini_6/'
    small_dirname2_thick_7 = dirname2  + '/thick/0p1cm/albini_7/'
    small_dirname2_thin_5 = dirname2   + '/thin/0p1cm/albini_5/'
    small_dirname2_thin_6 = dirname2   + '/thin/0p1cm/albini_6/'
    small_dirname2_thin_7 = dirname2   + '/thin/0p1cm/albini_7/'

    if(plot_size == 'large'):
        p61 = large_dirname1_thick_6
        p51 = large_dirname1_thick_5
        p71 = large_dirname1_thick_7
        p62 = large_dirname2_thick_6
        p52 = large_dirname2_thick_5
        p72 = large_dirname2_thick_7
        p61b = large_dirname1_thin_6
        p51b = large_dirname1_thin_5
        p71b = large_dirname1_thin_7
        p62b = large_dirname2_thin_6
        p52b = large_dirname2_thin_5
        p72b = large_dirname2_thin_7
    elif(plot_size == 'small'):
        p61 = small_dirname1_thick_6
        p51 = small_dirname1_thick_5
        p71 = small_dirname1_thick_7
        p62 = small_dirname2_thick_6
        p52 = small_dirname2_thick_5
        p72 = small_dirname2_thick_7
        p61b = small_dirname1_thin_6
        p51b = small_dirname1_thin_5
        p71b = small_dirname1_thin_7
        p62b = small_dirname2_thin_6
        p52b = small_dirname2_thin_5
        p72b = small_dirname2_thin_7
    elif(plot_size == 'med'):
        p61 = med_dirname1_thick_6
        p51 = med_dirname1_thick_5
        p71 = med_dirname1_thick_7
        p62 = med_dirname2_thick_6
        p52 = med_dirname2_thick_5
        p72 = med_dirname2_thick_7
        p61b = med_dirname1_thin_6
        p51b = med_dirname1_thin_5
        p71b = med_dirname1_thin_7
        p62b = med_dirname2_thin_6
        p52b = med_dirname2_thin_5
        p72b = med_dirname2_thin_7

    timelen = 5
    ax1 = consumption_plot.make_one_ax(ax1, p61, p61b, timelen, plot_temps)
    ax2 = consumption_plot.make_one_ax(ax2, p51, p51b, timelen, plot_temps)
    ax3 = consumption_plot.make_one_ax(ax3, p71, p71b, timelen, plot_temps)
    ax4 = consumption_plot.make_one_ax(ax4, p62, p62b, timelen, plot_temps)
    ax5 = consumption_plot.make_one_ax(ax5, p52, p52b, timelen, plot_temps)
    ax6 = consumption_plot.make_one_ax(ax6, p72, p72b, timelen, plot_temps)
    if(plot_temps):
        ax1.set_ylabel("Surface Temperature")
        ax2.set_ylabel("Surface Temperature")
        ax3.set_ylabel("Surface Temperature")
        ax1.legend(loc=3, frameon=True)
    else:
        ax1.set_ylabel("Mass Fraction")
        ax2.set_ylabel("Mass Fraction")
        ax3.set_ylabel("Mass Fraction")
        ax4.legend(loc=1, frameon=True)
    ax4.set_ylabel("")
    ax5.set_ylabel("")
    ax6.set_ylabel("")

    ax3.set_xlabel("Time (s)")
    ax6.set_xlabel("Time (s)")
    ax1.set_xlabel("")
    ax2.set_xlabel("")
    ax4.set_xlabel("")
    ax5.set_xlabel("")
    ax1.get_xaxis().set_ticklabels([])
    ax2.get_xaxis().set_ticklabels([])
    ax5.get_xaxis().set_ticklabels([])
    ax4.get_xaxis().set_ticklabels([])

    titlefont = {'fontweight':'heavy', 'size':36}

    ax1.set_title('a', loc='left', fontdict=titlefont)
    ax2.set_title('b', loc='left', fontdict=titlefont)
    ax3.set_title('c', loc='left', fontdict=titlefont)
    ax4.set_title('d', loc='left', fontdict=titlefont)
    ax5.set_title('e', loc='left', fontdict=titlefont)
    ax6.set_title('f', loc='left', fontdict=titlefont)

    ax1.set_title(get_plot_title(dirname1), loc='center')
    ax4.set_title(get_plot_title(dirname2), loc='center')

    #plt.subplots_adjust(left=0.1, wspace=0.1, top=0.8)
    plt.subplots_adjust(hspace=0.1, wspace=0.15)
    #plt.savefig('figs/ignition_6pane.png')
    plt.savefig('figs/consumption_6pane.pdf', bbox_inches='tight')
    #plt.show(block=True)
    return(0)

if __name__ == '__main__':
    main()
