import os
import sys
import csv
import time
import string
import operator
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate
import scipy.stats
import scipy.spatial
import seaborn as sns
import pandas as pd
from matplotlib import gridspec
import directed_hausdorff_mass_loss




def main():
    dirname1 = sys.argv[1]
    dirname2 = sys.argv[2]

    sns.set_style('whitegrid', {"grid.linewidth":50.0, "grid.color":"000000"})
    fig = plt.figure(figsize = (12,12))
    gs = gridspec.GridSpec(2,1, height_ratios=[1,1])
    plt.rc('text', usetex=True)
    ax2 = plt.subplot(gs[0])
    ax1 = plt.subplot(gs[1])

    #fig, (ax2, ax1, ax3, ax5, ax4, ax6) = plt.subplots(3,2,figsize=(8,10))
    ax1 = directed_hausdorff_mass_loss.one_ax_plot(dirname1,ax1)
    ax2 = directed_hausdorff_mass_loss.one_ax_plot(dirname2,ax2)
    ax1.set_ylabel("Hausdorff Distance")
    ax2.set_ylabel("Hausdorff Distance")

    ax2.set_xlabel("Diameter (mm)")
    ax1.set_xlabel("")

    titlefont = {'fontweight':'heavy', 'size':36}

    ax1.set_title('b', loc='left', fontdict=titlefont)
    ax2.set_title('a', loc='left', fontdict=titlefont)

    ax2.legend(loc=4)

    #plt.subplots_adjust(left=0.1, wspace=0.1, top=0.8)
    plt.subplots_adjust(hspace=0.4, wspace=0.3)
    plt.savefig('figs/hausdorff_2pane.png')
    #plt.show(block=True)
    return(0)

if __name__ == '__main__':
    main()
