import os
import sys
import csv
import time
import string
import operator
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate
import scipy.stats
import scipy.spatial
import seaborn as sns
import pandas as pd
from matplotlib import gridspec

sys.path.append('../../python_simulations/')
import plot_ignition_time_comparisons
import MLR_validation

def main():
    dirname1 = sys.argv[1] # ignition time directory
    dirname2 = sys.argv[2] # mass loss rate directory

    sns.set(font_scale=2)
    sns.set_style('whitegrid', {"grid.linewidth":50.0, "grid.color":"000000"})
    fig = plt.figure(figsize = (16,20))
    gs = gridspec.GridSpec(3,2, height_ratios=[1,1,1], width_ratios=[1,1])
    plt.rc('text', usetex=True)
    ax1 = plt.subplot(gs[0,0])
    ax2 = plt.subplot(gs[1,0])
    ax3 = plt.subplot(gs[2,0])
    ax4 = plt.subplot(gs[0,1])
    ax5 = plt.subplot(gs[1,1])
    ax6 = plt.subplot(gs[2,1])

    #def make_axes(dirname1,ax1, ax2, ax3):
    ax1, ax2, ax3 = plot_ignition_time_comparisons.make_axes(dirname1, ax1, ax2, ax3)
    ax5, ax4, ax6 = MLR_validation.make_axes(dirname2,ax5, ax4, ax6)

    ax6.set_xlabel("Time (s)")

    ax3.set_xlabel("Fuel Diameter (mm)")
    ax1.set_ylabel("Ignition Time Delay")
    ax2.set_ylabel("Ignition Time Delay")
    ax3.set_ylabel("Ignition Time Delay")
    ax1.get_xaxis().set_ticklabels([])
    ax2.get_xaxis().set_ticklabels([])

    titlefont = {'fontweight':'heavy', 'size':36}

    ax1.set_title('a', loc='left', fontdict=titlefont)
    ax2.set_title('b', loc='left', fontdict=titlefont)
    ax3.set_title('c', loc='left', fontdict=titlefont)
    ax4.set_title('d', loc='left', fontdict=titlefont)
    ax5.set_title('e', loc='left', fontdict=titlefont)
    ax6.set_title('f', loc='left', fontdict=titlefont)

    ax3.legend(loc=2)

    plt.subplots_adjust(hspace=0.2, wspace=0.2)
    plt.savefig('figs/validation.pdf')
    #plt.show(block=True)
    return(0)

if __name__ == '__main__':
    main()

