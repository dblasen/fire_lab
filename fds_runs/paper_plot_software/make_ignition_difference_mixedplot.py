import os
import sys
import csv
import time
import string
import operator
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate
import scipy.stats
import scipy.spatial
import seaborn as sns
import pandas as pd
from matplotlib import gridspec
import directed_hausdorff_mass_loss
import fractional_weight_loss_thin_vs_thick
import ignition_time_difference_averages_vs_diameter
import TGA_plot
import fixed_position_timelapse_thin_vs_thick

def main():
    dirname1 = sys.argv[1] #will take from this one when we plot the mini plots
    TGA_dir= str(sys.argv[1]) + "../albini_final/gpyro_output/"

    #thindir1 = str(sys.argv[1]) + "../comparisons_double_perturbances/comparisons_double_HRRPUA_perturbed_900001/thin/0p1cm/albini_5/"
    quarter_dir = str(sys.argv[1]) + "../comparisons_quarter_perturbances/"
    half_dir = str(sys.argv[1]) + "../comparisons_half_perturbances/"
    full_dir = str(sys.argv[1]) + "../comparisons_full_perturbances/"
    double_dir = str(sys.argv[1]) + "../comparisons_double_perturbances/"

    thick_dir = str(sys.argv[1]) + "../comparisons_full_perturbances/comparisons_full_HRRPUA_perturbed_450001/thick/0p5cm/albini_5/"
    thin_dir = str(sys.argv[1]) + "../comparisons_full_perturbances/comparisons_full_HRRPUA_perturbed_450001/thin/0p5cm/albini_5/"

    sns.set(font_scale=2)
    sns.set_style('whitegrid', {"grid.linewidth":50.0, "grid.color":"000000"})
    fig = plt.figure(figsize = (12,16))
    gs = gridspec.GridSpec(3,2, height_ratios=[1,1,1], width_ratios = [1,1])
    plt.rc('text', usetex=True)
    ax1 = plt.subplot(gs[0,0])
    ax2 = plt.subplot(gs[0,1])
    ax3 = plt.subplot(gs[1,0])
    ax4 = plt.subplot(gs[1,1])
    ax5 = plt.subplot(gs[2,0])
    ax6 = plt.subplot(gs[2,1])

    ax3,ax1,ax5 = ignition_time_difference_averages_vs_diameter.make_axes\
            (half_dir, full_dir, double_dir, quarter_dir, ax3, ax1, ax5)
    ax1.set_xticklabels([])
    ax3.set_xticklabels([])

    ax2, ax4 = fixed_position_timelapse_thin_vs_thick.make_both_ax(thick_dir, thin_dir, ax2, ax4)
    ax6 = TGA_plot.make_single_pane_plot_small_legend(TGA_dir, ax6)

    ax6.set_xticks([0,2000,4000,6000,8000])

    titlefont = {'fontweight':'heavy', 'size':36}

    ax1.set_title('a', loc='left', fontdict=titlefont)
    ax2.set_title('d', loc='left', fontdict=titlefont)
    ax3.set_title('b', loc='left', fontdict=titlefont)
    ax4.set_title('e', loc='left', fontdict=titlefont)
    ax5.set_title('c', loc='left', fontdict=titlefont)
    ax6.set_title('f', loc='left', fontdict=titlefont)

    ax1.legend(loc=2, frameon=True, handlelength=0.3)
    ax2.legend(loc=1, frameon=True, handlelength=0.3)
    ax6.legend(loc=1, frameon=True, handlelength=0.3)

    plt.subplots_adjust(hspace=0.2, wspace=0.25)
    #plt.savefig('figs/mixedbag.png')
    plt.savefig('figs/mixedbag.pdf', bbox_inches='tight')
    return(0)

if __name__ == '__main__':
    main()
