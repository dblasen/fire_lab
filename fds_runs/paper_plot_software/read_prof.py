import sys
import os

def read(filename):
    '''format on PROF files is weird, like this:
    time, position count, positions[0-count], values[0-count]

    so we can't use a normal csv dict reader here.
    returns:0 a list of {x[],y[],time} dicts
            1 a name
    '''
    with open(filename, 'r') as f:
        line1 = f.readline() #name of variable
        name = line1.strip()
        f.readline() #line2: description
        f.readline() #line3: blank
        first_data_line = True #capture initial dimensions of particle here
        output = []
        for line in f.readlines(): #line4+: data
            tokens = line.split(',')
            time = float(tokens[0])
            count = int(tokens[1])
            positions = [float(i) for i in tokens[2:count + 2]] #oh my god
            values = [float(i) for i in tokens[count + 2:]]     #it's so bad
            data = {'t':time, 'x':positions, 'y':values}
            output.append(data)
            if(first_data_line):
                first_data_line = False
                xdims = (positions[0], positions[-1])
        return(output, name, xdims)

if __name__ == "__main__":
    fname = sys.argv[1]
    fname = os.getcwd() + '/' + fname
    data = read(fname)
