import os
import sys
import csv
import time
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate
import seaborn as sns

def get_exp_data(pointfile):
    ignition_temp = 350
    ignition_mass = None
    ignition_time = None
    with open(pointfile, 'r') as f:
        values = []
        # skip first line (units -> s, C, C, C, ... C)
        f.next()
        reader = csv.DictReader(f)
        for row in reader:
            values.append(row)
        times = []
        masses = []
        initial_mass = 0.0 #will write over this in loop
        for timestep in values:
            if len(times) == 0 and is_ignited(timestep["surf_temp"],ignition_temp): #first time iteration
                initial_mass = float(timestep["mass"])
                ignition_time = float(timestep["Time"])
                times.append(0.0)
                masses.append(0.0)
            elif len(times) > 0:
                t = float(timestep["Time"]) - ignition_time
                times.append(t)
                mass = fractional_loss(float(timestep["mass"]) , initial_mass)
                masses.append(mass)
            else:pass
    if ((len(times) == 0) and (len(masses) == 0)):
            times.append(0.0)
            masses.append(0.0)
            ignition_time = 0
    return([times,masses], ignition_time)

def is_ignited(temperature, ignition_temp=350):
    return(float(temperature) > ignition_temp)

def fractional_loss(current, initial):
    return((initial - current)/initial)

def albini_fig_5_fn():
    xs = [0,60,120,180,240]
    ys = [0,0.07,0.13,0.20,0.265]
    points = [xs, ys]
    return(points)

def albini_fig_6_fn():
    xs = [0,100,200,300,400,500]
    ys = [0,0.10,0.15,0.23,0.29,0.35]
    points = [xs, ys]
    return(points)

def albini_fig_7_fn():
    xs = [0,120,240,360,480]
    ys = [0,0.03,0.055,0.085,0.105]
    points = [xs, ys]
    return(points)

def make_axes(dirname, ax1, ax2, ax3):
    pointfile_dir = os.getcwd() + "/" + dirname
    alb_data_5 = albini_fig_5_fn()
    alb_data_6 = albini_fig_6_fn()
    alb_data_7 = albini_fig_7_fn()
    #find our experiments so we can open them up
    dir_contents = os.listdir(pointfile_dir)
    fname = "albini_model_devc.csv"
    for item in dir_contents:
        if "_5" in item:
            exp_5, ignition5 = get_exp_data(dirname + '/' + item + '/' + fname)
        elif "_6" in item:
            exp_6, ignition6 = get_exp_data(dirname + '/' + item + '/' + fname)
        elif "_7" in item:
            exp_7, ignition7 = get_exp_data(dirname + '/' + item + '/' + fname)
        else:
            pass
    #plt.rc('text', usetex=True)
    ylims = [0,0.5]
    xlims = [0,300]
    #xtics = [0,60,120,180,240,300]
    #ax1.plot(exp_5[0], exp_5[1], color='black', marker='.', label='Simulation')
    ax1.plot(exp_5[0], exp_5[1], color='black', label='Simulation')
    #ax1.scatter(alb_data_5[0], alb_data_5[1],color='red')
    ax1.plot(alb_data_5[0], alb_data_5[1], color='red')
    ax1.set_ylabel("Fractional weight loss")
    ax1.set_ylim(ylims)
    #ax1.set_xticks(xtics)
    ax1.set_xlim(xlims)
    ylims = [0,0.5]
    xlims = [0,500]
    #xtics = [0,100,200,300,400,500]
    #ax2.plot(exp_6[0], exp_6[1], color='black', marker='.', label='Simulation')
    ax2.plot(exp_6[0], exp_6[1], color='black', label='Simulation')
    #ax2.scatter(alb_data_6[0], alb_data_6[1],color='red')
    ax2.plot(alb_data_6[0], alb_data_6[1], color='red')
    ax2.set_ylabel("Fractional weight loss")
    ax2.set_ylim(ylims)
    #ax2.set_xticks(xtics)
    ax2.set_xlim([0,500])
    ylims = [0,0.5]
    xlims = [0,600]
    #xtics = [0,120,240,360,480,600]
    #ax3.plot(exp_7[0], exp_7[1], color='black', marker='.', label='Simulation')
    ax3.plot(alb_data_7[0], alb_data_7[1], color='red', label='Experiment')
    ax3.plot(exp_7[0], exp_7[1], color='black', label='Simulation')
    #ax3.scatter(alb_data_7[0], alb_data_7[1],color='red')
    ax3.set_ylabel("Fractional weight loss")
    ax3.legend()
    ax3.set_ylim(ylims)
    #ax3.set_xticks(xtics)
    ax3.set_xlim([0,480])
    return(ax1, ax2, ax3)

def main():
    dirname = sys.argv[1]
    pointfile_dir = os.getcwd() + "/" + dirname
    alb_data_5 = albini_fig_5_fn()
    alb_data_6 = albini_fig_6_fn()
    alb_data_7 = albini_fig_7_fn()
    #find our experiments so we can open them up
    dir_contents = os.listdir(pointfile_dir)
    fname = "albini_model_devc.csv"
    for item in dir_contents:
        if "_5" in item:
            exp_5, ignition5 = get_exp_data(dirname + '/' + item + '/' + fname)
        elif "_6" in item:
            exp_6, ignition6 = get_exp_data(dirname + '/' + item + '/' + fname)
        elif "_7" in item:
            exp_7, ignition7 = get_exp_data(dirname + '/' + item + '/' + fname)
        else:
            pass
    #plt.rc('text', usetex=True)
    sns.set_style('whitegrid')
    sns.set(font_scale=2)
    fig = plt.figure(figsize = (24,8))
    ylims = [0,0.4]
    xlims = [0,300]
    xtics = [0,60,120,180,240,300]
    ax1 = fig.add_subplot(131)
    ax1.plot(exp_5[0], exp_5[1], color='black', marker='.', label='Simulation')
    ax1.scatter(alb_data_5[0], alb_data_5[1],color='red')
    ax1.plot(alb_data_5[0], alb_data_5[1], color='red', marker='.')
    ax1.set_title(str("4.8cm, 21% moisture (t(0) = {0})".format(round(ignition5))))
    ax1.set_xlabel("Time (s)")
    ax1.set_ylabel("Fractional weight loss")
    ax1.set_ylim(ylims)
    ax1.set_xticks(xtics)
    ax1.set_xlim(xlims)
    ylims = [0,0.4]
    xlims = [0,500]
    xtics = [0,100,200,300,400,500]
    ax2 = fig.add_subplot(132)
    ax2.plot(exp_6[0], exp_6[1], color='black', marker='.', label='Simulation')
    ax2.scatter(alb_data_6[0], alb_data_6[1],color='red')
    ax2.plot(alb_data_6[0], alb_data_6[1], color='red', marker='.')
    ax2.set_title(str("10.7cm, 4% moisture (t(0) = {0})".format(round(ignition6))))
    ax2.set_xlabel("Time (s)")
    ax2.set_ylim(ylims)
    ax2.set_xticks(xtics)
    ax2.set_xlim([0,500])
    ylims = [0,0.4]
    xlims = [0,600]
    xtics = [0,120,240,360,480,600]
    ax3 = fig.add_subplot(133)
    ax3.plot(exp_7[0], exp_7[1], color='black', marker='.', label='Simulation')
    ax3.scatter(alb_data_7[0], alb_data_7[1],color='red', label='Experiment')
    ax3.plot(alb_data_7[0], alb_data_7[1], color='red', marker='.')
    ax3.set_title(str("10.7cm, 60% moisture(t(0) = {0})".format(round(ignition7))))
    ax3.set_xlabel("Time (s)")
    ax3.legend()
    ax3.set_ylim(ylims)
    ax3.set_xticks(xtics)
    ax3.set_xlim([0,480])
    plt.subplots_adjust(left=0.1, wspace=0.1, top=0.8)
    plt.show(block=True)

if __name__ == '__main__':
    main()
