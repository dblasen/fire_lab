import os
import sys
import csv
import time
import matplotlib.pyplot as plt
from matplotlib import gridspec
import numpy as np
import scipy.interpolate
import pandas as pd
import seaborn as sns

from operator import add

import read_prof

def density_remove(point, density=1):
    return(float(point / density))

def is_ignited(temperature, ignition_temp=350):
    return(float(temperature) > ignition_temp)

def fractional_loss(current, initial):
    return((initial - current)/initial)

def albini_fig_5_fn():
    xs = [0,60,120,180,240]
    ys = [0,0.07,0.13,0.20,0.265]
    points = [xs, ys]
    return(points)

def albini_fig_6_fn():
    xs = [0,100,200,300,400,500]
    ys = [0,0.10,0.15,0.23,0.29,0.35]
    points = [xs, ys]
    return(points)

def albini_fig_7_fn():
    xs = [0,120,240,360,480]
    ys = [0,0.03,0.055,0.085,0.105]
    points = [xs, ys]
    return(points)

def adjust_exp_numbers(exp, sim_time, sim_mass):
    '''move experimental data to our point of ignition'''
    new_x = []
    new_y = []
    for x in exp[0]:
        new_x.append(float(x) + float(sim_time))
    for y in exp[1]:
        new_y.append(float(y) + float(sim_mass))
    return([new_x,new_y])

def get_sim_ignition_data(times, masses, surf_temps, ignition_temp=350.0):
    '''get the time and mass of ignition'''
    ignition_time = 0
    ignition_mass = 0
    for i in range(len(times)):
        if is_ignited(surf_temps[i],ignition_temp):
            ignition_time = times[i]
            ignition_mass = masses[i]
            break
    return(ignition_time, ignition_mass)

def get_times_masses_surf_temps(points):
    '''inbound list of dicts with loose strings; get strings'''
    times = []
    masses = []
    surf_temps = []
    for dictionary in points:
        times.append(float(dictionary["t"]))
        masses.append(float(dictionary["mass"]))
        surf_temps.append(float(dictionary["surf_temp"]))
    return(times,masses,surf_temps)

#wet_wood_density_DV0001
def get_points(pointfile, tags):
    with open(pointfile, 'r') as f:
        # skip first line (units -> s, C, C, C, ... C)
        f.next()
        reader = csv.DictReader(f)
        values = []
        for row in reader:
            values.append(row)
        time_points = []
        initial_mass = 0.0
        for timestep in values:
            if len(time_points) == 0: #first time iteration
                initial_mass = float(timestep["mass"])
            time_point = {}
            mass = fractional_loss(float(timestep["mass"]) , initial_mass)
            time_point["t"] = float(timestep["Time"])
            time_point["mass"] = float(mass)
            time_point["surf_temp"] = float(timestep["surf_temp"])
            for key,value in timestep.iteritems():
                for tag in tags:
                    if tag in key:
                        x = int(key[-4:]) #last 4 chars -> depth in mm
                        if x in time_point:
                            inner = time_point[x]
                        else:
                            time_point[x] = {}
                            inner = time_point[x]
                        inner[tag] = float(value)
            if time_point: #if dict has data...
                time_points.append(time_point)
    return(time_points)

def make_new_profile_xlims(xdims):
    '''some tuple like (0,0.1) that describes distance to center,
    but we want (-0.1, 0.1) with some wiggle room (10%)?
    '''
    return(-1 * abs(xdims[0] - xdims[1])*1.1, abs(xdims[0] - xdims[1])*1.1)

def make_profile_shape_pos(vector):
    '''same as below but with the x positions'''
    # [0, 0.1, 0.2] IN DEPTH which makes this annoying
    # to
    # [-0.2, -0.1, 0, 0, 0.1, 0.2]
    vec_max = abs(vector[-1] - vector[0])
    first = []
    for elt in vector[::-1]:
        first.append(-1 * abs(elt))
    nu_vector = first + vector
    return(nu_vector)

def make_profile_shape_vals(vector):
    '''data will be shaped for surface to half-depth
    but we want surface to opposite surface (mirror in middle)
    so we reshape it'''
    # [500,600,600]
    # to
    # [500, 600, 600, 600, 600, 500]
    rev_vector = vector[::-1]
    nu_vector = vector + rev_vector
    return(nu_vector)

def get_ts(data):
    '''
    not sure if needed but why not.
    get all the times in a list from the data.
    '''
    ts = []
    for elt in data:
        ts.append(elt['t'])
    return(ts)

def get_thin_fuel_temps(data):
    '''it wants to be special so we'll treat it special'''
    ys = []
    for elt in data:
        ys.append(elt['y'][-1])
    return(ys)

def get_all_ys_at_x(x,data):
    ''' 
    given some location, we should be able to look inside the structure
    containing the data and pull out the value we want.
    e.g. the moisture density at 0.0001m for the whole run
    '''
    #ok so: dry is a list of dicts like:
    #[x] (list)
    #[y] (list)
    #t (float)
    ys = []
    for elt in data:
        i = 0
        for item in elt['x']:
            if (x > elt['x'][-1]):
                break
            if (elt['x'][i] <= x and elt['x'][i+1] >= x):
                ys.append(elt['y'][i])
                break
            i += 1
    return(ys)

def stacked_line_plot(points, xdims, dry, char, ash, moisture, temp, title, ax, depth = 0.0005, xlim = 30, rhs = True):
    p1_xdim1, p1_xdim2 = make_new_profile_xlims(xdims)
    #p2_xlims = [0,300] #NOTE not sure if this is a good choice
    p2_ylims = [0,1.0]
    #p2_xtics = [i for i in xrange(0,300,30)]
    times, masses, surf_temps = get_times_masses_surf_temps(points)
    sim = [times,masses] #laziness for below
    initial_mass = masses[0]
    ignition_time, ignition_mass = get_sim_ignition_data(times, masses, surf_temps)
    ts = get_ts(dry)
    if(title == "thin"):
        temps_list = get_thin_fuel_temps(temp)
    else:
        temps_list = get_all_ys_at_x(depth, temp)
    dry_list = get_all_ys_at_x(depth, dry)
    moisture_list = get_all_ys_at_x(depth, moisture)
    ash_list = get_all_ys_at_x(depth, ash)
    char_list = get_all_ys_at_x(depth, char)
    xs  = ts[0:len(moisture_list)]
    Ms = moisture_list
    DVs = dry_list
    CHs = char_list
    ASs = ash_list
    temps = temps_list
    total_density = Ms[0] + DVs[0] + CHs[0]
    Ms = [i / total_density for i in Ms]
    DVs = [i / total_density for i in DVs]
    CHs = [i / total_density for i in CHs]
    #add these in place to make stacked values
    Ms = Ms
    DVs = map(add, Ms, DVs)
    CHs = map(add, DVs, CHs)
    ASs = map(add, CHs, ASs)
    #first plot: mass fraction as fn of distance
    ax.clear()
    ax.plot(xs, Ms, color='blue', marker='.', label='Water')
    ax.plot(xs, DVs, color='brown', marker='.', label='Wood')
    ax.plot(xs, CHs, color='black', marker='.', label='Char')
    ax.fill_between(xs, 0, Ms, facecolor = "blue", alpha = 0.6)
    ax.fill_between(xs, Ms, DVs, facecolor = "brown", alpha = 0.6)
    ax.fill_between(xs, DVs, CHs, facecolor = "black", alpha = 0.6)
    #density is in meters, so let's print mm instead
    mm = depth * 1000
    ax.set_title("Depth=%.2fmm (%s)"  % (mm, title))
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Solid Mass Fraction")
    ax.set_autoscale_on(False)
    ax.set_ylim(0,1)
    ax.set_xlim(0,xlim)
    ax.axhline(y=0.7, label="Ignition Temp", color = 'orange', linestyle='--') #it's a hack to get it in legend
    #if(title == "thick"):
    #if(title):
        #ax.legend(loc=1)
        #ax.set_xticklabels([])
        #ax.set_xlabel("")
    ax_t = ax.twinx()
    ax_t.set_autoscale_on(False)
    if(len(xs) < len(temps)):
        temps = temps[0:len(xs)]
    ax_t.plot(xs, temps, color = 'red', label='Temperature')
    if(rhs):
        ax_t.set_ylabel('Temperature', color = 'red')
        #ax_t.yaxis.label.set_color('red')
        ax_t.tick_params(axis='y', colors='red')
    else:
        ax_t.set_yticklabels([])
    ax_t.set_ylim([0,500])
    ax_t.grid(False)
    i = 0
    return(ax)

def get_experiment_number(directory):
    if "_5" in directory:
        return(5)
    elif "_6" in directory:
        return(6)
    elif "_7" in directory:
        return(7)
    else:
        raise(Exception("Directory must end in '_5', '_6', or '_7'!"))
    return(-1)

def make_one_ax(dirname, ax, depth = 0.0005, title = "thick", xlim = 30, rhs = True):
    pointfile_dir = os.getcwd() + "/" + dirname
    exp_number = get_experiment_number(pointfile_dir)
    if(exp_number == 5):
        exp = albini_fig_5_fn()
    elif(exp_number == 6):
        exp = albini_fig_6_fn()
    elif(exp_number == 7):
        exp = albini_fig_7_fn()
    else:
        raise("invalid albini plot number; what happened?")
    prof_file_1, n1, dims1 = read_prof.read(pointfile_dir + "/albini_model_prof_01.csv") #temperature?
    prof_file_2, n2, dims2 = read_prof.read(pointfile_dir + "/albini_model_prof_02.csv") #dryveg?
    prof_file_3, n3, dims3 = read_prof.read(pointfile_dir + "/albini_model_prof_03.csv") #char?
    prof_file_4, n4, dims4 = read_prof.read(pointfile_dir + "/albini_model_prof_04.csv") #ash?
    prof_file_5, n5, dims5 = read_prof.read(pointfile_dir + "/albini_model_prof_05.csv") #moisture?
    if(dims1 != dims2 or dims1 != dims3 or dims1 != dims4 or dims1 != dims5):
        raise(Exception("Particle sizes do not match; check profile output for dimensions?"))
    pointfile = pointfile_dir + "/albini_model_devc.csv"
    tags = ["M","DV", "CH", "AS"]
    points = get_points(pointfile, tags)
    ax = stacked_line_plot(points, dims1, prof_file_2, prof_file_3, prof_file_4, prof_file_5, prof_file_1, title, ax, depth, xlim, rhs)
    return(ax)

def make_both_ax(dirname, dirname2,ax1, ax2):
    pointfile_dir = os.getcwd() + "/" + dirname
    pointfile_dir2 = os.getcwd() + "/" + dirname2
    exp_number = get_experiment_number(pointfile_dir)
    if(exp_number == 5):
        exp = albini_fig_5_fn()
    elif(exp_number == 6):
        exp = albini_fig_6_fn()
    elif(exp_number == 7):
        exp = albini_fig_7_fn()
    else:
        raise("invalid albini plot number; what happened?")
    prof_file_1, n1, dims1 = read_prof.read(pointfile_dir + "/albini_model_prof_01.csv") #temperature?
    prof_file_2, n2, dims2 = read_prof.read(pointfile_dir + "/albini_model_prof_02.csv") #dryveg?
    prof_file_3, n3, dims3 = read_prof.read(pointfile_dir + "/albini_model_prof_03.csv") #char?
    prof_file_4, n4, dims4 = read_prof.read(pointfile_dir + "/albini_model_prof_04.csv") #ash?
    prof_file_5, n5, dims5 = read_prof.read(pointfile_dir + "/albini_model_prof_05.csv") #moisture?
    if(dims1 != dims2 or dims1 != dims3 or dims1 != dims4 or dims1 != dims5):
        raise(Exception("Particle sizes do not match; check profile output for dimensions?"))
    prof_file_12, n12, dims12 = read_prof.read(pointfile_dir2 + "/albini_model_prof_01.csv") #temperature?
    prof_file_22, n22, dims22 = read_prof.read(pointfile_dir2 + "/albini_model_prof_02.csv") #dryveg?
    prof_file_32, n32, dims32 = read_prof.read(pointfile_dir2 + "/albini_model_prof_03.csv") #char?
    prof_file_42, n42, dims42 = read_prof.read(pointfile_dir2 + "/albini_model_prof_04.csv") #ash?
    prof_file_52, n52, dims52 = read_prof.read(pointfile_dir2 + "/albini_model_prof_05.csv") #moisture?
    if(dims12 != dims22 or dims12 != dims32 or dims12 != dims42 or dims12 != dims52):
        raise(Exception("Particle sizes do not match; check profile output for dimensions?"))
    pointfile = pointfile_dir + "/albini_model_devc.csv"
    tags = ["M","DV", "CH", "AS"]
    points = get_points(pointfile, tags)
    pointfile2 = pointfile_dir2 + "/albini_model_devc.csv"
    points2 = get_points(pointfile2, tags)
    ax1 = stacked_line_plot(points, dims1, prof_file_2, prof_file_3, prof_file_4, prof_file_5, prof_file_1, "thick", ax1)
    ax2 = stacked_line_plot(points2, dims12, prof_file_22, prof_file_32, prof_file_42, prof_file_52, prof_file_12, "thin", ax2)
    return(ax1,ax2)

def simplemain():
    dirname = sys.argv[1] #thick
    dirname2 = sys.argv[2] #thin
    sns.set_style('ticks')
    fig = plt.figure(figsize = (6,12))
    gs = gridspec.GridSpec(2,1, height_ratios=[1,1])
    plt.rc('text', usetex=True)
    ax1 = plt.subplot(gs[0])
    ax2 = plt.subplot(gs[1])
    ax1, ax2 = make_both_ax(dirname, dirname2, ax1, ax2)
    fig.canvas.draw()
    i=0
    plt.savefig('figs/pyrolysis.png')

def main():
    dirname = sys.argv[1] #thick
    dirname2 = sys.argv[2] #thin
    title = sys.argv[3]
    pointfile_dir = os.getcwd() + "/" + dirname
    pointfile_dir2 = os.getcwd() + "/" + dirname2
    exp_number = get_experiment_number(pointfile_dir)
    if(exp_number == 5):
        exp = albini_fig_5_fn()
    elif(exp_number == 6):
        exp = albini_fig_6_fn()
    elif(exp_number == 7):
        exp = albini_fig_7_fn()
    else:
        raise("invalid albini plot number; what happened?")

    prof_file_1, n1, dims1 = read_prof.read(pointfile_dir + "/albini_model_prof_01.csv") #temperature?
    prof_file_2, n2, dims2 = read_prof.read(pointfile_dir + "/albini_model_prof_02.csv") #dryveg?
    prof_file_3, n3, dims3 = read_prof.read(pointfile_dir + "/albini_model_prof_03.csv") #char?
    prof_file_4, n4, dims4 = read_prof.read(pointfile_dir + "/albini_model_prof_04.csv") #ash?
    prof_file_5, n5, dims5 = read_prof.read(pointfile_dir + "/albini_model_prof_05.csv") #moisture?
    if(dims1 != dims2 or dims1 != dims3 or dims1 != dims4 or dims1 != dims5):
        raise(Exception("Particle sizes do not match; check profile output for dimensions?"))

    prof_file_12, n12, dims12 = read_prof.read(pointfile_dir2 + "/albini_model_prof_01.csv") #temperature?
    prof_file_22, n22, dims22 = read_prof.read(pointfile_dir2 + "/albini_model_prof_02.csv") #dryveg?
    prof_file_32, n32, dims32 = read_prof.read(pointfile_dir2 + "/albini_model_prof_03.csv") #char?
    prof_file_42, n42, dims42 = read_prof.read(pointfile_dir2 + "/albini_model_prof_04.csv") #ash?
    prof_file_52, n52, dims52 = read_prof.read(pointfile_dir2 + "/albini_model_prof_05.csv") #moisture?
    if(dims12 != dims22 or dims12 != dims32 or dims12 != dims42 or dims12 != dims52):
        raise(Exception("Particle sizes do not match; check profile output for dimensions?"))

    pointfile = pointfile_dir + "/albini_model_devc.csv"
    tags = ["M","DV", "CH", "AS"]
    points = get_points(pointfile, tags)

    pointfile2 = pointfile_dir2 + "/albini_model_devc.csv"
    points2 = get_points(pointfile2, tags)

    sns.set_style('ticks')
    fig = plt.figure(figsize = (6,12))
    gs = gridspec.GridSpec(2,1, height_ratios=[1,1])
    plt.rc('text', usetex=True)
    ax1 = plt.subplot(gs[0])
    ax2 = plt.subplot(gs[1])

    ax1 = stacked_line_plot(points, dims1, prof_file_2, prof_file_3, prof_file_4, prof_file_5, prof_file_1, "thick", ax1)
    ax2 = stacked_line_plot(points2, dims12, prof_file_22, prof_file_32, prof_file_42, prof_file_52, prof_file_12, "thin", ax2)
    fig.canvas.draw()
    i=0
    plt.savefig('figs/mass_loss_comparison/pyrolysis_%s.png' % str('%05d' % i))

if __name__ == "__main__":
    simplemain()
