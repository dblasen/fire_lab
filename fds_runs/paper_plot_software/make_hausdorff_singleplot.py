import os
import sys
import csv
import time
import string
import operator
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate
import scipy.stats
import scipy.spatial
import seaborn as sns
import pandas as pd
from matplotlib import gridspec
import directed_hausdorff_mass_loss
import fractional_weight_loss_thin_vs_thick

def get_plot_title(dirname):
    if 'quarter' in dirname:
        return(r"$Flux=112.5 kW/m^2$")
    elif 'half' in dirname:
        return(r"$Flux=225 kW/m^2$")
    elif 'full' in dirname:
        return(r"$Flux=450 kW/m^2$")
    elif 'double' in dirname:
        return(r"$Flux=900 kW/m^2$")
    else:
        return("Flux Unknown")


def main():
    dirname1 = sys.argv[1] #will take from this one when we plot the mini plots
    #dirname2 = sys.argv[2] #or just use normal HRRPUA for ease...

    #thindir1 = str(sys.argv[1]) + "../comparisons_full_perturbances/comparisons_full_HRRPUA_perturbed_450001/thin/0p1cm/albini_5/"
    #thindir5 = str(sys.argv[1]) + "../comparisons_full_perturbances/comparisons_full_HRRPUA_perturbed_450001/thin/0p5cm/albini_5/"
    #thickdir1 = str(sys.argv[1]) + "../comparisons_full_perturbances/comparisons_full_HRRPUA_perturbed_450001/thick/0p1cm/albini_5/"
    #thickdir5 = str(sys.argv[1]) + "../comparisons_full_perturbances/comparisons_full_HRRPUA_perturbed_450001/thick/0p5cm/albini_5/"

#    thindir1 = str(sys.argv[1]) + "../comparisons_quarter_perturbances/comparisons_quarter_HRRPUA_perturbed_112501/thin/0p1cm/albini_5/"
#    thindir5 = str(sys.argv[1]) + "../comparisons_quarter_perturbances/comparisons_quarter_HRRPUA_perturbed_112501/thin/0p5cm/albini_5/"
#    thickdir1 = str(sys.argv[1]) + "../comparisons_quarter_perturbances/comparisons_quarter_HRRPUA_perturbed_112501/thick/0p1cm/albini_5/"
#    thickdir5 = str(sys.argv[1]) + "../comparisons_quarter_perturbances/comparisons_quarter_HRRPUA_perturbed_112501/thick/0p5cm/albini_5/"

    thindir1 = str(sys.argv[1]) + "../comparisons_double_perturbances/comparisons_double_HRRPUA_perturbed_900001/thin/0p1cm/albini_5/"
    thindir5 = str(sys.argv[1]) + "../comparisons_double_perturbances/comparisons_double_HRRPUA_perturbed_900001/thin/0p5cm/albini_5/"
    thickdir1 = str(sys.argv[1]) + "../comparisons_double_perturbances/comparisons_double_HRRPUA_perturbed_900001/thick/0p1cm/albini_5/"
    thickdir5 = str(sys.argv[1]) + "../comparisons_double_perturbances/comparisons_double_HRRPUA_perturbed_900001/thick/0p5cm/albini_5/"

    sns.set(font_scale=2)
    sns.set_style('whitegrid', {"grid.linewidth":50.0, "grid.color":"000000"})
    fig = plt.figure(figsize = (4,3))
    gs = gridspec.GridSpec(1,1)
    plt.rc('text', usetex=True)
    ax1 = plt.subplot(gs[0])

    ax1 = directed_hausdorff_mass_loss.one_ax_plot(dirname1,ax1)

    ax1.set_ylabel("Hausdorff Distance")

    ax1.set_xlabel("Diameter (mm)")

    titlefont = {'fontweight':'heavy', 'size':36}

    ax1.set_title('a', loc='left', fontdict=titlefont)

    #ax3.set_title("", loc='right')

    ax1.set_title(get_plot_title(dirname1), loc='center')

    ax1.get_xaxis().set_ticklabels([])

    ax1.legend(loc=4, frameon=True)

    plt.subplots_adjust(hspace=0.1, wspace=0.2)
    plt.savefig('figs/hausdorff_1.png')
    #plt.savefig('figs/hausdorff_mixedpane.eps',format='eps', dpi=20)
    return(0)

if __name__ == '__main__':
    main()
