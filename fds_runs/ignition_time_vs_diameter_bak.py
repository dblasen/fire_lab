import os
import sys
import csv
import time
import string
import operator
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate

def get_ignition_time(pointfile):
    ignition_temp = 350
    with open(pointfile, 'r') as f:
        # skip first line (units -> s, C, C, C, ... C)
        values = []
        f.next()
        reader = csv.DictReader(f)
        ignition_time = -1 #overwrite if valid, error if not
        for row in reader:
            if is_ignited(row["surf_temp"],ignition_temp): #first time iteration
                ignition_time = float(row["Time"])
                return(ignition_time)
            else:pass
    return(ignition_time)

def make_line(data, experiment_number, fuel_type):
    new_set = []
    for dictionary in data:
        if (dictionary["value"] == -1): continue
        if (dictionary["exp"] == experiment_number and dictionary['type'] == fuel_type):
            new_set.append([dictionary["diameter"],dictionary["value"]])
    new_set = sorted(new_set, key=operator.itemgetter(0))
    xs = [i[0] for i in new_set]
    ys = [i[1] for i in new_set]
    return(xs,ys)


def gather_data(path):
    data = []
    dir_contents = os.listdir(path)
    for item in dir_contents:
        if "base" in item:
            continue
        elif "ignore" in item:
            continue
        elif "thin" in item:
            key0 = "thin"
        elif "thick" in item:
            key0 = "thick"
        else:continue
        #need to explore individual experiments now
        path2 = path + item
        dir_contents2 = sorted(os.listdir(path2))
        for entry in dir_contents2:
            key1 = entry[:-2] #diameter
            key1 = string.replace(key1, 'p', '.')
            path3 = path2 + '/' + entry
            dir_contents3 = os.listdir(path3)
            for elt in dir_contents3:
                if not os.path.isdir(path3 + '/' + elt):
                    continue
                if "gpyro" in elt:
                    continue
                key2 = elt[-1] #5,6, or 7th figure
                value = get_ignition_time(path3 + '/' + elt + '/' + 'albini_model_devc.csv')
                data.append({'diameter':float(key1), 'type':str(key0), 'exp':int(key2), 'value':float(value)})
    return(data)

def is_ignited(temperature, ignition_temp=350):
    return(float(temperature) > ignition_temp)

dirname1 = sys.argv[1]
data_dir = os.getcwd() + "/" + dirname1
data = gather_data(data_dir)
xs,ys = make_line(data, 5, "thick")

#plt.rc('text', usetex=True)
fig = plt.figure(figsize = (24,8))

ax1 = fig.add_subplot(131)
thick_xs, thick_ys = make_line(data, 5, "thick")
thin_xs, thin_ys = make_line(data, 5, "thin")
ax1.plot(thick_xs, thick_ys, color='black', marker='.', label='Thick')
#ax1.scatter(alb_data_5[0], alb_data_5[1],color='red')
ax1.plot(thin_xs, thin_ys, color='blue', marker='.', label='Thin')
ax1.set_title(str("21% Moisture"))
ax1.set_xlabel("Particle Diameter")
ax1.set_ylabel("Time to ignition")

ax2 = fig.add_subplot(132)
thick_xs, thick_ys = make_line(data, 6, "thick")
thin_xs, thin_ys = make_line(data, 6, "thin")
ax2.plot(thick_xs, thick_ys, color='black', marker='.', label='Thick')
#ax2.scatter(alb_data_6[0], alb_data_6[1],color='red')
ax2.plot(thin_xs, thin_ys, color='blue', marker='.', label='Thin')
ax2.set_title(str("4% Moisture"))
ax2.set_xlabel("Particle Diameter")
ax2.set_ylabel("Time to ignition")

ax3 = fig.add_subplot(133)
thick_xs, thick_ys = make_line(data, 7, "thick")
thin_xs, thin_ys = make_line(data, 7, "thin")
ax3.plot(thick_xs, thick_ys, color='black', marker='.', label='Thick')
#ax3.scatter(alb_data_7[0], alb_data_7[1],color='red')
ax3.plot(thin_xs, thin_ys, color='blue', marker='.', label='Thin')
ax3.set_title(str("60% Moisture"))
ax3.set_xlabel("Particle Diameter")
ax3.set_ylabel("Time to ignition")
ax3.legend(loc=2)

plt.subplots_adjust(left=0.1, wspace=0.1, top=0.8)
plt.show(block=True)


