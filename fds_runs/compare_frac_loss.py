import os
import csv
import time
import itertools
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate

def fractional_loss(current, initial):
    return((initial - current)/initial)

def albini_fig_5_fn():
    xs = [0,60,120,180,240]
    ys = [0,0.07,0.13,0.20,0.265]
    points = [xs, ys]
    return(points)

def albini_fig_6_fn():
    xs = [0,100,200,300,400,500]
    ys = [0,0.10,0.15,0.23,0.29,0.35]
    points = [xs, ys]
    return(points)

def albini_fig_7_fn():
    xs = [0,120,240,360,480]
    ys = [0,0.03,0.055,0.085,0.105]
    points = [xs, ys]
    return(points)

exp1 = str(os.getcwd() + "/" +"albini_material_mass_conversion_series/runs_4/1111/albini_model_devc.csv")
exp2 = str(os.getcwd() + "/" +"albini_material_mass_conversion_series/runs_4b/1111/albini_model_devc.csv")
try:
    alb_data = albini_fig_7_fn()
    ylims = [0,0.2]
    xlims = [0,600]
    xtics = [0,120,240,360,480,600]
    values = []
    with open(exp1, 'r') as f:
        # skip first line (units -> s, C, C, C, ... C)
        f.next()
        reader = csv.DictReader(f)
        for row in reader:
            values.append(row)
        times1 = []
        masses1 = []
        initial_mass = 0.0 #will write over this in loop
        for timestep in values:
            if len(times1) == 0: #first time iteration
                initial_mass = float(timestep["mass"])
            t = float(timestep["Time"])
            times1.append(t)
            mass = fractional_loss(float(timestep["mass"]) , initial_mass)
            masses1.append(mass)
    values = []
    with open(exp2, 'r') as f:
        # skip first line (units -> s, C, C, C, ... C)
        f.next()
        reader = csv.DictReader(f)
        for row in reader:
            values.append(row)
        times2 = []
        masses2 = []
        initial_mass = 0.0 #will write over this in loop
        for timestep in values:
            if len(times2) == 0: #first time iteration
                initial_mass = float(timestep["mass"])
            t = float(timestep["Time"])
            times2.append(t)
            mass = fractional_loss(float(timestep["mass"]) , initial_mass)
            masses2.append(mass)
    fig = plt.figure(figsize = (12,12))
    print(masses2[-1],masses1[-1])

    ax = fig.add_subplot(111)
    ax.clear()
    #ax.plot(times1, masses1, color='green', marker='.', label='Simulation (prop)', linestyle="None")
    ax.plot(times1, masses1, color='green', label='Simulation (prop)')
    #ax.plot(times2, masses2, color='blue', marker='.', label='Simulation (no prop)', linestyle="None")
    ax.plot(times2, masses2, color='blue', label='Simulation (no prop)')
    xnew = np.linspace(alb_data[0][0], alb_data[0][-1], 10)
    yfn = scipy.interpolate.interp1d(alb_data[0], alb_data[1], kind='slinear')
    ynew = yfn(xnew)
    ax.plot(alb_data[0], alb_data[1], color='red', marker='.', linestyle="None" )
    ax.plot(xnew, ynew, color='red', label='Albini data')
    ax.set_title("Fractional weight loss of fuel element")
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Fractional weight loss")
    ax.set_ylim(ylims)
    ax.set_xticks(xtics)
    ax.legend()
    fig.canvas.draw()
    #fig.show()
    plt.savefig('figs/figs_mass_conversion_series/compare_albini_mass_pltfig.png')
except:
    print("No usable data found in files")
