import os
import sys
import csv
import time
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate

def get_exp_data(pointfile):
    with open(pointfile, 'r') as f:
        values = []
        # skip first line (units -> s, C, C, C, ... C)
        f.next()
        reader = csv.DictReader(f)
        for row in reader:
            values.append(row)
        times = []
        masses = []
        initial_mass = 0.0 #will write over this in loop
        for timestep in values:
            if len(times) == 0: #first time iteration
                initial_mass = float(timestep["mass"])
            t = float(timestep["Time"])
            times.append(t)
            mass = fractional_loss(float(timestep["mass"]) , initial_mass)
            masses.append(mass)
    return([times,masses])

def get_ignition_point(pointfile, ignition_temp=350):
    '''get the ignition point based on some surface temperature (350C to start)'''

def fractional_loss(current, initial):
    return((initial - current)/initial)

def albini_fig_5_fn():
    xs = [0,60,120,180,240]
    ys = [0,0.07,0.13,0.20,0.265]
    points = [xs, ys]
    return(points)

def albini_fig_6_fn():
    xs = [0,100,200,300,400,500]
    ys = [0,0.10,0.15,0.23,0.29,0.35]
    points = [xs, ys]
    return(points)

def albini_fig_7_fn():
    xs = [0,120,240,360,480]
    ys = [0,0.03,0.055,0.085,0.105]
    points = [xs, ys]
    return(points)

if not (sys.argv[1] and sys.argv[2]):
    raise(Exception("Syntax: python fractional_weight_loss_3pane_compare.py <thick directory> <thin directory>"))
dirname1 = sys.argv[1]
dirname2 = sys.argv[2]
pointfile_dir1 = os.getcwd() + "/" + dirname1
pointfile_dir2 = os.getcwd() + "/" + dirname2
alb_data_5 = albini_fig_5_fn()
alb_data_6 = albini_fig_6_fn()
alb_data_7 = albini_fig_7_fn()

#find our experiments so we can open them up
dir_contents = os.listdir(pointfile_dir1)
fname = "albini_model_devc.csv"
for item in dir_contents:
    if "_5" in item:
        exp_5_1 = get_exp_data(dirname1 + '/' + item + '/' + fname)
    elif "_6" in item:
        exp_6_1 = get_exp_data(dirname1 + '/' + item + '/' + fname)
    elif "_7" in item:
        exp_7_1 = get_exp_data(dirname1 + '/' + item + '/' + fname)
    else:
        pass

#find our experiments so we can open them up
dir_contents = os.listdir(pointfile_dir2)
fname = "albini_model_devc.csv"
for item in dir_contents:
    if "_5" in item:
        exp_5_2 = get_exp_data(dirname2 + '/' + item + '/' + fname)
    elif "_6" in item:
        exp_6_2 = get_exp_data(dirname2 + '/' + item + '/' + fname)
    elif "_7" in item:
        exp_7_2 = get_exp_data(dirname2 + '/' + item + '/' + fname)
    else:
        pass

#plt.rc('text', usetex=True)
fig = plt.figure(figsize = (24,8))

ylims = [0,0.4]
xlims = [0,300]
xtics = [0,60,120,180,240,300]
ax1 = fig.add_subplot(131)
ax1.plot(exp_5_1[0], exp_5_1[1], color='black', marker='.', label='Simulation (thick)')
ax1.plot(exp_5_2[0], exp_5_2[1], color='blue', marker='.', label='Simulation (thin)')
ax1.scatter(alb_data_5[0], alb_data_5[1],color='red')
ax1.plot(alb_data_5[0], alb_data_5[1], color='red', marker='.')
ax1.set_title("4.8cm, 21% moisture")
ax1.set_xlabel("Time (s)")
ax1.set_ylabel("Fractional weight loss")
ax1.set_ylim(ylims)
ax1.set_xticks(xtics)

ylims = [0,0.4]
xlims = [0,500]
xtics = [0,100,200,300,400,500]
ax2 = fig.add_subplot(132)
ax2.plot(exp_6_1[0], exp_6_1[1], color='black', marker='.', label='Simulation (thick)')
ax2.plot(exp_6_2[0], exp_6_2[1], color='blue', marker='.', label='Simulation (thin)')
ax2.scatter(alb_data_6[0], alb_data_6[1],color='red')
ax2.plot(alb_data_6[0], alb_data_6[1], color='red', marker='.')
ax2.set_title("10.7cm, 4% moisture")
ax2.set_xlabel("Time (s)")
ax2.set_ylim(ylims)
ax2.set_xticks(xtics)

ylims = [0,0.2]
xlims = [0,600]
xtics = [0,120,240,360,480,600]
ax3 = fig.add_subplot(133)
ax3.plot(exp_7_1[0], exp_7_1[1], color='black', marker='.', label='Simulation (thick)')
ax3.plot(exp_7_2[0], exp_7_2[1], color='blue', marker='.', label='Simulation (thin)')
ax3.scatter(alb_data_7[0], alb_data_7[1],color='red', label='Experiment')
ax3.plot(alb_data_7[0], alb_data_7[1], color='red', marker='.')
ax3.set_title("10.7cm, 60% moisture")
ax3.set_xlabel("Time (s)")
ax3.legend()
ax3.set_ylim(ylims)
ax3.set_xticks(xtics)

plt.subplots_adjust(left=0.1, wspace=0.1, top=0.8)
plt.show(block=True)
