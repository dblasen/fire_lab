import os
import sys
import csv
import time
import math
import matplotlib.pyplot as plt
from matplotlib import gridspec
import numpy as np
import scipy.interpolate
import pandas as pd

from operator import add

def density_remove(point, density=1):
    return(float(point) / float(density))

def is_ignited(temperature, ignition_temp=350):
    return(float(temperature) > ignition_temp)

def fractional_loss(current, initial):
    return((initial - current)/initial)

def albini_fig_5_fn():
    xs = [0,60,120,180,240]
    ys = [0,0.07,0.13,0.20,0.265]
    points = [xs, ys]
    return(points)

def albini_fig_6_fn():
    xs = [0,100,200,300,400,500]
    ys = [0,0.10,0.15,0.23,0.29,0.35]
    points = [xs, ys]
    return(points)

def albini_fig_7_fn():
    xs = [0,120,240,360,480]
    ys = [0,0.03,0.055,0.085,0.105]
    points = [xs, ys]
    return(points)

def adjust_exp_numbers(exp, sim_time, sim_mass):
    '''move experimental data to our point of ignition'''
    new_x = []
    new_y = []
    for x in exp[0]:
        new_x.append(float(x) + float(sim_time))
    for y in exp[1]:
        new_y.append(float(y) + float(sim_mass))
    return([new_x,new_y])

def get_sim_ignition_data(times, masses, surf_temps, ignition_temp=350.0):
    '''get the time and mass of ignition'''
    ignition_time = 0
    ignition_mass = 0
    for i in range(len(times)):
        if is_ignited(surf_temps[i],ignition_temp):
            ignition_time = times[i]
            ignition_mass = masses[i]
            break
    return(ignition_time, ignition_mass)

def get_times_masses_surf_temps(points):
    '''inbound list of dicts with loose strings; get strings'''
    times = []
    masses = []
    surf_temps = []
    for dictionary in points:
        times.append(float(dictionary["t"]))
        masses.append(float(dictionary["mass"]))
        surf_temps.append(float(dictionary["surf_temp"]))
    return(times,masses,surf_temps)

#wet_wood_density_DV0001
def get_points(pointfile, tags):
    with open(pointfile, 'r') as f:
        # skip first line (units -> s, C, C, C, ... C)
        f.next()
        reader = csv.DictReader(f)
        values = []
        for row in reader:
            values.append(row)
        time_points = []
        initial_mass = 0.0
        for timestep in values:
            if len(time_points) == 0: #first time iteration
                initial_mass = float(timestep["mass"])
            time_point = {}
            mass = fractional_loss(float(timestep["mass"]) , initial_mass)
            time_point["t"] = float(timestep["Time"])
            time_point["mass"] = float(mass)
            time_point["surf_temp"] = float(timestep["surf_temp"])
            time_point["air_temp"] = float(timestep["air_temp"])
            for key,value in timestep.iteritems():
                for tag in tags:
                    if tag in key:
                        x = int(key[-4:]) #last 4 chars -> depth in mm
                        if x in time_point:
                            inner = time_point[x]
                        else:
                            time_point[x] = {}
                            inner = time_point[x]
                        inner[tag] = float(value)
            if time_point: #if dict has data...
                time_points.append(time_point)
    return(time_points)

def round_to_50(num):
    return(int(math.ceil(num / 50.0))*50)

def stacked_line_plot(points, exp, exp_number):
    if(exp_number) == 5:
        ylims = [0,0.4]
        xlims = [0,300]
        xtics = [0,60,120,180,240,300]
        title = "4.8cm, 21 percent moisture"
    elif(exp_number) == 6:
        ylims = [0,0.4]
        xlims = [0,500]
        xtics = [0,100,200,300,400,500]
        title = "10.7cm, 4 percent moisture"
    elif(exp_number) == 7:
        ylims = [0,0.2]
        xlims = [0,600]
        xtics = [0,120,240,360,480,600]
        title = "10.7cm, 60 percent moisture"
    times, masses, surf_temps = get_times_masses_surf_temps(points)
    sim = [times,masses] #laziness for below
    initial_mass = masses[0]
    ignition_time, ignition_mass = get_sim_ignition_data(times, masses, surf_temps)
    xlims = [xlims[0], round_to_50(xlims[1]+ignition_time)]
    adjusted_exp = adjust_exp_numbers(exp, ignition_time, ignition_mass)
    exp = adjusted_exp
    xs  = []
    Ms = []
    DVs = []
    CHs = []
    ASs = []
    TEs = []
    fig = plt.figure(figsize = (24,12))
    gs = gridspec.GridSpec(1,2, width_ratios=[1,1])
    first_iteration = True
    total_rho = 0 #placeholder; will crash below if not calculated in first iteration
    air_temp = 0
    plt.rc('text', usetex=True)
    i = 0
    for time_point in points:
        t = time_point.pop("t")
        if(float(t) > float(xlims[1])): #end of plot so exit now
            return(0)
        for key in sorted(time_point):
            if(key == "air_temp"):
                air_temp = time_point[key]
                continue
            if(key == "mass" or key == "t" or key == "surf_temp"): #breaks code below...
                continue
            val = time_point[key]
            if(first_iteration):
                total_rho = val["M"] + val["DV"]
                first_iteration = False
            xs.append(key)
            TEs.append(val["TE"])
            Ms.append(density_remove(val["M"],total_rho))
            DVs.append(density_remove(val["DV"],total_rho))
            CHs.append(density_remove(val["CH"],total_rho))
            ASs.append(density_remove(val["AS"],total_rho))
        #add these in place to make stacked values
        Ms = Ms
        DVs = map(add, Ms, DVs)
        CHs = map(add, DVs, CHs)
        ASs = map(add, CHs, ASs)
        #first plot: mass fraction as fn of distance
        ax = plt.subplot(gs[0])
        ax.clear()
        l1 = ax.plot(xs, Ms, color='blue', marker='.', label='Moisture')
        l2 = ax.plot(xs, DVs, color='brown', marker='.', label='Dry Vegetation')
        l3 = ax.plot(xs, CHs, color='black', marker='.', label='Char')
        l4 = ax.plot(xs, ASs, color='gray', marker='.', label='Ash')
        ax.fill_between(xs, 0, Ms, facecolor = "blue", alpha = 0.6)
        ax.fill_between(xs, Ms, DVs, facecolor = "brown", alpha = 0.6)
        ax.fill_between(xs, DVs, CHs, facecolor = "black", alpha = 0.6)
        ax.fill_between(xs, CHs, ASs, facecolor = "gray", alpha = 0.6)
        ax.set_xlim([xs[0],xs[-1]])
        ax.set_title("FDS simulation material density at time=%.1f" % t)
        ax.set_xlabel("Distance into medium (mm)")
        ax.set_ylabel("Mass Fraction")
        #temperature on same plot
        ax_t = ax.twinx()
        l5 = ax_t.plot(xs, TEs, color = 'red', label='Temperature')
        #l6 = ax_t.axhline(y=air_temp, color="orange", linewidth=2, label="Air Temperature")
        air_temps = [air_temp for position in range(len(xs))]
        l6 = ax_t.plot(xs, air_temps, color="orange", linewidth=2, label="Air Temp")
        ax_t.set_ylabel('Degrees Celsius', color = 'red')
        ax_t.set_ylim([0,1000])
        ax_t.set_xlim([xs[0],xs[-1]])
        lns = l1 + l2 + l3 + l4 + l5 + l6
        labels = [l.get_label() for l in lns]
        ax.legend(lns, labels, loc=1)
        # second plot: mass loss as fn of time
        ax2 = plt.subplot(gs[1])
        ax2.clear()
        ax2.plot(exp[0], exp[1], color="red", marker='.', markersize=1.5, linestyle="-", label="Experiment")
        ax2.plot(sim[0], sim[1], color="blue", linestyle="-", label="Simulation")
        ax2.plot(sim[0][i], sim[1][i], color="black",marker="^", markersize=5.0)
        ax2.axvline(x=sim[0][i], color="black")
        ax2.axvline(x=ignition_time, color="green", label="Ignition")
        ax2.set_title(title)
        ax2.set_ylim(ylims)
        ax2.set_xlim(xlims)
        ax2.set_xticks(xtics)
        ax2.set_ylabel('Mass fraction loss')
        ax2.set_xlabel("Time (s)")
        #ax2.set_ylim([0, 1])
        ax2.grid(linestyle='--', linewidth=0.5)
        ax2.legend(loc = 4)
        fig.canvas.draw()
        #fig.show()
        plt.savefig('figs/mass_loss_comparison/pyrolysis_%s.png' % str('%05d' % i))
        xs  = []
        Ms = []
        DVs = []
        CHs = []
        ASs = []
        TEs = []
        i += 1
    return(0)

def get_experiment_number(directory):
    if "_5" in directory:
        return(5)
    elif "_6" in directory:
        return(6)
    elif "_7" in directory:
        return(7)
    else:
        raise(Exception("Directory must end in '_5', '_6', or '_7'!"))
    return(-1)


alb_data_5 = albini_fig_5_fn()
alb_data_6 = albini_fig_6_fn()
alb_data_7 = albini_fig_7_fn()

dirname = sys.argv[1]
pointfile_dir = os.getcwd() + "/" + dirname
exp_number = get_experiment_number(pointfile_dir)
if(exp_number == 5):
    exp = albini_fig_5_fn()
elif(exp_number == 6):
    exp = albini_fig_6_fn()
elif(exp_number == 7):
    exp = albini_fig_7_fn()
else:
    raise("invalid albini plot number; what happened?")

pointfile = pointfile_dir + "/albini_model_devc.csv"
tags = ["M","DV", "CH", "AS", "TE", "RHO"]
points = get_points(pointfile, tags)

stacked_line_plot(points,exp, exp_number)

### last file begins
