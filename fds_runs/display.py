import os
import csv
import time
import matplotlib.pyplot as plt

def make_xy(points, tag):
    """unpack points into an x,y paired list
    Parameters:
        points: a large dictionary of stuff from wfds
        tag: a 2 char label describing the location of the observation
    Returns:
        A zipped list of 
    """
    keys = points.keys() #list
    vals = points.values() #list
    xs = []
    ys = []
    for k, v in points.iteritems():
        #first 2 chars describe which set it's in (tag)
        if(k[0:2] != tag):
            continue #not our tag, skip it
        if k[2] == 'p':
            sign = float(1.0)
        elif k[2] == 'n':
            sign = float(-1.0)
        lead = "" #numbers leading the decimal
        dec = ""
        in_dec = False
        for c in k[3:]: #have to unpack the rest...
            if c == 'p' and not in_dec: #p = (point/period)
                in_dec = True #rest of the string is decimal
            elif in_dec:
                dec = dec + c
            else:
                lead = lead + c #append this char to leading number
        num = str(lead + "." + dec) #
        num = float(num) * sign
        xs.append(num)
        ys.append(float(v))
    return(zip(xs,ys))

def simple_xy(points):
    """unpack points into an x,y paired list
    Parameters:
        points: a large dictionary of stuff from wfds
        tag: a 2 char label describing the location of the observation
    Returns:
        A zipped list of 
    """
    xs = []
    ys = []
    for k, v in points.iteritems():
        if(k == "Time"): continue
        try:
            num = float(k) #
        except:
            continue
        xs.append(num)
        ys.append(float(v))
    return(zip(xs,ys))

#pointfile = os.getcwd() + "/" + "fed_2/onetree_surf_1mesh_devc.csv"
#pointfile = os.getcwd() + "/" + "fed_7/onetree_surf_1mesh_devc.csv"
#pointfile = os.getcwd() + "/" + "fed_9/onetree_surf_1mesh_devc.csv"
#pointfile = os.getcwd() + "/" + "thick_wall2/thick_wall_devc.csv"

#pointfile = os.getcwd() + "/" + "albini_burn_1D_plot/albini_model_devc.csv"
#pointfile = os.getcwd() + "/" + "albini_burn_1D_plot_mirrored/albini_model_devc.csv"
#pointfile = os.getcwd() + "/" + "albini_batch_runs_2/albini_35mm/albini_model_devc.csv"
pointfile = os.getcwd() + "/" + "albini_batch_runs_5/albini_100mm/albini_model_devc.csv"

values = []

with open(pointfile, 'r') as f:
    # skip first line (units -> s, C, C, C, ... C)
    f.next()
    reader = csv.DictReader(f)
    for row in reader:
        values.append(row)
    tag = "MH"
    frames = []
    for timestep in values:
        frame = {}
        frame["t"] = float(timestep["Time"])
        frame["temps"] = simple_xy(timestep)
        #frame["temps"] = make_xy(timestep,tag)
        frame["temps"] = sorted(frame["temps"], key = lambda x: x[0]) #sort into ascending order
        #print(frame)
        frames.append(frame)

    delay = 0.00
    fig = plt.figure(figsize = (12,12))
    ax = fig.add_subplot(111)
    for i in range(0,len(frames)):
        ax.clear()
        ax.plot(*zip(*frames[i]["temps"]), color="red", marker = '.', label="Object Temperature")
        ax.axhline(y=327, color='blue', label="Ignition Point (327C)")
        #ax.plot(xs, values2[i], color="blue", label = name2)
        ax.set_title("Point temperatures along Z axis in FDS Sim time = %s" % str(frames[i]["t"]))
        ax.set_xlabel("Point on Z-axis")
        ax.set_ylabel("Degrees C")
        ax.set_ylim([0, 500])
        ax.legend()
        fig.canvas.draw()
        #fig.show()
        plt.savefig('figs/albini_pltfig_%03d.png' % (i))
        time.sleep(delay)
