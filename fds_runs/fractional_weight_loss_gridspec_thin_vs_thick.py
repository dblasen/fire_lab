import os
import sys
import csv
import time
import matplotlib.pyplot as plt
from matplotlib import gridspec
import numpy as np
import scipy.interpolate
import pandas as pd

from operator import add

import read_prof

def density_remove(point, density=1):
    return(float(point / density))

def is_ignited(temperature, ignition_temp=350):
    return(float(temperature) > ignition_temp)

def fractional_loss(current, initial):
    return((initial - current)/initial)

def albini_fig_5_fn():
    xs = [0,60,120,180,240]
    ys = [0,0.07,0.13,0.20,0.265]
    points = [xs, ys]
    return(points)

def albini_fig_6_fn():
    xs = [0,100,200,300,400,500]
    ys = [0,0.10,0.15,0.23,0.29,0.35]
    points = [xs, ys]
    return(points)

def albini_fig_7_fn():
    xs = [0,120,240,360,480]
    ys = [0,0.03,0.055,0.085,0.105]
    points = [xs, ys]
    return(points)

def get_sim_ignition_data(times, masses, surf_temps, ignition_temp=350.0):
    '''get the time and mass of ignition'''
    ignition_time = 0
    ignition_mass = 0
    for i in range(len(times)):
        if is_ignited(surf_temps[i],ignition_temp):
            ignition_time = times[i]
            ignition_mass = masses[i]
            break
    return(ignition_time, ignition_mass)

def get_times_masses_surf_temps(points):
    '''inbound list of dicts with loose strings; get strings'''
    times = []
    masses = []
    surf_temps = []
    for dictionary in points:
        times.append(float(dictionary["t"]))
        masses.append(float(dictionary["mass"]))
        surf_temps.append(float(dictionary["surf_temp"]))
    return(times,masses,surf_temps)

#wet_wood_density_DV0001
def get_points(pointfile, tags):
    with open(pointfile, 'r') as f:
        # skip first line (units -> s, C, C, C, ... C)
        f.next()
        reader = csv.DictReader(f)
        values = []
        for row in reader:
            values.append(row)
        time_points = []
        initial_mass = 0.0
        for timestep in values:
            if len(time_points) == 0: #first time iteration
                initial_mass = float(timestep["mass"])
            time_point = {}
            mass = fractional_loss(float(timestep["mass"]) , initial_mass)
            time_point["t"] = float(timestep["Time"])
            time_point["mass"] = float(mass)
            time_point["surf_temp"] = float(timestep["surf_temp"])
            for key,value in timestep.iteritems():
                for tag in tags:
                    if tag in key:
                        x = int(key[-4:]) #last 4 chars -> depth in mm
                        if x in time_point:
                            inner = time_point[x]
                        else:
                            time_point[x] = {}
                            inner = time_point[x]
                        inner[tag] = float(value)
            if time_point: #if dict has data...
                time_points.append(time_point)
    return(time_points)

def make_new_profile_xlims(xdims):
    '''some tuple like (0,0.1) that describes distance to center,
    but we want (-0.1, 0.1) with some wiggle room (10%)?
    '''
    return(-1 * abs(xdims[0] - xdims[1])*1.1, abs(xdims[0] - xdims[1])*1.1)

def make_profile_shape_pos(vector):
    '''same as below but with the x positions'''
    # [0, 0.1, 0.2] IN DEPTH which makes this annoying
    # to
    # [-0.2, -0.1, 0, 0, 0.1, 0.2]
    vec_max = abs(vector[-1] - vector[0])
    first = []
    for elt in vector[::-1]:
        first.append(-1 * abs(elt))
    nu_vector = first + vector
    return(nu_vector)

def make_profile_shape_vals(vector):
    '''data will be shaped for surface to half-depth
    but we want surface to opposite surface (mirror in middle)
    so we reshape it'''
    # [500,600,600]
    # to
    # [500, 600, 600, 600, 600, 500]
    rev_vector = vector[::-1]
    nu_vector = vector + rev_vector
    return(nu_vector)

#dual_stacked_line_plot(points_thick, dims1_thick, prof_file_2_thick, prof_file_3_thick, prof_file_4_thick, prof_file_5_thick, prof_file_1_thick, title_thick,\
#        points_thin, dims1_thick, prof_file_2_thick, prof_file_3_thick, prof_file_4_thick, prof_file_5_thick, prof_file_1_thick, title_thick)
def dual_stacked_line_plot(points_thick, xdims_thick, dry_thick, char_thick, ash_thick, moisture_thick, temp_thick,\
        points_thin, xdims_thin, dry_thin, char_thin, ash_thin, moisture_thin, temp_thin, title):
    p1_xdim1, p1_xdim2 = make_new_profile_xlims(xdims_thick)
    p2_xlims = [0,300] #NOTE not sure if this is a good choice
    p2_ylims = [0,1.0]
    p2_xtics = [i for i in xrange(0,300,30)]
    times_thick, masses_thick, surf_temps_thick = get_times_masses_surf_temps(points_thick)
    sim_thick = [times_thick,masses_thick] #laziness for below
    initial_mass_thick = masses_thick[0]
    ignition_time_thick, ignition_mass_thick = get_sim_ignition_data(times_thick, masses_thick, surf_temps_thick)

    times_thin, masses_thin, surf_temps_thin = get_times_masses_surf_temps(points_thin)
    sim_thin = [times_thin,masses_thin] #laziness for below
    initial_mass_thin = masses_thin[0]
    ignition_time_thin, ignition_mass_thin = get_sim_ignition_data(times_thin, masses_thin, surf_temps_thin)

    fig = plt.figure(figsize = (24,8))
    gs = gridspec.GridSpec(1,3, width_ratios=[1,1,1])
    plt.rc('text', usetex=True)
    i = 0
    for time_point in temp_thick: #index on this to be lazy
        xs_thick  = make_profile_shape_pos(dry_thick[i]['x'])
        Ms_thick = make_profile_shape_vals(moisture_thick[i]['y'])
        DVs_thick = make_profile_shape_vals(dry_thick[i]['y'])
        CHs_thick = make_profile_shape_vals(char_thick[i]['y'])
        ASs_thick = make_profile_shape_vals(ash_thick[i]['y'])
        temps_thick = make_profile_shape_vals(temp_thick[i]['y'])
        t_thick = temp_thick[i]['t']
        #add these in place to make stacked values
        Ms_thick = Ms_thick
        DVs_thick = map(add, Ms_thick, DVs_thick)
        CHs_thick = map(add, DVs_thick, CHs_thick)
        ASs_thick = map(add, CHs_thick, ASs_thick)
        #first plot: mass fraction as fn of distance
        ax = plt.subplot(gs[0])
        ax.clear()
        ax.plot(xs_thick, Ms_thick, color='blue', marker='.', label='Moisture')
        ax.plot(xs_thick, DVs_thick, color='brown', marker='.', label='Dry Vegetation')
        ax.plot(xs_thick, CHs_thick, color='black', marker='.', label='Char')
        ax.plot(xs_thick, ASs_thick, color='gray', marker='.', label='Ash')
        ax.fill_between(xs_thick, 0, Ms_thick, facecolor = "blue", alpha = 0.6)
        ax.fill_between(xs_thick, Ms_thick, DVs_thick, facecolor = "brown", alpha = 0.6)
        ax.fill_between(xs_thick, DVs_thick, CHs_thick, facecolor = "black", alpha = 0.6)
        ax.fill_between(xs_thick, CHs_thick, ASs_thick, facecolor = "gray", alpha = 0.6)
        ax.set_title("FDS simulation material density at time=%.1f (thermally thick)" % t_thick)
        ax.set_xlabel("X-axis of particle (m)")
        ax.set_ylabel("Kg/m3")
        ax.set_xlim(p1_xdim1,p1_xdim2)
        ax.set_autoscale_on(False)
        ax.set_ylim(0,700)
        ax_t = ax.twinx()
        ax_t.set_autoscale_on(False)
        ax_t.set_xlim(p1_xdim1,p1_xdim2)
        ax_t.plot(xs_thick, temps_thick, color = 'red', label='Temperature')
        ax_t.set_ylabel('Temperature', color = 'red')
        ax_t.set_ylim([0,500])
        # second plot: mass loss as fn of time
        ax2 = plt.subplot(gs[1])
        ax2.clear()
        ax2.plot(sim_thick[0], sim_thick[1], color="blue", linestyle="-", label="Thick")
        ax2.plot(sim_thin[0], sim_thin[1], color="red", linestyle="-", label="Thin")
        ax2.plot(sim_thick[0][i], sim_thick[1][i], color="blue",marker="^", markersize=5.0)
        ax2.plot(sim_thin[0][i], sim_thin[1][i], color="red",marker="^", markersize=5.0)
        ax2.axvline(x=sim_thick[0][i], color="black")
        ax2.axvline(x=ignition_time_thick, color="lightblue", label="Ignition (thick)")
        ax2.axvline(x=ignition_time_thin, color="pink", label="Ignition (thin)")
        ax2.set_title(title)
        ax2.set_ylim(p2_ylims)
        ax2.set_xlim(p2_xlims)
        ax2.set_xticks(p2_xtics)
        ax2.set_ylabel('Mass fraction loss')
        ax2.set_xlabel("Time (s)")
        ax2.set_ylim([0, 1])
        ax2.grid(linestyle='--', linewidth=0.5)
        ax2.legend(loc = 4)
        xs_thin  = make_profile_shape_pos(dry_thin[i]['x'])
        Ms_thin = make_profile_shape_vals(moisture_thin[i]['y'])
        DVs_thin = make_profile_shape_vals(dry_thin[i]['y'])
        CHs_thin = make_profile_shape_vals(char_thin[i]['y'])
        ASs_thin = make_profile_shape_vals(ash_thin[i]['y'])
        temps_thin = make_profile_shape_vals(temp_thin[i]['y'])
        t_thin = temp_thin[i]['t']
        #add these in place to make stacked values
        Ms_thin = Ms_thin
        DVs_thin = map(add, Ms_thin, DVs_thin)
        CHs_thin = map(add, DVs_thin, CHs_thin)
        ASs_thin = map(add, CHs_thin, ASs_thin)
        #third plot: mass fraction as fn of distance (#2)
        ax3 = plt.subplot(gs[2])
        ax3.clear()
        ax3.plot(xs_thin, Ms_thin, color='blue', marker='.', label='Moisture')
        ax3.plot(xs_thin, DVs_thin, color='brown', marker='.', label='Dry Vegetation')
        ax3.plot(xs_thin, CHs_thin, color='black', marker='.', label='Char')
        ax3.plot(xs_thin, ASs_thin, color='gray', marker='.', label='Ash')
        ax3.fill_between(xs_thin, 0, Ms_thin, facecolor = "blue", alpha = 0.6)
        ax3.fill_between(xs_thin, Ms_thin, DVs_thin, facecolor = "brown", alpha = 0.6)
        ax3.fill_between(xs_thin, DVs_thin, CHs_thin, facecolor = "black", alpha = 0.6)
        ax3.fill_between(xs_thin, CHs_thin, ASs_thin, facecolor = "gray", alpha = 0.6)
        ax3.set_title("FDS simulation material density at time=%.1f (thermally thin)" % t_thin)
        ax3.set_xlabel("X-axis of particle (m)")
        ax3.set_ylabel("Kg/m3")
        ax3.set_xlim(p1_xdim1,p1_xdim2)
        ax3.set_autoscale_on(False)
        ax3.legend(loc=4)
        ax3.set_ylim(0,700)
        ax_t2 = ax3.twinx()
        ax_t2.set_autoscale_on(False)
        ax_t2.set_xlim(p1_xdim1,p1_xdim2)
        ax_t2.plot(xs_thin, temps_thin, color = 'red', label='Temperature')
        ax_t2.set_ylabel('Temperature', color = 'red')
        ax_t2.set_ylim([0,500])
        fig.canvas.draw()
        plt.savefig('figs/mass_loss_comparison/pyrolysis_%s.png' % str('%05d' % i))
        print("saving image %s" % i)
        xs_thick  = []
        Ms_thick = []
        DVs_thick = []
        CHs_thick = []
        ASs_thick = []
        xs_thin  = []
        Ms_thin = []
        DVs_thin = []
        CHs_thin = []
        ASs_thin = []
        i += 1
    return(0)

def flatten_thin_temps(thin_list):
    for elt in thin_list:
        elt['y'][0] = elt['y'][1]
    return(thin_list)

def get_experiment_number(directory):
    if "_5" in directory:
        return(5)
    elif "_6" in directory:
        return(6)
    elif "_7" in directory:
        return(7)
    else:
        raise(Exception("Directory must end in '_5', '_6', or '_7'!"))
    return(-1)

try:
    dirname_thick = sys.argv[1]
    dirname_thin = sys.argv[2]
    title = sys.argv[3]
except:
    raise(Exception("Syntax: python fractional_weight_loss_gridspec_thin_vs_thick.py <thick dir> <thin dir> <title>"))
pointfile_dir_thick = os.getcwd() + "/" + dirname_thick
pointfile_dir_thin = os.getcwd() + "/" + dirname_thin
exp_number_thick = get_experiment_number(pointfile_dir_thick)
exp_number_thin = get_experiment_number(pointfile_dir_thin)

prof_file_1_thick, n1_thick, dims1_thick = read_prof.read(pointfile_dir_thick + "/albini_model_prof_01.csv") #temperature?
prof_file_2_thick, n2_thick, dims2_thick = read_prof.read(pointfile_dir_thick + "/albini_model_prof_02.csv") #dryveg?
prof_file_3_thick, n3_thick, dims3_thick = read_prof.read(pointfile_dir_thick + "/albini_model_prof_03.csv") #char?
prof_file_4_thick, n4_thick, dims4_thick = read_prof.read(pointfile_dir_thick + "/albini_model_prof_04.csv") #ash?
prof_file_5_thick, n5_thick, dims5_thick = read_prof.read(pointfile_dir_thick + "/albini_model_prof_05.csv") #moisture?
if(dims1_thick != dims2_thick or dims1_thick != dims3_thick or dims1_thick != dims4_thick or dims1_thick != dims5_thick):
    raise(Exception("Particle sizes do not match; check profile output for dimensions?"))

prof_file_1_thin, n1_thin, dims1_thin = read_prof.read(pointfile_dir_thin + "/albini_model_prof_01.csv") #temperature?
prof_file_1_thin = flatten_thin_temps(prof_file_1_thin)
prof_file_2_thin, n2_thin, dims2_thin = read_prof.read(pointfile_dir_thin + "/albini_model_prof_02.csv") #dryveg?
prof_file_3_thin, n3_thin, dims3_thin = read_prof.read(pointfile_dir_thin + "/albini_model_prof_03.csv") #char?
prof_file_4_thin, n4_thin, dims4_thin = read_prof.read(pointfile_dir_thin + "/albini_model_prof_04.csv") #ash?
prof_file_5_thin, n5_thin, dims5_thin = read_prof.read(pointfile_dir_thin + "/albini_model_prof_05.csv") #moisture?
if(dims1_thin != dims2_thin or dims1_thin != dims3_thin or dims1_thin != dims4_thin or dims1_thin != dims5_thin):
    raise(Exception("Particle sizes do not match; check profile output for dimensions?"))

tags = ["M","DV", "CH", "AS"]
pointfile_thick = pointfile_dir_thick + "/albini_model_devc.csv"
points_thick = get_points(pointfile_thick, tags)
pointfile_thin = pointfile_dir_thin + "/albini_model_devc.csv"
points_thin = get_points(pointfile_thin, tags)

#def stacked_line_plot(points, xdims, dry, char, ash, moisture, temp, title):
#stacked_line_plot(points, dims1, prof_file_2, prof_file_3, prof_file_4, prof_file_5, prof_file_1, title)
dual_stacked_line_plot(points_thick, dims1_thick, prof_file_2_thick, prof_file_3_thick, prof_file_4_thick, prof_file_5_thick, prof_file_1_thick,\
        points_thin, dims1_thin, prof_file_2_thin, prof_file_3_thin, prof_file_4_thin, prof_file_5_thin, prof_file_1_thin, title)

### last file begins
