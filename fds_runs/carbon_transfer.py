import os
import csv
import time
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate

def get_points(pointfile):
    with open(pointfile, 'r') as f:
        # skip first line (units -> s, C, C, C, ... C)
        f.next()
        reader = csv.DictReader(f)
        values = []
        for row in reader:
            values.append(row)
        points = []
        prev_co2 = 0
        prev_co = 0
        prev_h2o = 0
        for timestep in values:
            point = {}
            point["t"] = t = float(timestep["Time"])
            point["co2"] = prev_co2 = float(timestep["CO2_flow"]) + prev_co2
            point["co"] = prev_co = float(timestep["CO_flow"]) + prev_co
            point["h2o"] = prev_h2o = float(timestep["H2O_flow"]) + prev_h2o
            points.append(point)
            if t > 30: break
    return(points)


#pointfile1 = os.getcwd() + "/" + "NIST_doug_fir_runs/new_runs(temp_increase)/validation_files/2m1/tree_2_m_14_pc_devc.csv"
#pointfile2 = os.getcwd() + "/" + "NIST_doug_fir_runs/new_runs(temp_increase)/thin_files/2m1/tree_2_m_14_pc_devc.csv"
pointfile1 = os.getcwd() + "/" + "NIST_doug_fir_runs/new_runs_no_temp_short_duration/validation_files/2m1/tree_2_m_14_pc_devc.csv"
pointfile2 = os.getcwd() + "/" + "NIST_doug_fir_runs/new_runs_no_temp_short_duration/thin_files/2m1/tree_2_m_14_pc_devc.csv"

points1 = get_points(pointfile1)
points2 = get_points(pointfile2)

ts = [i["t"] for i in points1]
co2_1 = [i["co2"] for i in points1]
co2_2 = [i["co2"] for i in points2]
co_1 = [i["co"] for i in points1]
co_2 = [i["co"] for i in points2]
h2o_1 = [i["h2o"] for i in points1]
h2o_2 = [i["h2o"] for i in points2]

fig = plt.figure(figsize = (12,12))
ax = fig.add_subplot(111)
ax.clear()
ax.plot(ts, co2_1, color='black', marker='.', label='Thick fuel CO2')
ax.plot(ts, co2_2, color='red', marker='.', label='Thin fuel CO2')
ax.plot(ts, h2o_1, color='blue', marker='.', label='Thick fuel H2O')
ax.plot(ts, h2o_2, color='green', marker='.', label='Thin fuel H2O')
#ax.plot(alb_data[0], alb_data[1], color='red', marker='.', linestyle="None" )
#ax.plot(xnew, ynew, color='red', label='Thin fuel')
ax.set_title("NIST-based Douglas Fir gas release")
ax.set_xlabel("Time (s)")
ax.set_ylabel("kg/s")
ax.legend()
fig.canvas.draw()
#fig.show()
plt.savefig('figs/NIST_gas_release.png')
