import os
import csv
import time
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate

from operator import add

def normal_line_plot(points):
    xs  = []
    VVs = []
    DVs = []
    CHs = []
    ASs = []
    fig = plt.figure(figsize = (12,12))
    ax = fig.add_subplot(111)
    for time_point in points:
        t = time_point.pop("t")
        for key in sorted(time_point):
            val = time_point[key]
            xs.append(key)
            VVs.append(val["VV"])
            DVs.append(val["DV"])
            CHs.append(val["CH"])
            ASs.append(val["AS"])
        ax.clear()
        ax.plot(xs, VVs, color='green', marker='.', label='Virgin Vegetation')
        ax.plot(xs, DVs, color='brown', marker='.', label='Dry Vegetation')
        ax.plot(xs, CHs, color='black', marker='.', label='Char')
        ax.plot(xs, ASs, color='gray', marker='.', label='Ash')
        ax.set_title("Albini experiment density of materials at time=%s" % str(t))
        ax.set_xlabel("Distance into medium (mm)")
        ax.set_ylabel("Kg/m3")
        ax.legend()
        fig.canvas.draw()
        fig.show()
        #plt.savefig('figs/pyrolysis_%s.png' % t)
        xs  = []
        VVs = []
        DVs = []
        CHs = []
        ASs = []
    return(0)

def density_remove(point, density=1):
    return(float(point / density))


def stacked_line_plot(points):
    xs  = []
    VVs = []
    DVs = []
    CHs = []
    ASs = []
    fig = plt.figure(figsize = (12,12))
    ax = fig.add_subplot(111)
    i = 0
    for time_point in points:
        t = time_point.pop("t")
        for key in sorted(time_point):
            val = time_point[key]
            xs.append(key)
            VVs.append(val["VV"])
            DVs.append(val["DV"])
            CHs.append(val["CH"])
            ASs.append(val["AS"])
            #VVs.append(density_remove(val["VV"],586.0))
            #DVs.append(density_remove(val["DV"],514.0))
            #CHs.append(density_remove(val["CH"],134.0))
            #ASs.append(density_remove(val["AS"],067.0))
        #add these in place to make stacked values
        VVs = VVs
        DVs = map(add, VVs, DVs)
        CHs = map(add, DVs, CHs)
        ASs = map(add, CHs, ASs)
        ax.clear()
        ax.plot(xs, VVs, color='green', marker='.', label='Virgin Vegetation')
        ax.plot(xs, DVs, color='brown', marker='.', label='Dry Vegetation')
        ax.plot(xs, CHs, color='black', marker='.', label='Char')
        ax.plot(xs, ASs, color='gray', marker='.', label='Ash')
        ax.fill_between(xs, 0, VVs, facecolor = "green", alpha = 0.6)
        ax.fill_between(xs, VVs, DVs, facecolor = "brown", alpha = 0.6)
        ax.fill_between(xs, DVs, CHs, facecolor = "black", alpha = 0.6)
        ax.fill_between(xs, CHs, ASs, facecolor = "gray", alpha = 0.6)
        ax.set_title("Albini experiment density of materials at time=%s" % str(t))
        ax.set_xlabel("Distance into medium (mm)")
        ax.set_ylabel("Kg/m3")
        ax.legend(loc=4)
        fig.canvas.draw()
        #fig.show()
        plt.savefig('figs/pyrolysis_%s.png' % str('%05d' % i))
        xs  = []
        VVs = []
        DVs = []
        CHs = []
        ASs = []
        i += 1
    return(0)

#wet_wood_density_DV0001
def get_points(pointfile, tags):
    with open(pointfile, 'r') as f:
        # skip first line (units -> s, C, C, C, ... C)
        f.next()
        reader = csv.DictReader(f)
        values = []
        for row in reader:
            values.append(row)
        time_points = []
        for timestep in values:
            time_point = {}
            time_point["t"] = float(timestep["Time"])
            for key,value in timestep.iteritems():
                for tag in tags:
                    if tag in key:
                        x = int(key[-4:]) #last 4 chars -> depth in mm
                        if x in time_point:
                            inner = time_point[x]
                        else:
                            time_point[x] = {}
                            inner = time_point[x]
                        inner[tag] = float(value)
            if time_point: #if dict has data...
                time_points.append(time_point)
    return(time_points)


pointfile = os.getcwd() + "/albini_mass_conversion_test/albini_multi_layer/albini_model_devc.csv"
tags = ["VV", "DV", "CH", "AS"]

points = get_points(pointfile, tags)

stacked_line_plot(points)

