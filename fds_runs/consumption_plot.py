import os
import sys
import csv
import time
import re
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate
import scipy.stats
import seaborn as sns

def get_shutoff_time(fname):
    ramp_vals = []
    shutoff_time = 0
    with open(fname, 'r') as f:
        for line in f:
            if 'burner_supply' in line:
                if '&RAMP' in line:
                    ramp_vals.append(line)
    for line in ramp_vals:
        line_tokens = line.split(',')
        for line_token in line_tokens:
            if 'T' in line_token:
                value = re.findall("\d+\.\d+", line_token)
                if (len(value) > 0 ) and (float(value[0]) != 0):
                    shutoff_time = float(value[0])
    return(shutoff_time)

def get_exp_data(pointfile):
    with open(pointfile, 'r') as f:
        values = []
        # skip first line (units -> s, C, C, C, ... C)
        f.next()
        reader = csv.DictReader(f)
        for row in reader:
            values.append(row)
        times = []
        masses = []
        temps = []
        initial_mass = 0.0 #will write over this in loop
        for timestep in values:
            if len(times) == 0: #first time iteration
                initial_mass = float(timestep["mass"])
            t = float(timestep["Time"])
            times.append(t)
            mass = fractional_loss(float(timestep["mass"]) , initial_mass)
            masses.append(mass)
            temp = float(timestep["surf_temp"])
            temps.append(temp)
    return([times,masses],[times,temps])

def adjust_exp_data_for_consumption(data, shutoff_time):
    times = []
    masses = []
    for time,mass in zip(data[0],data[1]):
        if time > shutoff_time:
            times.append(time - shutoff_time)
            masses.append(mass)
    return([times,masses])

def fractional_loss(current, initial):
    return((initial - current)/initial)

def main(plot_temps = False):
    if (len(sys.argv) < 3):
        raise(Exception("Syntax: python fractional_weight_loss_3pane_compare.py <thick directory> <thin directory> <seconds>"))
    dirname1 = sys.argv[1] #thick
    dirname2 = sys.argv[2] #thin
    timelen = int(sys.argv[3])
    pointfile_dir1 = os.getcwd() + "/" + dirname1
    pointfile_dir2 = os.getcwd() + "/" + dirname2

    #find our experiments so we can open them up
    dir_contents = os.listdir(pointfile_dir1)
    fname = "albini_model_devc.csv"
    fname2 = "albini_model.fds"
    for item in dir_contents:
        if "_5" in item:
            exp_5_1, exp_5_1_temps = get_exp_data(dirname1 + '/' + item + '/' + fname)
            shutoff_time_5_1 = get_shutoff_time(dirname1 + '/' + item + '/' + fname2)
            exp_5_1 = adjust_exp_data_for_consumption(exp_5_1, shutoff_time_5_1)
            exp_5_1_temps = adjust_exp_data_for_consumption(exp_5_1_temps, shutoff_time_5_1)
        elif "_6" in item:
            exp_6_1, exp_6_1_temps = get_exp_data(dirname1 + '/' + item + '/' + fname)
            shutoff_time_6_1 = get_shutoff_time(dirname1 + '/' + item + '/' + fname2)
            exp_6_1 = adjust_exp_data_for_consumption(exp_6_1, shutoff_time_6_1)
            exp_6_1_temps = adjust_exp_data_for_consumption(exp_6_1_temps, shutoff_time_6_1)
        elif "_7" in item:
            exp_7_1, exp_7_1_temps = get_exp_data(dirname1 + '/' + item + '/' + fname)
            shutoff_time_7_1 = get_shutoff_time(dirname1 + '/' + item + '/' + fname2)
            exp_7_1 = adjust_exp_data_for_consumption(exp_7_1, shutoff_time_7_1)
            exp_7_1_temps = adjust_exp_data_for_consumption(exp_7_1_temps, shutoff_time_7_1)
        else:
            pass

    #find our experiments so we can open them up
    dir_contents = os.listdir(pointfile_dir2)
    fname = "albini_model_devc.csv"
    fname2 = "albini_model.fds"
    for item in dir_contents:
        if "_5" in item:
            exp_5_2, exp_5_2_temps = get_exp_data(dirname2 + '/' + item + '/' + fname)
            shutoff_time_5_2 = get_shutoff_time(dirname2 + '/' + item + '/' + fname2)
            exp_5_2 = adjust_exp_data_for_consumption(exp_5_2, shutoff_time_5_2)
            exp_5_2_temps = adjust_exp_data_for_consumption(exp_5_2_temps, shutoff_time_5_2)
        elif "_6" in item:
            exp_6_2, exp_6_2_temps = get_exp_data(dirname2 + '/' + item + '/' + fname)
            shutoff_time_6_2 = get_shutoff_time(dirname2 + '/' + item + '/' + fname2)
            exp_6_2 = adjust_exp_data_for_consumption(exp_6_2, shutoff_time_6_2)
            exp_6_2_temps = adjust_exp_data_for_consumption(exp_6_2_temps, shutoff_time_6_2)
        elif "_7" in item:
            exp_7_2, exp_7_2_temps = get_exp_data(dirname2 + '/' + item + '/' + fname)
            shutoff_time_7_2 = get_shutoff_time(dirname2 + '/' + item + '/' + fname2)
            exp_7_2 = adjust_exp_data_for_consumption(exp_7_2, shutoff_time_7_2)
            exp_7_2_temps = adjust_exp_data_for_consumption(exp_7_2_temps, shutoff_time_7_2)
        else:
            pass

    print(shutoff_time_5_1, shutoff_time_6_1, shutoff_time_7_1)
    print(shutoff_time_5_2, shutoff_time_6_2, shutoff_time_7_2)

    sns.set_style('whitegrid')
    fig, (ax2, ax1, ax3) = plt.subplots(1,3,figsize=(24,8), sharey=True)

    if(plot_temps):
        ax1.plot(exp_5_1_temps[0], exp_5_1_temps[1], color='black', marker='.', label='Simulation (thick)')
        ax1.plot(exp_5_2_temps[0], exp_5_2_temps[1], color='blue', marker='.', label='Simulation (thin)')
        ax1.set_title("21% moisture")
        ax1.set_xlabel("Time (s)")
        ax1.set_xlim(0,timelen)


        ax2.plot(exp_6_1_temps[0], exp_6_1_temps[1], color='black', marker='.', label='Simulation (thick)')
        ax2.plot(exp_6_2_temps[0], exp_6_2_temps[1], color='blue', marker='.', label='Simulation (thin)')
        ax2.set_ylabel("Temperature")
        ax2.set_title("4% moisture")
        ax2.set_xlabel("Time (s)")
        ax2.set_xlim(0,timelen)


        ax3.plot(exp_7_1_temps[0], exp_7_1_temps[1], color='black', marker='.', label='Simulation (thick)')
        ax3.plot(exp_7_2_temps[0], exp_7_2_temps[1], color='blue', marker='.', label='Simulation (thin)')
        ax3.set_title("60% moisture")
        ax3.set_xlabel("Time (s)")
        ax3.set_xlim(0,timelen)
        ax3.legend(loc=4)

    else:
        ax1.plot(exp_5_1[0], exp_5_1[1], color='black', marker='.', label='Simulation (thick)')
        ax1.plot(exp_5_2[0], exp_5_2[1], color='blue', marker='.', label='Simulation (thin)')
        ax1.set_title("21% moisture")
        ax1.set_xlabel("Time (s)")
        ax1.set_xlim(0,timelen)


        ax2.plot(exp_6_1[0], exp_6_1[1], color='black', marker='.', label='Simulation (thick)')
        ax2.plot(exp_6_2[0], exp_6_2[1], color='blue', marker='.', label='Simulation (thin)')
        ax2.set_ylabel("Fractional weight loss")
        ax2.set_title("4% moisture")
        ax2.set_xlabel("Time (s)")
        ax2.set_xlim(0,timelen)


        ax3.plot(exp_7_1[0], exp_7_1[1], color='black', marker='.', label='Simulation (thick)')
        ax3.plot(exp_7_2[0], exp_7_2[1], color='blue', marker='.', label='Simulation (thin)')
        ax3.set_title("60% moisture")
        ax3.set_xlabel("Time (s)")
        ax3.set_xlim(0,timelen)
        ax3.legend(loc=4)


    plt.subplots_adjust(left=0.1, wspace=0.1, top=0.8)
    plt.show(block=True)

if __name__ == '__main__':
    main(True)
