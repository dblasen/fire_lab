import os
import csv
import time
import matplotlib.pyplot as plt

fig = plt.figure(figsize = (12,12))
ax = fig.add_subplot(111)

x_points = [2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10]

#ignition times with no drying or pyrolysis
y1_points = [6.7805403E+001,
             8.2203303E+001,
             9.7801557E+001,
             1.1760693E+002,
             1.4460515E+002,
             1.5181030E+002,
             1.7280148E+002,
             1.9200218E+002,
             2.0880279E+002,
             2.3580990E+002,
             2.6400963E+002,
             2.8680881E+002,
             3.0601169E+002,
             3.1980060E+002,
             3.3840038E+002,
             3.6960118E+002
            ]

''' NOTE with drying, the wood rarely reaches ignition temperature!
# ignition times with drying
y2_points = [4.1281234E+002,
             4.9200035E+002,
             ,
             ,
             ,
             ,
             ,
             ,
             ,
             ,
             ,
             ,
             ,
             ,
             ,
             
            ]

# ignition times with full pyrolysis
y3_points = [,
             ,
             ,
             ,
             ,
             ,
             ,
             ,
             ,
             ,
             ,
             ,
             ,
             ,
             ,
             
            ]
'''

# ignition times of thin sphere
y4_points = [1.8606547E+001,
             2.2209025E+001,
             2.7003514E+001,
             3.0611227E+001,
             3.5410736E+001,
             3.9011540E+001,
             4.3800676E+001,
             4.9209292E+001,
             5.4604590E+001,
             5.7605513E+001,
             6.4800807E+001,
             7.0800993E+001,
             7.4405928E+001,
             7.9203113E+001,
             8.7604069E+001,
             9.3011490E+001
            ]

# ignition times of thick spheres
y5_points = [6.0114046E+000,
             6.0125599E+000,
             6.6085962E+000,
             6.6082183E+000,
             7.2001586E+000,
             7.2035483E+000,
             7.2115554E+000,
             8.4051878E+000,
             9.0111831E+000,
             1.0209439E+001,
             1.0805481E+001,
             1.2009221E+001,
             1.2004253E+001,
             1.3206132E+001,
             1.2004253E+001,
             1.6802922E+001
            ]

# ignition times of thin (single cell) spheres
y5b_points = [1.0020916E+002,
             1.3920862E+002,
             1.8480545E+002,
             2.4001173E+002,
             3.0240997E+002,
             3.7200568E+002,
             3.9420216E+002,
             5.5200652E+002,
             6.0000000E+002,
             6.0000000E+002,
             6.0000000E+002,
             6.0000000E+002,
             6.0000000E+002,
             6.0000000E+002,
             6.0000000E+002,
             6.0000000E+002,
            ]

ax.clear()
#ax.plot(x_points, y1_points, color="black", marker='.', linestyle="None", label="No drying or pyrolysis")
ax.plot(x_points, y5b_points, color="blue", marker='.', linestyle="None", label="thin fuel (sphere)")
ax.plot(x_points, y5_points, color="red", marker='.', linestyle="None", label="thick fuel (sphere)")
ax.set_title("Ignition temperatures (327 C) at diameters in FDS6 (12.3% water)")
ax.set_xlabel("Diameter (cm)")
ax.set_ylabel("Time to ignition temp (s)")
ax.set_ylim([0, 600])
ax.legend()
fig.canvas.draw()
plt.show(block=True)
