import os
import sys
import csv
import time
import string
import operator
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate

def get_ignition_time(pointfile):
    ignition_temp = 350
    with open(pointfile, 'r') as f:
        # skip first line (units -> s, C, C, C, ... C)
        values = []
        f.next()
        reader = csv.DictReader(f)
        ignition_time = -1 #overwrite if valid, error if not
        for row in reader:
            if is_ignited(row["surf_temp"],ignition_temp): #first time iteration
                ignition_time = float(row["Time"])
                return(ignition_time)
            else:pass
    return(ignition_time)

def make_line(data, experiment_number, fuel_type):
    new_set = []
    for dictionary in data:
        if (dictionary["value"] == -1): continue
        if (dictionary["exp"] == experiment_number and dictionary['type'] == fuel_type):
            new_set.append([dictionary["diameter"],dictionary["value"]])
    new_set = sorted(new_set, key=operator.itemgetter(0))
    xs = [i[0] for i in new_set]
    ys = [i[1] for i in new_set]
    return(xs,ys)

def open_all_experiments_in_dir(path):
    data = []
    dir_contents = os.listdir(path)
    for item in dir_contents:
        if not os.path.isdir(path + '/' + item):
            continue
        elif "ignore" in item:
            continue
        elif not item[-6:].isdigit(): #end of dirname must be size (e.g. 225001)
            continue
        else:
            data.append({"name":item, "data":gather_data(path + '/' + item + '/')})
    return(data)

def gather_data(path):
    data = []
    dir_contents = os.listdir(path)
    for item in dir_contents:
        if "base" in item:
            continue
        elif "ignore" in item:
            continue
        elif "thin" in item:
            key0 = "thin"
        elif "thick" in item:
            key0 = "thick"
        else:continue
        #need to explore individual experiments now
        path2 = path + item
        dir_contents2 = sorted(os.listdir(path2))
        for entry in dir_contents2:
            key1 = entry[:-2] #diameter
            key1 = string.replace(key1, 'p', '.')
            path3 = path2 + '/' + entry
            dir_contents3 = os.listdir(path3)
            for elt in dir_contents3:
                if not os.path.isdir(path3 + '/' + elt):
                    continue
                if "gpyro" in elt:
                    continue
                key2 = elt[-1] #5,6, or 7th figure
                value = get_ignition_time(path3 + '/' + elt + '/' + 'albini_model_devc.csv')
                # NOTE *10 below to convert to mm from cm
                data.append({'diameter':float(key1)*10, 'type':str(key0), 'exp':int(key2), 'value':float(value)})
    return(data)

def is_ignited(temperature, ignition_temp=350):
    return(float(temperature) > ignition_temp)

def get_differences(vector1, vector2):
    assert (len(vector1) == len(vector2))
    new_vector = []
    for i in range(len(vector1)):
        difference = (abs(vector1[i] - vector2[i]) / vector1[i]) * 100
        #new_vector.append(abs(vector1[i] / vector2[i]))
        new_vector.append(difference)
    return(new_vector)

def get_average_lines(alldata):
    allthick5s = []
    allthin5s = []
    allthick6s = []
    allthin6s = []
    allthick7s = []
    allthin7s = []
    for data in alldata:
        thick_xs1, thick_ys1 = make_line(data['data'], 5, "thick")
        thin_xs1, thin_ys1 = make_line(data['data'], 5, "thin")
        allthin5s.append(thin_ys1)
        allthick5s.append(thick_ys1)
        thick_xs1, thick_ys1 = make_line(data['data'], 6, "thick")
        thin_xs1, thin_ys1 = make_line(data['data'], 6, "thin")
        allthin6s.append(thin_ys1)
        allthick6s.append(thick_ys1)
        thick_xs1, thick_ys1 = make_line(data['data'], 7, "thick")
        thin_xs1, thin_ys1 = make_line(data['data'], 7, "thin")
        allthin7s.append(thin_ys1)
        allthick7s.append(thick_ys1)
    thick5avg = np.average(allthick5s, axis=0)
    thin5avg = np.average(allthin5s, axis=0)
    thick6avg = np.average(allthick6s, axis=0)
    thin6avg = np.average(allthin6s, axis=0)
    thick7avg = np.average(allthick7s, axis=0)
    thin7avg = np.average(allthin7s, axis=0)
    return(thick5avg, thin5avg, thick6avg, thin6avg, thick7avg, thin7avg)

def main():
    dirname1 = sys.argv[1]
    data_dir1 = os.getcwd() + "/" + dirname1
    alldata = open_all_experiments_in_dir(data_dir1) #it's a lot of crap alright.

    #plt.rc('text', usetex=True)
    #fig = plt.figure(figsize = (24,8))
    fig, (ax2, ax1, ax3) = plt.subplots(1,3,figsize=(24,8), sharey=True)

    for data in alldata:
        thick_xs1, thick_ys1 = make_line(data['data'], 5, "thick")
        thin_xs1, thin_ys1 = make_line(data['data'], 5, "thin")
        ax1.plot(thick_xs1, thick_ys1, color='red', linestyle='dashed')
        ax1.plot(thin_xs1, thin_ys1, color='blue', linestyle='dashed')

        thick_xs1, thick_ys1 = make_line(data['data'], 6, "thick")
        thin_xs1, thin_ys1 = make_line(data['data'], 6, "thin")
        ax2.plot(thick_xs1, thick_ys1, color='red', linestyle='dashed')
        ax2.plot(thin_xs1, thin_ys1, color='blue', linestyle='dashed')

        thick_xs1, thick_ys1 = make_line(data['data'], 7, "thick")
        thin_xs1, thin_ys1 = make_line(data['data'], 7, "thin")
        ax3.plot(thick_xs1, thick_ys1, color='red', linestyle='dashed')
        ax3.plot(thin_xs1, thin_ys1, color='blue', linestyle='dashed')

    thick5avg, thin5avg, thick6avg, thin6avg, thick7avg, thin7avg = get_average_lines(alldata)
    ax1.plot(thick_xs1, thick5avg, color='red', linestyle='solid', linewidth=4, label='thick average')
    ax1.plot(thin_xs1, thin5avg, color='blue', linestyle='solid', linewidth=4, label='thin average')

    ax2.plot(thick_xs1, thick6avg, color='red', linestyle='solid', linewidth=4, label='thick average')
    ax2.plot(thin_xs1, thin6avg, color='blue', linestyle='solid', linewidth=4, label='thin average')

    ax3.plot(thick_xs1, thick7avg, color='red', linestyle='solid', linewidth=4, label='thick average')
    ax3.plot(thin_xs1, thin7avg, color='blue', linestyle='solid', linewidth=4, label='thin average')

    ax3.set_title(str("60% Moisture"))
    ax3.set_xlabel("Particle Diameter (mm)")
    ax3.legend(loc=2)
    ax3.grid()

    ax2.set_title(str("4% Moisture"))
    ax2.set_xlabel("Particle Diameter (mm)")
    ax2.set_ylabel("Ignition time (s)")
    ax2.grid()

    ax1.grid()
    ax1.set_title(str("21% Moisture"))
    ax1.set_xlabel("Particle Diameter (mm)")
    plt.subplots_adjust(left=0.1, wspace=0.1, top=0.8)
    plt.show(block=True)

if __name__ == '__main__':
    main()
