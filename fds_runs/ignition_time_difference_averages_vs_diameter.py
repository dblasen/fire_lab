import os
import sys
import csv
import time
import string
import operator
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate
import seaborn as sns

import ignition_time_average_noise_check

def get_ignition_time(pointfile):
    ignition_temp = 350
    with open(pointfile, 'r') as f:
        # skip first line (units -> s, C, C, C, ... C)
        values = []
        f.next()
        reader = csv.DictReader(f)
        ignition_time = -1 #overwrite if valid, error if not
        for row in reader:
            if is_ignited(row["surf_temp"],ignition_temp): #first time iteration
                ignition_time = float(row["Time"])
                return(ignition_time)
            else:pass
    return(ignition_time)

def make_line(data, experiment_number, fuel_type):
    new_set = []
    for dictionary in data:
        if (dictionary["value"] == -1): continue
        if (dictionary["exp"] == experiment_number and dictionary['type'] == fuel_type):
            new_set.append([dictionary["diameter"],dictionary["value"]])
    new_set = sorted(new_set, key=operator.itemgetter(0))
    xs = [i[0] for i in new_set]
    ys = [i[1] for i in new_set]
    return(xs,ys)


def gather_data(path):
    data = []
    dir_contents = os.listdir(path)
    for item in dir_contents:
        if "base" in item:
            continue
        elif "ignore" in item:
            continue
        elif "thin" in item:
            key0 = "thin"
        elif "thick" in item:
            key0 = "thick"
        else:continue
        #need to explore individual experiments now
        path2 = path + item
        dir_contents2 = sorted(os.listdir(path2))
        for entry in dir_contents2:
            key1 = entry[:-2] #diameter
            key1 = string.replace(key1, 'p', '.')
            path3 = path2 + '/' + entry
            dir_contents3 = os.listdir(path3)
            for elt in dir_contents3:
                if not os.path.isdir(path3 + '/' + elt):
                    continue
                if "gpyro" in elt:
                    continue
                key2 = elt[-1] #5,6, or 7th figure
                value = get_ignition_time(path3 + '/' + elt + '/' + 'albini_model_devc.csv')
                # NOTE *10 below to convert to mm from cm
                data.append({'diameter':float(key1)*10, 'type':str(key0), 'exp':int(key2), 'value':float(value)})
    return(data)

def is_ignited(temperature, ignition_temp=350):
    return(float(temperature) > ignition_temp)

def get_differences(vector1, vector2):
    assert (len(vector1) == len(vector2))
    new_vector = []
    for i in range(len(vector1)):
        difference = (abs(vector1[i] - vector2[i]) / vector1[i]) * 100
        #new_vector.append(abs(vector1[i] / vector2[i]))
        new_vector.append(difference)
    return(new_vector)

def main():

    dirname1 = sys.argv[1] #half
    dirname2 = sys.argv[2] #normal
    dirname3 = sys.argv[3] #double
    dirname4 = sys.argv[4] #quarter
    data_dir1 = os.getcwd() + "/" + dirname1
    data_dir2 = os.getcwd() + "/" + dirname2
    data_dir3 = os.getcwd() + "/" + dirname3
    data_dir4 = os.getcwd() + "/" + dirname4
    alldata1 = ignition_time_average_noise_check.open_all_experiments_in_dir(data_dir1)
    alldata2 = ignition_time_average_noise_check.open_all_experiments_in_dir(data_dir2)
    alldata3 = ignition_time_average_noise_check.open_all_experiments_in_dir(data_dir3)
    alldata4 = ignition_time_average_noise_check.open_all_experiments_in_dir(data_dir4)

    sns.set_style('whitegrid')
    fig, (ax2, ax1, ax3) = plt.subplots(1,3,figsize=(24,8), sharey=True)

    thick5avgd1, thin5avgd1, thick6avgd1, thin6avgd1, thick7avgd1, thin7avgd1 = ignition_time_average_noise_check.get_average_lines(alldata1)
    thick5avgd2, thin5avgd2, thick6avgd2, thin6avgd2, thick7avgd2, thin7avgd2 = ignition_time_average_noise_check.get_average_lines(alldata2)
    thick5avgd3, thin5avgd3, thick6avgd3, thin6avgd3, thick7avgd3, thin7avgd3 = ignition_time_average_noise_check.get_average_lines(alldata3)
    thick5avgd4, thin5avgd4, thick6avgd4, thin6avgd4, thick7avgd4, thin7avgd4 = ignition_time_average_noise_check.get_average_lines(alldata4)

    ys1 = get_differences(thin5avgd1, thick5avgd1)
    ys2 = get_differences(thin5avgd2, thick5avgd2)
    ys3 = get_differences(thin5avgd3, thick5avgd3)
    ys4 = get_differences(thin5avgd4, thick5avgd4)
    thick_xs1 = [1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0]
    ax1.plot(thick_xs1, ys1, color='red', marker='.', label='225 kW/m^2')
    ax1.plot(thick_xs1, ys2, color='blue', marker='.', label='450 kW/m^2')
    ax1.plot(thick_xs1, ys3, color='green', marker='.', label='900 kW/m^2')
    ax1.plot(thick_xs1, ys4, color='lightgreen', marker='.', label='112.5 kW/m^2')
    ax1.set_title(str("21% Moisture"))
    ax1.set_xlabel("Particle Diameter (mm)")

    ys1 = get_differences(thin6avgd1, thick6avgd1)
    ys2 = get_differences(thin6avgd2, thick6avgd2)
    ys3 = get_differences(thin6avgd3, thick6avgd3)
    ys4 = get_differences(thin6avgd4, thick6avgd4)
    ax2.plot(thick_xs1, ys1, color='red', marker='.', label='225 kW/m^2')
    ax2.plot(thick_xs1, ys2, color='blue', marker='.', label='450 kW/m^2')
    ax2.plot(thick_xs1, ys3, color='green', marker='.', label='900 kW/m^2')
    ax2.plot(thick_xs1, ys4, color='lightgreen', marker='.', label='112.5 kW/m^2')
    ax2.set_title(str("4% Moisture"))
    ax2.set_xlabel("Particle Diameter (mm)")
    ax2.set_ylabel("Thin ignition time (percentage over thick)")

    ys1 = get_differences(thin7avgd1, thick7avgd1)
    ys2 = get_differences(thin7avgd2, thick7avgd2)
    ys3 = get_differences(thin7avgd3, thick7avgd3)
    ys4 = get_differences(thin7avgd4, thick7avgd4)
    ax3.plot(thick_xs1, ys1, color='red', marker='.', label='225 kW/m^2')
    ax3.plot(thick_xs1, ys2, color='blue', marker='.', label='450 kW/m^2')
    ax3.plot(thick_xs1, ys3, color='green', marker='.', label='900 kW/m^2')
    ax3.plot(thick_xs1, ys4, color='lightgreen', marker='.', label='112.5 kW/m^2')
    ax3.set_title(str("60% Moisture"))
    ax3.set_xlabel("Particle Diameter (mm)")
    ax3.legend(loc=2)

    plt.subplots_adjust(left=0.1, wspace=0.1, top=0.8)
    plt.show(block=True)


if __name__ == "__main__":
    main()
