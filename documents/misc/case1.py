from fenics import *
import numpy as np
T = 5.0 #final time
num_steps = 50 #number of time steps
dt = T / num_steps #time step size
alpha = 3 #parameter alpha
beta = 1.2 #parameter beta

temp_f = 300
temp_a = 23.5
temp_s = temp_a

# Create mesh and define function space
nx = 100 #number of intervals in the mesh
x_min = 0.0
x_max = 10.0
mesh = IntervalMesh(nx, x_min, x_max)
V = FunctionSpace(mesh, 'CG', 1)
# Define boundary condition
#u_D = Constant(0) #needed?
u_D = Expression('1 + x[0] + beta*t', degree=1, beta=beta, t=0)
#u_D = Expression('1 + x[0] + beta*t',
#degree=1, beta=beta, t=0)

tol = 1E-12

def boundary_hot(x, on_boundary):
    if on_boundary:
        if near(x[0], 0, tol) or near(x[0], 1, tol):
            return True
        else:
            return False
    else:
        return False

def boundary(x, on_boundary):
    return on_boundary and near(x[0], 0, tol)

#get the current transfer of heat into the medium
def surface():
    h_eff = 1 #TODO who knows what this should be
    #temp_s = x[0][0]
    return(Constant(h_eff * (temp_f - temp_a) - h_eff * (temp_s - temp_a)))

surf = surface()
bc = DirichletBC(V, surf, boundary)
# Define initial value
u_n = interpolate(u_D, V)
#u_n = project(u_D, V)

#Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
#f = Constant(beta - 2 - 2*alpha) #TODO this is clearly part of the problem
f = Constant(0) #rho cp?
#f = Constant(surface, boundary(V))
#F = u*v*dx + dt*dot(grad(u), grad(v))*dx - (u_n + dt*f)*v*dx
F = u*v*dx + dt*dot(grad(u), grad(v))*dx - (u_n + dt*f)*v*dx
a, L = lhs(F), rhs(F)
# Time-stepping
u = Function(V)
t = 0
for n in range(num_steps):
    # Update current time
    t += dt
    u_D.t = t
    # Compute solution
    solve(a == L, u, bc)
    plot(u)
    # Compute error at vertices
    u_e = interpolate(u_D, V)
    error = np.abs(u_e.vector().array() - u.vector().array()).max()
    #print('t = %.2f: error = %.3g' % (t, error))
    print('t = %.2f: ' % (t))
    # Update previous solution
    u_n.assign(u)
# Hold plot
interactive()
