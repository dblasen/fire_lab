\section*{Chapter 2: Estimation of Material Parameters}\label{sec:Chapter 2: Estimation of Material Parameters}

\subsection*{Existing Material Parameters}

	Initial testing of the model suggested that the materials were not combusting at an appropriate rate. This issue presented itself in the form of unreasonably slow rates of fuel consumption when the solids were placed in the verification model: a large fire bath with a controlled heat release. This could have been explained by numerous potential causes but one area of immediate suspicion was in the kinetic parameters of our solid materials. 
	These material parameters were obtained directly from FDS simulation input files intended to model burning vegetetation-- specifically, needles from a fir tree. In addition, experimentation suggested that the model was highly sensitive to perturbation of some kinetic parameters and thus that determining exact values from experimental data would be a valuable endeavor.
	  
\subsection*{Arrhenius Equation Constants}
	  
	Many modern pyrolysis models (including FDS) depend on the use of Arrhenius Equation kinetic constants to describe how they combust. It is easiest to consider  this formulation by examining the transition of a single solid-phase species to another solid-phase species through a single reaction:

$\frac{da}{dt} = A\ exp(- \frac{E}{RT})(1-a)^n$

	 Where $a$ is the dimensionless conversion (and hence $\frac{da}{dt}$ the \textit{rate} of converison), $A$ (denoted $Z$ in some models) the pre-exponential factor ($s^{-1}$), $E$ the activation energy ($\frac{kJ}{mol}$), $n$ the reaction order, $R$ the gas constant ($8.314 \frac{J}{mol*K}$), and $T$ the temperature ($^\circ K$) \citep{Lautenberger2009}.
	
	 This is a new development; previous versions of FDS (and other models still in some regular use) depend instead on other descriptors for this behavior, like reference temperature and reference rate. These parameters are more easily determined from laboratory measurements but have fallen from use in current models as researchers have moved towards adoption of validated physical systems \citep{Hurley2016}. The new parameters, which are advantageous because they capture the behavior of temperature-dependent physical parameters in a more general manner, cannot be easily measured in the laboratory but may be numerically determined from experimental data.  
	  
	\subsection*{Approaches to Determining Kinetic Parameters}  
	  
	There are a few valid approaches to identifying the correct values for these kinetic constants. One approach to finding kinetic constants is to numerically solve the following ODEs:

$\frac{dY_a}{dt} = -A_{a}Y_{a}exp(-\frac{E_a}{RT})\ ;\ Y_{a}(0) = Y_{a,0}\ ;\ a = 1,N $	

\vspace{1mm}

$\frac{dY_r}{dt} = -v_{r}\sum_{a}\frac{dY_a}{dt}\ ;\ Y_{r}(0) = 0 $
	
\noindent Where $Y_a$ is the mass fraction of $a$, $Y_r$ is the residue of the reaction, and $v_r$ is the yield of the reaction \citep{McGrattan2017a}.
	
	 A newer method with growing popularity is the option of automatically estimating the parameters by using a genetic algorithm to find a good fit to the experimental data with a large number of unknown parameters \citep{McGrattan2017a}. This is desirable because the complexity of the system of ODEs required to describe the experiment grows rapidly and with it the difficulty of the numerical solution-- both in terms of computation time and user effort. By comparison, the genetic algorithm may use more computation time but it is considerably less taxing on the user.  
	  
	  
	Genetic algorithms are a broad collection of optimization techniques based on the concept of natural selection as a function of individual fitness. More fit genes make individuals more likely to breed and thus more likely to be represented in future generations; in this case, the genes are unknown parameters, individuals are complete simulations, and fitness is agreement between the individual and the experimental data. Fitness of an individual is computed in parts as:
	
$E_{T_{s}}^{I} = \frac{1}{n_{\Delta t}} \sum ( \frac{T_{s,exp}}{\lvert T_{s,try} - T_{s,exp}\rvert + \epsilon T_{s,exp}} ) ^ \xi $  

\vspace{1mm}
  
$E_{\dot{m}''_{s}}^{I} = \frac{1}{n_{\Delta t}} \sum ( \frac{\dot{m}''_{s,exp}}{\lvert \dot{m}''_{s,try} - \dot{m}''_{s,exp}\rvert + \epsilon \dot{m}''_{s,exp}} ) ^ \xi $  
  
\noindent where $I$ denotes an individual, $E$ the inverse of the error (and its subscript $T$ for temperature or $\dot{m}''$ for mass flux), $n_{\Delta t}$  the total number of time steps, $\epsilon$ a very small number to prevent division by zero, and $\xi$ the exponent to which the error's inverse is raised.
  
These inverse errors are then weighted and summed to determine the individual's fitness:  
  	   
$ \widetilde{f}^I = \phi_{T_s} E_{T_s}^I +  \phi_{\dot{m}''_s} E_{\dot{m}''_s}^I $

\noindent where each $\phi$ is a scalar for the inverse error of that parameter. Separate fitness values for each experiment are summed:

$ {f}^I = \sum_{n_{\dot{q}''_e}} \widetilde{f}^I $


	This describes how fitness is assigned to each set of parameters is fairly fine detail but doesn't address how fit individuals are selected for reproduction nor their offspring mutated nor how the new generation replaces the old; these processes are fully documented but beyond the scope of this discussion \citep{Lautenberger2016}.
	 
	  
	\subsection*{Methods}
	  
	The experimental data are from three TGA experiments at 5, 10, and 20 degree per minute heating schedules in an anoxic environment. The samples are all ground, oven-dried wood from the same specimen of \textit{Pinus Contorta}.

	Our model is formulated as a zero-dimensional simulation, meaning that a homogeneous particle with no internal gradients is subjected to gradually increasing atmospheric temperature. The atmosphere is anoxic so the particle does not burn. The particle varies in temperature, mass, and solid-phase species mass fraction as it undergoes this process. It is generally accepted that thermally-thin models are appropriate for simulating TGA \citep{Mcgrattan2016}. 
	
	We describe the TGA specimen as three condensed-phase species undergoing two reactions with the following known characteristics:  
	  
\vspace{5mm}
\begin{tabular}{ |p{4cm}|p{4cm}|p{1.5cm}|p{1.5cm}|  }
\hline
\multicolumn{4}{|c|}{Condensed-phase Species} \\
\hline
Parameter (\textit{unit})& Dry Vegetation & Char & Ash \\
\hline
Specific Heat $(\frac{J}{kg*K})$ & 1800 & 1400 & 850 \\
Emissivity $(-)$ & 0.95 & 0.90 & 0.50\\
Density $kg*m^3$ & 588 & (-) & (-) \\
\hline
\end{tabular}
\vspace{5mm}

\vspace{5mm}
\begin{tabular}{ |p{4cm}|p{4cm}|p{2cm}| }
\hline
\multicolumn{3}{|c|}{Condensed-phase Reactions} \\
\hline
Parameter (\textit{unit})& DryVegetation/Char & Char/Ash \\
\hline
Solid Enthalpy $(\frac{J}{kg})$ & 0 & 0 \\
Vapor Enthalpy $(\frac{J}{kg})$ & $10^4$ & $10^4$ \\
\hline
\end{tabular}
\vspace{5mm}

	We permit the genetic algorithm to modify the following parameters with corresponding ranges to determine optimal values:
	
\vspace{5mm}
\begin{tabular}{ |p{5cm}|p{3cm}|p{2cm}|p{2cm}|  }
\hline
\multicolumn{4}{|c|}{Condensed-phase Species Parameters} \\
\hline
Parameter(\textit{unit}) Min|Max & Dry Vegetation & Char & Ash \\
\hline
Density $kg*m^3$ & (-)(-) & 300|580 & 100|300 \\
Conductivity $\frac{W}{m*K}$ & 50|2500 & 10|100 & (-) \\
Cond. Exponent $(-)$ & 0|5 & 0|5 & (-) \\
\hline
\end{tabular}
\vspace{5mm}

\vspace{5mm}
\begin{tabular}{ |p{5cm}|p{4cm}|p{2cm}| }
\hline
\multicolumn{3}{|c|}{Condensed-phase Reactions} \\
\hline
Parameter(\textit{unit}) Min|Max & DryVegetation/Char & Char/Ash \\
\hline
$A$ $(s^{-1})$ & $10^6$|$10^{11}$ & $10^4$|$10^{11}$ \\
$E$ $(\frac{kJ}{mol})$ & $60$|$200$ & $60$|$200$ \\
Order $(-)$ & $1.0$|$4.0$ & $1.0$|$4.0$ \\
\hline
\end{tabular}
\vspace{5mm}
	
\break	
	
	\subsection*{Results}
	
	We find the following set of parameters at the end of optimization:
	
\vspace{5mm}
\begin{tabular}{ |p{5cm}|p{3cm}|p{2cm}|p{2cm}|  }
\hline
\multicolumn{4}{|c|}{Condensed-phase Species Parameters} \\
\hline
Parameter(\textit{unit}) & Dry Vegetation & Char & Ash \\
\hline
Density $kg*m^3$ & (-)(-) & 402 & 133 \\
Conductivity $\frac{W}{m*K}$ & 0.195 & 0.760 & (-)\\
Cond. Exponent $(-)$ & 0.371 & 0.500 & (-) \\
\hline
\end{tabular}
\vspace{5mm}

\vspace{5mm}
\begin{tabular}{ |p{5cm}|p{4cm}|p{2cm}| }
\hline
\multicolumn{3}{|c|}{Condensed-phase Reactions} \\
\hline
Parameter(\textit{unit}) Min|Max & DryVegetation/Char & Char/Ash \\
\hline
$A$ $(s^{-1})$ & $9.99^{11}$ & $9.99^{11}$ \\
$E$ $(\frac{kJ}{mol})$ & $142.3$ & $163.8$ \\
Order $(-)$ & $3.64$ & $3.99$ \\
\hline
\end{tabular}
\vspace{5mm}
	
	The best fitness as a function of generation is displayed in \fig{fig:fig_1}. The property estimation terminated early due to convergence on a solution.
	
\begin{figure}

\includegraphics[width=\textwidth]{figures/jpg/figure_1-12.eps}
\isucaption{Property estimation converges on a solution early ($<500$ generations).}
\label{fig:fig_1}
\end{figure}

	  \subsection*{Discussion}
	  
	  Comparison between experimental and simulated TGA in \fig{fig:fig_2} shows good agreement, suggesting the optimization managed to successfully fit to the experimental data as desired. The early stop mentioned above suggests that the optimization found an optimal fit given its input space. Overall, the fit is excellent and captures the behavior of the material in all three heating schedules quite well.  
	  
	  The most obvious area of issues is the early part of the simulated TGA where it lags behind the experimental data in pyrolyzing; this is due to the very simple three-material formulation, which drives the optimizer to fit the first (of two) reactions from roughly $t = 0$ to $t = 3000$ on the 5 degree schedule. This is the 'optimal' solution since it ensures that the rest of the experiment ($t > 3000$) can be fit with the second reaction very well.
	  
	  The selected material parameter values are also suspect in areas-- particularly the pre-exponential factor. This is likely affected by the very simple nature of the experiment but it remains an open question.
	  
	  There is still uncertainty as to whether a better valid fit might be found with additional condensed-phase material transitions. There is little doubt that additional transitions would further decrease error between the experimental and simulated TGA but it is not clear at this time whether there is adequate justification in fire science literature for additional transitions. It is likely that we will find this justification and then re-examine these models to improve precision further.
	  
\begin{figure}

\includegraphics[width=\textwidth]{figures/jpg/3pane_square.eps}
\isucaption{Simulated versus experimental TGA}
\label{fig:fig_2}
\end{figure}