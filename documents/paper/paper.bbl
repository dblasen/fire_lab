\begin{thebibliography}{13}
\expandafter\ifx\csname natexlab\endcsname\relax\def\natexlab#1{#1}\fi
\providecommand{\bibinfo}[2]{#2}
\ifx\xfnm\relax \def\xfnm[#1]{\unskip,\space#1}\fi
%Type = Article
\bibitem[{Mell et~al.(2007)Mell, Jenkins, Gould, and Cheney}]{Mell2007}
\bibinfo{author}{W.~Mell}, \bibinfo{author}{M.~A. Jenkins},
  \bibinfo{author}{J.~Gould}, \bibinfo{author}{P.~Cheney},
\newblock \bibinfo{title}{{A physics-based approach to modelling grassland
  fires}},
\newblock \bibinfo{journal}{International Journal of Wildland Fire}
  \bibinfo{volume}{16} (\bibinfo{year}{2007}) \bibinfo{pages}{1--22}.
%Type = Book
\bibitem[{Quintiere(2006)}]{Quintiere2006}
\bibinfo{author}{J.~G. Quintiere}, \bibinfo{title}{{Fundamentals of Fire
  Phenomena}}, \bibinfo{year}{2006}.
%Type = Article
\bibitem[{Mcgrattan and Mcdermott(2016)}]{Mcgrattan2016}
\bibinfo{author}{K.~Mcgrattan}, \bibinfo{author}{R.~Mcdermott},
\newblock \bibinfo{title}{{Sixth Edition Fire Dynamics Simulator User ' s
  Guide}}  (\bibinfo{year}{2016}).
%Type = Article
\bibitem[{Linn et~al.(2002)Linn, Reisner, Colman, and Winterkamp}]{Linn2002}
\bibinfo{author}{R.~Linn}, \bibinfo{author}{J.~Reisner}, \bibinfo{author}{J.~J.
  Colman}, \bibinfo{author}{J.~Winterkamp},
\newblock \bibinfo{title}{{Studying wildfire behavior using FIRETEC}},
\newblock \bibinfo{journal}{International Journal of Wildland Fire}
  \bibinfo{volume}{11} (\bibinfo{year}{2002}) \bibinfo{pages}{233}.
%Type = Article
\bibitem[{Lamorlette and Candelier(2015)}]{Lamorlette2015}
\bibinfo{author}{A.~Lamorlette}, \bibinfo{author}{F.~Candelier},
\newblock \bibinfo{title}{{Thermal behavior of solid particles at ignition:
  Theoretical limit between thermally thick and thin solids}},
\newblock \bibinfo{journal}{International Journal of Heat and Mass Transfer}
  \bibinfo{volume}{82} (\bibinfo{year}{2015}) \bibinfo{pages}{117--122}.
%Type = Article
\bibitem[{Albini and Reinhardt(1995)}]{Albini1995a}
\bibinfo{author}{F.~A. Albini}, \bibinfo{author}{E.~D. Reinhardt},
\newblock \bibinfo{title}{{Modeling Ignition And Burning Rate Of Large Woody
  Natural Fuels}},
\newblock \bibinfo{journal}{International Journal of Wildland Fire}
  \bibinfo{volume}{5} (\bibinfo{year}{1995}) \bibinfo{pages}{81--91}.
%Type = Article
\bibitem[{Ritchie et~al.(????)Ritchie, Steckler, Hamins, and Cleary}]{Ritchie}
\bibinfo{author}{S.~J. Ritchie}, \bibinfo{author}{K.~D. Steckler},
  \bibinfo{author}{A.~Hamins}, \bibinfo{author}{T.~G. Cleary},
\newblock \bibinfo{title}{{The Effect of Sample Size on the Heat Release Rate
  of Charring Materials}}  (????) \bibinfo{pages}{177--188}.
%Type = Misc
\bibitem[{Mcdermott(2017)}]{Mcdermott2017}
\bibinfo{author}{R.~Mcdermott}, \bibinfo{title}{{FDS road map}},
  \bibinfo{year}{2017}.
%Type = Article
\bibitem[{Lautenberger(2014)}]{Lautenberger2009}
\bibinfo{author}{C.~Lautenberger},
\newblock \bibinfo{title}{{Gpyro – A Generalized Pyrolysis Model for
  Combustible Solids Technical Reference Chris Lautenberger 63 Hesse Hall
  Department of Mechanical Engineering University of California , Berkeley
  Berkeley , CA 94720 Acknowledgments}}  (\bibinfo{year}{2014}).
%Type = Article
\bibitem[{McGrattan et~al.(2017)McGrattan, Hostikka, McDermott, Floyd,
  Weinschenk, and Overholt}]{McGrattan2017}
\bibinfo{author}{K.~McGrattan}, \bibinfo{author}{S.~Hostikka},
  \bibinfo{author}{R.~McDermott}, \bibinfo{author}{J.~Floyd},
  \bibinfo{author}{C.~Weinschenk}, \bibinfo{author}{K.~Overholt},
\newblock \bibinfo{title}{{Sixth Edition Fire Dynamics Simulator Technical
  Reference Guide Volume 1 : Mathematical Model}} \bibinfo{volume}{1}
  (\bibinfo{year}{2017}) \bibinfo{pages}{1--147}.
%Type = Article
\bibitem[{Alves and Figueiredo(1989)}]{Alves1989}
\bibinfo{author}{S.~S. Alves}, \bibinfo{author}{J.~L. Figueiredo},
\newblock \bibinfo{title}{{A model for pyrolysis of wet wood}},
\newblock \bibinfo{journal}{Chemical Engineering Science} \bibinfo{volume}{44}
  (\bibinfo{year}{1989}) \bibinfo{pages}{2861--2869}.
%Type = Book
\bibitem[{Rockafellar et~al.(2009)Rockafellar, Wets, and
  Wets}]{rockafellar2009variational}
\bibinfo{author}{R.~T. Rockafellar}, \bibinfo{author}{M.~Wets},
  \bibinfo{author}{R.~J.~B. Wets}, \bibinfo{title}{{Variational Analysis}},
  Grundlehren der mathematischen Wissenschaften, \bibinfo{publisher}{Springer
  Berlin Heidelberg}, \bibinfo{year}{2009}.
%Type = Book
\bibitem[{Bergman et~al.(2011)Bergman, Lavine, Incropera, and DeWitt}]{bergman}
\bibinfo{author}{T.~L. Bergman}, \bibinfo{author}{A.~S. Lavine},
  \bibinfo{author}{F.~P. Incropera}, \bibinfo{author}{D.~P. DeWitt},
  \bibinfo{title}{{Fundamentals of Hear and Mass Transfer}},
  \bibinfo{publisher}{Wiley}, \bibinfo{edition}{7th} edition,
  \bibinfo{year}{2011}.

\end{thebibliography}
