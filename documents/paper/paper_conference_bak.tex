\documentclass[5p, times, preprint]{elsarticle}

\usepackage{gensymb}
\usepackage{amssymb}
\usepackage{lineno}
\usepackage{graphicx}
\usepackage{float}
%\usepackage{svg}

\journal{Journal Name}

\begin{document}

\begin{frontmatter}

\title{Quantifying effects of using thermally thin fuel approximations on modelling fire propagation in woody fuels}

%% use optional labels to link authors explicitly to addresses:
%% \author[label1,label2]{<author name>}
%% \address[label1]{<address>}
%% \address[label2]{<address>}

\author{David Blasen}
\author{Jesse Johnson}
\author{William Jolly}
\author{Russell Parsons}

\address{Montana, United States}

\begin{abstract}
%% Text of abstract
In this paper, we quantify the effects of thermally thin fuel prescriptions commonly made in numerical models that eliminate temperature gradients within a heated object. This assumption affects the modeled ignition and burn behavior but there is little research on its impact, particularly in larger fuels.
We begin by comparing modeled to observed ignition times and burn rates. To control for the variability in the material properties of wood, as opposed to the modeled variability due to the thin-fuel assumption, we conduct experiments using thermogravimetric analysis (TGA) for samples of lodgepole pine. From this data, we derive material properties via optimization with genetic algorithms. We also perform burnout experiments on large, woody fuels to confirm ignition time and mass loss rates for a range of fuel specimens. These experiments are then repeated with a numerical modeling platform to validate the model. Once validated, we use the model to explore the significance of thermally thin fuel assumptions by performing the same analyses on fuels with both thermally thick and thermally thin fuel treatments. We quantify the above phenomena but also examine how the composition of fuels varies spatially and temporally. 
\end{abstract}

\end{frontmatter}

%%
%% Start line numbering here if you want
%%
%\linenumbers

%% main text
\section{Introduction}
\label{S:1}

Physics-based simulation of fire has become a valuable tool in the study of wildland fire behavior \citep{Mell2007}. The decreasing cost of computation and improvements in model fidelity present an experimental platform for hypothesis testing that is far less constrained by limitations of physical laboratories. In addition, the precise measurements available within a computational model simplify the study of processes that are difficult to measure experimentally. A growing selection of toolkits for designing and carrying out computational experiments presents many opportunities to scientists curious about fire behavior but unable to carry out experiments on the desired scale.
\par
However, among the many opportunities offered by these toolkits are pitfalls that unwary users may encounter. Building even relatively simple models requires diligent observation of good design principles and skeptical examination of their results; it is trivially easy to get interesting results from a nonsensical model. These pitfalls are not intended to confound new users-- they are natural byproducts of the enormous flexibility of the platforms faithfully carrying out the user’s instructions whether they accurately prescribe the scenario or not. It is plausible that a user could use kinetic parameters for a fuel that they found but have not validated for their case. There are also many ways to prescribe sources of heat with significant differences in their behavior. The user is expected to know where these tools are valid.
\par
One such example of an assumption with a limited range of validity lies in the prescription of thermally thin fuels. By definition, a thermally thin fuel has no temperature gradient \citep{Quintiere2006}. This assumption requires that the object be small as described in Appendix A and is generally stated to be valid for most fuels under 1 mm thick  \citep{Quintiere2006}.
\par
In spite of the limited range of validity, simulation toolkits like FDS \citep{Mcgrattan2016} allow prescription of thermally thin fuel treatments. Models like FIRETEC \citep{Linn2002} use them exclusively, possibly surpassing their range of validity. While thermally thin fuels may be very appropriate representations for certain objects, it is ultimately up to the user to ensure that they are utilized in the right scenarios. These models place no restriction on the application of thermally thin fuel prescriptions.
The ignition time for thermally thin fuels has been studied \citep{Quintiere2006} but other relevant properties such as the mass loss rates of ignited fuels are not well characterized. Thus there is a need to quantify other consequences of thermally thin fuel treatments.
\par
In this paper, we begin by creating an appropriate model for heating cylindrical fuel elements over a fire. We validate the model by comparing to previous experimental work then proceed to test a variety of fuels treated as thermally thick and thermally thin to quantify differences in their ignition times and mass loss rates. We also examine the  composition of the fuels as they undergo heating and pyrolysis. The fuels are prescribed with differing moisture contents and size and are burned in fire environments of varying intensity. We contrast the behaviors of thermally thick and thermally thin fuel treatments to illustrate their appropriateness in various scenarios.

\section{Methods}
\label{S:2}

\subsection{Description}
\label{S:2.1}

Our model aims to capture relevant physical processes by emulating an experimental design that is appealing due to its simplicity and the utility of the data it gathered \citep{Albini1995a}. We specify a $1m^2$ open-flame burner and place wooden fuels 60cm above where they absorb a fraction of the burner’s heat flux. We model a sensor inside of a ball of ceramic cement and tune our burner to roughly match the average recorded fire environment temperature from the experiment (~928 \degree K). We specify the parameters of our wood fuels to match those derived by numerical approximation of experimental results described below. We describe the chemistry of the fuel produced by the pyrolyzing fuel by referring to existing work that identifies heat release rate of burning wood rather than bench-scale determinations \citep{Ritchie}. The computational domain is 2m by 2m by 1m with a 10cm mesh scale; this resolution was determined through mesh refinement to be the point at which results cease to depend on mesh resolution. The boundaries, aside from the ground, are treated as open air. The resulting model exhibits much of the desired behavior.

\subsection{Material Parameters}

One area of considerable uncertainty in the formulation of the model lay in the material parameters of our fuels. While there are numerous examples of woody fuel materials in FDS, they vary considerably in their parameterizations and it's unclear which description might be appropriate for a new fuel. A shortage of validated material parameters is in fact a lingering concern of FDS; while the project’s modeling of the physics in solid fuels is believed to be reasonably good, the lack of validated material parameters for many solid fuels of interest remains an area of active work \citep{Mcdermott2017}.
\par
Our solution to this problem is to derive material parameters from experimental data. We conducted thermogravimetric analysis (TGA) on samples of lodgepole pine (Pinus contorta) and used this data as input to gpyro, a fine-resolution pyrolysis model that has a module for numerically approximating material parameters \citep{Lautenberger2009}. By doing this we were able to obtain the kinetic parameters for dried samples of lodgepole pine wood which we then used in our model. The parameters we identified are enumerated in Table 1 in Appendix B along with a description of their relationships. The fit between our experimental data and a simultation using these parameters can be seen in Figure 1f.

\subsection{Thin Fuel Support}
Another important consideration for this model is that it must allow for simple characterization of both thin and thick fuels. The platform used in this project, FDS Version 6, permits both fine and coarse sub-mesh fuel description of temperature and material composition. Individual fuels in the mesh have their own sub-grids that are smaller than that used for the computational fluid dynamics of the mesh \citep{McGrattan2017}. A user may impose a coarse representation upon the fuel to prescribe thermally thin treatment. 

\subsection{Model Assumptions}
The model operates under a few key assumptions imposed by the simulation framework: \par

\begin{enumerate}
\item Only one gaseous reaction (combustion of wood gasses) is significant.
\item Mass transfer of moisture and flammable gases inside of fuel is insignificant.
\item The fuel is homogeneous.
\end{enumerate}

To determine if the physical parameters describing fuels we found by fitting TGA data with  gpyro are appropriate for the numerical treatment used in FDS, we compared numerical experiments to observations from experimental work \citep{Albini1995a}.  Agreement was excellent for intermediate moisture content (21\%) but deteriorated for low (4\%) and high (60\%) moisture content. Agreement was excellent for fuels of smaller diameter (~5cm) but suffered on those of larger diameter (~10cm). In all cases differences in mass loss rate between model and observations were less than 5\% of total mass lost with insignificant differences on smaller diameter, intermediate moisture content fuels.

\section{Experiments}
\subsection{Overview}
With a model that matches acceptably to previous experimental data, we move on to discuss experiments relevant to assessing the validity of thin fuel treatments. As mentioned above, we consider time until ignition and mass loss rate. We also examine the material composition of fuels as a function of time and space.
\par
Each set of experiments compares a series of 30cm long cylindrical fuels ranging from 1.0 mm to 5.0 mm in diameter at 4\%, 21\%, and 60\% moisture content are simulated. We also change the flux of the heat source to examine its significance at 112.5, 225, 450, and 900 $kW/m^2$.
\par
When we refer to heat flux we are referring to that of the burner and not that at the surface of the fuel. The fuel will only absorb a small fraction of the heat flux from the burner.
\par
Because of model sensitivity to initial conditions we conduct each experiment ten times with slight (1 $W/m^2$) perturbations in the burner’s heat flux. The resulting experimental data is averaged or regressed. This is mentioned in more detail in the discussion.

\subsection{Ignition Time}
We record the times at which each fuel reaches an ignition temperature of 350 \degree C at its surface. This is measured directly from the surface of the fuel in the model.

\subsection{Mass Loss Rate}
We record the mass loss rates of fuels starting at the time of heating. We record the mass of the fuel at every time step.

\subsection{Mass Loss Rate}
We also gather detailed sub-grid measurements of temperature and composition throughout the fuel. Composition is determined by mass fraction of each solid phase material component: water, wood, and char.

\begin{figure*}[]
\centering
\includegraphics[width=0.9\textwidth, keepaspectratio]{figures/mixedbag.pdf}
\caption{a,b,c: Ignition time delay increase in thermally thin fuel treatment over thermally thick fuel treatment as percentage of total time at heat fluxes of 112.5, 225, 450, and 900 $kW/m^2$. 4\%, 21\%, and 60\% moisture content top to bottom, respectively.  d,e: Composition of 5mm fuels at depth = 0.5mm, heat flux = 450 $kW/m^2$; d: thick treatment, e: thin treatment.  f: Simulated and measured TGA plots for lodgepole pine samples. 20\degree C/min, 10\degree C/min, and 5\degree C/min heating rates left to right, respectively.}
\end{figure*}

\begin{figure*}[]
\centering
\includegraphics[width=0.9\textwidth, keepaspectratio]{figures/ignition_6pane.pdf}
\caption{Ignition time delays. Left column: heat flux = 112.5 $kW/m^2$. Right column: heat flux = 900 $kW/m^2$. Top row: 4\% moisture content. Middle row: 21\% moisture content. Bottom row: 60\% moisture content.}
\end{figure*}

\begin{figure*}[t]
\centering
\includegraphics[width=0.9\textwidth, keepaspectratio]{figures/hausdorff_mixedpane.pdf}
\caption{Mass loss rates.  a,b: Hausdorff distance as function of fuel diameter. Top: heat flux = 225 $kW/m^2$. Bottom: heat flux = 900 $kW/m^2$.  c,d: mass loss of 21\% moisture content thermally thick and thermally thin treated fuels of diameter 1mm (top) and 5mm (bottom) at heat flux = 900 $kW/m^2$.}
\end{figure*}

\section{Experiment Discussion}

\subsection{Ignition Time}
We find that thermally-thin fuels ignite more slowly than thermally-thick fuels but that the difference varies with moisture content, heat flux, and fuel diameter. A direct comparison of ignition times for fuel of different sizes, moisture contents, and under different heat fluxes can be seen in Figure 2. Values from all 10 experiments are regressed for trend lines. The total increase in ignition time caused by thermally thin fuel prescriptions for all of our experiments may be found in Figures 1a, 1b, and 1c. These plots show averages of the 10 experiments with slightly perturbed heat fluxes in order to smooth out the sensitivity of the experiment at low heat flux.
\par
As can be seen in any plot in Figure 2, increasing fuel diameter increases the disparity between the ignition times of thermally thick and thermally thin fuel prescriptions. This is entirely expected. We can see that near 1mm diameter, the difference in ignition time in all experiments is never more than a fraction of a second. This supports the idea that thermally thin fuel prescriptions are generally appropriate for fuels under 1mm in thickness \citep{Quintiere2006}.
\par
Figure 2 demonstrates that fuels with higher moisture content are more appropriately modeled by thermally thin prescriptions than fuels with lower moisture content. It is easier to see this in the lower heat flux experiments in the left column but it is also present in the higher heat flux experiments in the right. This finding is somewhat less intuitive but could be explained by increased thermal conductivity in the fuel moving heat away from the surface and delaying ignition. This would impact thermally thick fuel treatments but not thermally thin fuel treatments because they are effectively a point in space. In Figure 1a, we note that the ignition time disparity for 4\% moisture content fuels heated at 112.5 $kW/m^2$ increases with size, but that in Figure 1b it increases more gradually for 21\% moisture content fuels and, in Figure 1c, it appears to not significantly increase at all for 60\% moisture content fuels. 
\par
Figure 1 shows that changes in ignition time from thermally thin prescriptions are susceptible to variations in heat flux. Lower heat fluxes cause less pronounced differences in ignition time between thermally thick and thermally thin fuel prescriptions. This agrees with existing models \citep{Quintiere2006}. This could be explained by lower heat flux slowly heating fuels of both prescriptions to temperature instead of rapidly heating the outer surface of fuels treated as thick.
\par
We see above that ignition time is affected by thermally thin fuel prescriptions quite significantly. When the fuels are as small as 1mm in diameter, they generally ignite very similarly to thermally thick fuels regardless of moisture content or the heat of the fire; as seen in Figure 2, very small fuels can see a change in ignition delay of well under a second. But even a slight increase in fuel size will quickly cause the ignition times of these fuels to diverge. Once fuels are 5mm in diameter, we see more than a doubling in ignition time for heating rates higher than 450 $kW/m^2$. We see that fuels much larger than a few millimeters in size will suffer from unrealistic ignition times. This is evident in many models that utilize the thin-fuels assumption; grass, needles, and other fuels rapidly ignite and are consumed but slightly larger fuels are left completely unaffected.

\subsection{Mass Loss Rate}
We find that mass loss rates of fuels of thermally thick and thermally thin prescriptions vary depending on moisture content, heat flux, and diameter. We see some direct mass loss rate comparisons in Figures 3c and 3d. We compute the Hausdorff distance between thermally-thick and thermally-thin treatments for fuels. This measures distances between subsets in a metric space: in our case, these are observations for an experiment in the metric space of time \citep{rockafellar2009variational}. With this test, we have a metric to compare dissimilarity between mass loss rate curves. Hausdorff distances are regressed for trend lines. These can be seen in Figures 3a and 3b. 
\par
In Figure 3c we see that fuels of 1mm in diameter burn up in virtually identical fashion whether they are prescribed as thermally thick or thermally thin. But in Figure 3d it’s clear that fuels that are 5mm in diameter show interesting differences based on their prescription. Thus, as expected, fuel diameter is a significant factor in mass loss rate differences between thermally thick and thermally thin treatments. The distinctive ‘step’ in the thermally thin prescribed fuel is where all moisture has been driven off but the wood has not yet started combusting; in Figure 3c we see that these processes occur with some overlap for thermally thick fuel treatments; fuels prescribed as thermally thin must have all of their moisture driven off before they can ignite.
\par
We can clearly see that moisture content has a marked effect on mass loss rate in Figures 3a and 3b. We see that higher moisture contents decrease the difference in mass loss rates between thermally thick and thermally thin prescriptions for fuels. This is possibly also caused by increased thermal conductivity in fuels with higher moisture content as described above.
\par
Heat flux is significant as well. Comparing Figure 3a to 3b, we can see that Hausdorff distances between the mass loss rate curves of thermally thick and thermally thin prescriptions are uniformly greater with increased heat flux. This is likely influenced by the reduced disparity in ignition times mentioned above.

\subsection{Fuel Composition}
We find that the composition of a fuel is significantly influenced by whether the fuel is prescribed as thermally thick or thermally thin. These may be seen in Figure 1d and 1e. This finding makes sense given the different way that these fuels heat and thus pyrolyze. In particular, we see that moisture must be entirely purged from fuels treated as thermally thin before the wood may ignite.
\par
The temperatures in the fuels also differ; we see that the fuel treated as thermally thin is heating more slowly than the other and that its temperature climb markedly slows as moisture is driven off. In the other fuel, temperature is more erratic due to its internal gradient and the process of driving off moisture is presumably less sudden since no such stall exists in the temperature data.

\section{Conclusion}
We established and validated a model for burning cylindrical fuels and recording their ignition times, mass loss rates, and material compositions. We numerically determined physical parameters for lodgepole pine fuel using experimental data. We conducted many experiments to establish the significance of fuel size, fuel moisture content, and fire intensity in the precision of thermally thin fuel prescriptions.
\par
We found that thermally thin fuel prescriptions have insignificant effects on both ignition time delay and mass loss rate at fuel diameters around 1mm but that both are significantly effected at fuel diameters approaching 5mm. We found that lower heat flux and higher fuel moisture content reduce the significance of these differences to a limited extent but that fuel diameter remains the single most important determinant in whether a thermally thin fuel prescription is appropriate.
\par
We conclude that thermally thin fuel prescriptions are entirely appropriate under the commonly accepted thickness of 1mm and suggest that, in cases where fuels are characterized by moderate to high moisture contents and are subjected to lower flux heat sources, they may be appropriate in slightly larger fuels. However, even with these conditions, they do suffer from loss of precision quite soon after.  Researchers should carefully consider both their fuel and fire environment before using thermally thin fuel prescriptions.

\section{Acknowledgements}
Research was funded through agreement \#17-JV-1221637-129 with the United States Forest Service Rocky Mountain Research Station with underlying support from the Western Wildlands Environmental Threat Assessment Center. We would like to thank the reviewers for their time and consideration of our submitted paper. We would like to thank the members of the FDS project for incorporating a feature request on our behalf.

\section{Appendices}
\subsection{Appendix A: Smallness of Thermally Thin Fuels}
Thermally thin objects are defined as being sufficiently thin that no internal temperature gradient is formed under heating. Generally, it must be the case that the physical thickness, d, is less than the thermal penetration depth. For the temperature gradient to be small over region d, it must be the case that

$$Bi \equiv \frac{dh_c}{k} \ll \frac{h_c(T_s - T_o)}{\dot{q''}}$$

where $Bi$ is the object’s Biot number, $h_c$ is the effective heat transfer coefficient, $k$ is the thermal conductivity, $T_s$ is the surface temperature of the object, T0 is the initial temperature of the object, and $\dot{q̇''}$ is the heat flux \citep{Quintiere2006}. If these conditions are satisfied then the ignition time for a fuel prescribed as thermally thin should be very similar to that of a fuel prescribed as thermally thick.

\subsection{Appendix B: Gpyro Formulations}
Gpyro is used to numerically approximate material parameters described above in addition to parameters of a single step, heterogeneous reaction as:

$$ \frac{d\alpha}{dt} = Z\ exp \left( - \frac{E}{RT} \right) (1-\alpha)^n $$

Where $\alpha$ is the dimensionless conversion, $Z$- called $A$ in the paper above- is the pre-exponential factor ($s^-1$), $E$ is the activation energy ($kJ/mol$), $n$ is the dimensionless reaction order \citep{Lautenberger2009}, $T$ is temperature in $\degree K$, and $R$ is the gas constant ($8.314 J/mol$).
\par
Gpyro assumes that the density and thermal conductivity of each condensed phase species vary with temperature. In the case of conductivity:

$$ k_i(T) = k_{s,i}(T) + k_{r,i}(T) = k_{0,i}\left(\frac{T}{T_r}\right)^{n_{k,i}} + \gamma_i \sigma T^3 $$

where $k_{0,i}$ is the conductivity at reference temperature $T_r$, $n_{k,i}$ is the exponent that scales the conductivity, $\gamma_i$ is the radiative portion of conductivity (for radiation crossing pores in the substrate) \citep{Lautenberger2009}, and $\sigma$ is the Stefan-Boltzmann constant $(5.67*10^{-8}W*m^{-2}*K^{-4})$. Thus Gpyro establishes materials may change in conductivity with respect to temperature.
\par

The parameters we numerically approximated are in Table 1.

\begin{table}[h]
\centering
\begin{tabular}{l l l}
\hline
\textbf{Name} & \textbf{Unit} & \textbf{Value}\\
\hline
$A\ (wood)$ & $s^{-1}$ & $2.45E+13$\\
\hline
$E\ (wood)$ & $\frac{kJ}{mol}$ & $178$\\
\hline
$Order$ & $-$ & $4.5$\\
\hline
$Rho\ initial\ (char)$ &  $\frac{kg}{m3}$ & $134$\\
\hline
$Conductivity\ initial\ (wood)$ & $\frac{W}{m*K}$ & $0.19$\\
\hline
$Conductivity\ initial\ (char)$ & $\frac{W}{m*K}$ & $0.095$\\
\hline
$Conductivity\ exp\ (wood)$ & $-$ & $0.038$\\
\hline
$Conductivity\ exp\ (char)$ & $-$ & $0.14$\\
\hline
$Specific\ Heat\ Cap.\ (wood)$ & $\frac{J}{kg*K}$ & $2845$\\
\hline
$Specific\ Heat\ Cap.\ (char)$ & $\frac{J}{kg*K}$ & $1734$\\
\end{tabular}
\caption{Material and reaction parameters for lodgepole pine wood found through numerical approximation with Gpyro. }
\end{table}


%\begin{figure}[h]
%\centering\includegraphics[width=0.4\linewidth]{placeholder}
%\caption{Figure caption}
%\end{figure}

%\begin{equation}
%\label{eq:emc}
%e = mc^2
%\end{equation}
%\end

%\begin{itemize}
%\item Bullet point one
%\item Bullet point two
%\end{itemize}


%\begin{table}[h]
%\centering
%\begin{tabular}{l l l}
%\hline
%\textbf{Treatments} & \textbf{Response 1} & \textbf{Response 2}\\
%\hline
%Treatment 1 & 0.0003262 & 0.562 \\
%Treatment 2 & 0.0015681 & 0.910 \\
%Treatment 3 & 0.0009271 & 0.296 \\
%\hline
%\end{tabular}
%\caption{Table caption}
%\end{table}

\section{References}
\bibliographystyle{model1-num-names}
\bibliography{summer_2018.bib}

\end{document}