function [min_point max_point] = get_extrema(points_in, min_point, max_point)
%get_extrema.m ... finds most and least optimal points 

%--------------------- get_extrema ---------------------
%   Author: Chap Alex 
%   Creation date: 07/07/05 
%   Last mod date: 01/13/06
%-------------------------------------------------------
% Purpose:  
%   This function updates the least (min) and most (max)
%   optimal inport configurations using the [points] argument.
%-------------------------------------------------------
% Calls to: 
%   (none)
%-------------------------------------------------------
% Called by:
%   sa_optimize.m
%-------------------------------------------------------
% Globals:
%   (none)
%-------------------------------------------------------
% Arguments:
%   points_in   ... either a matrix of [inports outport] values
%                   or an arry of structs having a field
%                   struct(x).points that is an array of [inports outports]
%                   --usually called with [walkers] struct
%   min_point   ... array containing the least optimal config seen
%                   [inports outport]
%   max_point   ... array containing the most optimal config seen
%                   [inports outport]
%-------------------------------------------------------
% Returns:
%   min_point   ... array containing the least optimal config seen
%                   [inports outport]
%   max_point   ... array containing the most optimal config seen
%                   [inports outport]
%-------------------------------------------------------
% Notes:
%   This function can handle both of the following conditions
%   1.  [points_in] is a placeholder for [walkers] 
%           and [walkers] has the field [.points]
%   2.  [points_in] is a matrix of point arrays where
%           each row is [inports outport]
%-------------------------------------------------------

%assume no min, max are found and adopt default values
%it is fine to assign a scalar, as only the last value
%maters in the min, max determination
if nargin == 1
    min_point = realmax;    %only min_point(end) matters
    max_point = -realmax;    %only max_point(end) matters
end

%set up the internal struct used to determine min, max
%[points_in] is one of two possiblities, described above
if isstruct(points_in) %[walkers] passed in for [points]
    points_to_check = points_in;
else                %an array of points for [points]
    %fill the internal struct
    for count = 1:size(points_in,1)
        points_to_check(count).points = points_in(count,:);
    end
end %else...

%check each point in the internal struct and update min, max
for count = 1:size(points_to_check,2)
    
    %check for new max point
    if points_to_check(count).points(end) > max_point(end)
        max_point = points_to_check(count).points;
    
    %check for new min point
    elseif points_to_check(count).points(end) < min_point(end)
        min_point = points_to_check(count).points;
    end
    
end %for count...
