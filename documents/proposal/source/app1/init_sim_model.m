function [func ranges inports_names num_inputs] = init_sim_model(strategy, dims)
%es_init_model.m ... initializes model to be optimized

%--------------------- es_init_model -------------------
%   Author: Chap Alex 
%   Creation date: 03/30/06 
%   Last mod date: 04/02/06
%-------------------------------------------------------
% Purpose:  
%   Initializes the random function model.  The current
%   setup is 2 inports, 1 outport with ranges of [-3,3] on
%   both inports.
%   This function also provides a function pointer to
%   the function that evaluates the random function.
%-------------------------------------------------------
% Calls to: 
%   (none)
%-------------------------------------------------------
% Called by:
%   es_main.m
%-------------------------------------------------------
% Globals:
%   (none)
%-------------------------------------------------------
% Arguments:
%   (none)
%-------------------------------------------------------
% Returns:
%   func        ... pointer to the function that evaluates the
%                   random function.
%   ranges      ... matrix of upper and lower bounds on all inports
%                   format is 
%                       
%                   ranges =    inport1_lower inport1_upper
%                               inport2_lower inport2_upper
%                               ...
%   num_inports ... number of inports model uses
%-------------------------------------------------------
% Notes:
%-------------------------------------------------------

if (strategy == 'sa')
    func = @sa_eval_sim;
elseif (strategy == 'es')
    func = @es_eval_sim;
else
    func = @si_eval_sim;
end

switch (dims)
    case 2
        inports_names = {'I','slip_const'};%, 'PBmax', 'Kf', 'ctrl', 'TB'};

        ranges = [.5 10 ; 0 .5];% ; 100 2000 ; .5 2 ; .5 2 ; .5 2];
    case 3
        inports_names = {'I','slip_const', 'PBmax'};%, 'Kf', 'ctrl', 'TB'};

        ranges = [.5 10 ; 0 .5 ; 100 2000];% ; .5 2 ; .5 2 ; .5 2];   
    case 4
        inports_names = {'I','slip_const', 'PBmax', 'Kf'};%, 'ctrl', 'TB'};

        ranges = [.5 10 ; 0 .5 ; 100 2000 ; .5 2];% ; .5 2 ; .5 2];
    case 5
        inports_names = {'I','slip_const', 'PBmax', 'Kf', 'ctrl'};%, 'TB'};

        ranges = [.5 10 ; 0 .5 ; 100 2000 ; .5 2 ; .5 2];% ; .5 2];
    case 6
        inports_names = {'I','slip_const', 'PBmax', 'Kf', 'ctrl', 'TB'};

        ranges = [.5 10 ; 0 .5 ; 100 2000 ; .5 2 ; .5 2 ; .5 2];
end

num_inputs = size(inports_names,2);
