%close all;
%clear;
%global state
%state = 15;
function [stats] = es_main_sim(dims)

[stats population] = es_perform_sim_optimization(dims);

