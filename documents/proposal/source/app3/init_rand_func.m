function [expected] = init_rand_func(state, range, max_distance, recreate)
%init_rand_func.m ... initializes random function generator

%------------------- init_rand_func --------------------
%   Author: Chap Alex 
%   Creation date: 08/05/05 
%   Last mod date: 01/06/06
%-------------------------------------------------------
% Purpose:  
%   Initializes the random function generator and returns
%   an estimated maximum outport value.  The expected value
%   will be more accurate as [max_distance] is reduced and
%   is also affected by the granularity used in creating
%-------------------------------------------------------
% Calls to: 
%   (none)
%-------------------------------------------------------
% Called by:
%   perform_optimization.m
%-------------------------------------------------------
% Globals:
%   print_file  ... handle for output file 
%   XM          ... inport 1 values for random function
%   YM          ... inport 2 values for random function
%   Z           ... outport values for random function
%-------------------------------------------------------
% Arguments:
%   state       ... seed for random function generator
%   ranges      ... matrix of upper and lower bounds on all inports
%                   format is 
%                       
%                   ranges =    inport1_lower inport1_upper
%                               inport2_lower inport2_upper
%                               ...
%   max_distance... controls accuracy of [expected], and is roughly
%                   the distance from the returned value of [expected]
%                   to the actual max value of the random function
%   recreate    ... TRUE == reset global meshgrid for rand function
%               ... FALSE == keep currne global variables
%-------------------------------------------------------
% Returns:
%   expected    ... Estimate of the maximum outport value in the
%                   random function.  It will be within [max_distance]
%                   
%-------------------------------------------------------
% Notes:
%-------------------------------------------------------
global XM YM Z print_file;

if nargin < 4   %assume user wants to keep global variables
    recreate = false;
end

%if any global variables are missing or user requests rebuild
if (isempty(XM) || isempty(YM) || isempty(Z) || recreate)
    fprintf(print_file, '\nInitializing random function...\n');
    
    % controls function's acceleration (change of slope)
    % lower numbers are less accelerated (gentler slopes)
    age = 1;
    
    % controls maximum value of global maximum
    hilliness = 10;
    
    % controls the roughness of the generated terrain
    % larger numbers produce less overall maxima
    granularity = 1;
    
    % controls the sequence of random numbers
    rand('state', state);
    
    % lays out x and y grid
    [XM YM] = meshgrid( range(1,1):granularity:range(1,2), ...
                        range(2,1):granularity:range(2,2));
                    
    
    %size of XM,YM grids
    rand_size = size(XM);
    
    %create random matrix
    Z = rand(rand_size);
    
    %smooths out Z matrix
    %muitiple calls to del2 will produce sharper slopes
    for i = 1:age
        Z = del2(Z);
    end
    
    % "grows" the hills 
    Z = Z * hilliness;
   
end %(isempty...)
    
%build temp grid to overlay and interpolate the [expected] value
[xm ym] = meshgrid( range(1,1):max_distance:range(1,2), ...
                    range(2,1):max_distance:range(2,2));

zm = interp2(XM,YM,Z, xm,ym, 'cubic');

expected = max(max(zm));
    
  