\section*{Proposal}\label{sec:proposal}

	The Wildland-Urban Interface Fire Dynamics Simulator (WFDS) is an extension of Fire Dynamics Simulator (FDS) that aims to simplify the inclusion of vegetation in physics-based simulation of forest fires \citep{wfds_site}. It offers an interface to end users that makes description of fuels much easier, improving development speed and minimizing introduced error. It also includes formulaic changes that are useful for studying fire in its prescribed domain but less so in others (like buildings). FDS is a cutting edge suite developed by the National Institute of Standards and Technology (NIST) and VTT Technical Research Centre of Finland that employs computational fluid dynamics (CFD) techniques to capture predicted phenomena at significantly finer temporal and spatial granularity than other models popular in physics-based simulation of fires \citep{Mcgrattan2017}. 
	
	WFDS has demonstrated considerable utility in modeling grasslands fire behavior \citep{wfds_site} and drawn interest and activity from groups seeking to model forest fire behavior \citep{Parsons2010} but it presently does not provide support for thermally-thick solid fuels.

\vspace{.1cm}
\begin{center}
\shadowbox{%
\begin{minipage}{1.0\textwidth}
\begin{quote}
\textit{WFDS relies on the ``thin fuels assumption'' which means that fuel particles prescribed as vegetation are treated as thermally-thin, meaning that they do not form an internal gradient: a so-called lumped capacitance model \citep{bergman}. We propose research to quantify this assumption's effects on wildfire propagation models.} 
\end{quote}
\end{minipage}
}%close shadowbox
\end{center}	  
    
%Presently, we know of no research investigating the consequences of this assumption on modeling the ignition and propagation of wildfires.  
    
	The behavior of a simulation can be affected in multiple ways by this assumption. Most obviously, it can affect the way that fuel particles are consumed in the simulation. The fuel particles may take different amounts of time to reach ignition, changing the rate at which fire propagates across the simulation's domain. This can also affect the rate at which fuels are burned once ignited. Further, the behavior of the CFD problem may also be affected. Solids will remove heat from the CFD if they are cooler than their immediate environment and add heat to it if they are warmer (and fuels may obviously add heat if they are combusting). This interaction is well understood \citep{bergman} but the consequences it may have on a fire environment have only recently become feasible for computational evaluation. 
  
	Developers of WFDS have modified fuel elements to behave more appropriately under wildfire conditions; this assists in the verification of their models but potentially undermines their validity. They have also made numerous changes to improve the behavior of vegetation in fire simulations. The most recent version of FDS (version 6) includes full support for thermally-thick particles but lacks the tools needed by users of WFDS to evaluate their models. Most critically, there has not yet been any rigorous study on the effect of thin versus thick fuel models using these tools. 
  
	We propose a two-part solution to this problem: we will assist in the verification studies WFDS is currently undertaking to ensure compliance with FDS and we will create an interim solution allowing WFDS researchers to make use of FDS models using their existing simulation configurations. This will ensure that comparisons of thick- and thin-fuel models are performed on a mathematically sound physics platform.
	  
	The first problem will require comparison of FDS heat and mass transfer models to analytical and numerical solutions. Analytical solutions to simple geometries are known \citep{bergman} and numerical solutions will be found using Finite Element Method (FEM) software packages \citep{AlnaesBlechta2015a}. With these comparisons, we will assist in the process of moving useful software from WFDS into the contemporary release of FDS, giving both groups of researchers access to developments from their counterparts. Further, we will evaluate the models against experimental data to verify correct treatment of boundary conditions and ensure that comparison of thick- and thin-fuel models are appropriately handled by FDS\citep{Albini1995a}.  
	  
	The second problem will require translation of existing WFDS configurations into FDS version 6. This will give WFDS users- who are in need of thermally-thick fuel behavior- access to features that aren't available in the version of FDS from which WFDS forked (FDS version 5). This problem has added complexity in that WFDS uses namespaces that FDS6 does not contain; it will likely require both modification of the FDS6 package and scripts to translate WFDS configurations to FSD6 compliant files. 
  
	This two-way approach will ensure both that cutting edge models from each project are available to other researchers and that study of thick- and thin-fuel models is done with sufficient rigor. It is likely that the verification problem will take substantially longer but our work will provide a solid foundation for other comparisons. More immediately, it will allow for direct testing by WFDS researchers of the efficacy of the ``thin fuels assumption''.