\begin{thebibliography}{6}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi: #1}\else
  \providecommand{\doi}{doi: \begingroup \urlstyle{rm}\Url}\fi

\bibitem[Albini and Reinhardt(1995)]{Albini1995a}
Frank~A. Albini and Elizabeth~D. Reinhardt.
\newblock {Modeling Ignition And Burning Rate Of Large Woody Natural Fuels}.
\newblock \emph{International Journal of Wildland Fire}, 5\penalty0
  (2):\penalty0 81--91, 1995.
\newblock ISSN 10498001.
\newblock \doi{10.1071/WF9950081}.

\bibitem[Aln{\ae}s et~al.(2015)Aln{\ae}s, Blechta, Hake, Johansson, Kehlet,
  Logg, Richardson, Ring, Rognes, and Wells]{AlnaesBlechta2015a}
Martin~S. Aln{\ae}s, Jan Blechta, Johan Hake, August Johansson, Benjamin
  Kehlet, Anders Logg, Chris Richardson, Johannes Ring, Marie~E. Rognes, and
  Garth~N. Wells.
\newblock The fenics project version 1.5.
\newblock \emph{Archive of Numerical Software}, 3\penalty0 (100), 2015.
\newblock \doi{10.11588/ans.2015.100.20553}.

\bibitem[Bergman et~al.(2011)Bergman, Lavine, Incropera, and DeWitt]{bergman}
Theodore~L. Bergman, Adrienne~S. Lavine, Frank~P. Incropera, and David~P.
  DeWitt.
\newblock \emph{Fundamentals of Heat and Mass Transfer}.
\newblock Wiley, 7th edition, 2011.

\bibitem[McGrattan et~al.(2017)McGrattan, Hostikka, McDermott, Floyd,
  Weinschenk, and Overholt]{Mcgrattan2017}
K~McGrattan, S.~Hostikka, Randall McDermott, Jason Floyd, Craig Weinschenk, and
  Kristopher Overholt.
\newblock {Sixth Edition Fire Dynamics Simulator Technical Reference Guide
  Volume 1 : Mathematical Model}.
\newblock 1:\penalty0 1--147, 2017.
\newblock \doi{http://dx.doi.org/10.6028/NIST.SP.1018-1}.

\bibitem[Mell(2010)]{wfds_site}
William Mell.
\newblock Wildland-urban fire models.
\newblock \url{https://www.fs.fed.us/pnw/fera/research/wfds/index.shtml}, 2010.
\newblock [Online; accessed 28-September-2017].

\bibitem[Parsons et~al.(2010)Parsons, Mell, and Mccauley]{Parsons2010}
Russell~A Parsons, William Mell, and Peter Mccauley.
\newblock {Modeling the spatial distribution of forest crown biomass and
  effects on fire behavior with FUEL3D and WFDS}.
\newblock \penalty0 (Rothermel 1983), 2010.

\end{thebibliography}
